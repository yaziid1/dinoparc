export type ImageMode = "RUFFLE" | "BASE_64" | "NONE";

export interface Account {
  account: {
    id: string;
    name: string;
    cash: number;
    unlockedDinoz: number[];
    nbPoints: number;
    averageLevelPoints: number;
    messages: boolean;
    blackList: unknown;
    lastLogin: number;
    sacrificePoints: unknown;
    dailyShiniesFought: number;
    hermitStage: number;
    hermitStageCurrentWins: number;
    hermitStageFightingDinoz: unknown;
    accountDinozLimit: number;
    wistitiCaptured: number;
    eventCounter: unknown;
    dinozIsList: unknown[];
    imgMode?: ImageMode;
  };
  numberOfDinoz: number;
}
