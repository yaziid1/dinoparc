export interface CraftResult {
  success: boolean;
  generatedObject: string;
  generatedObjectQty: number;
}
