export interface ElementsValues {
  Feu: number;
  Terre: number;
  Eau: number;
  Foudre: number;
  Air: number;
}
