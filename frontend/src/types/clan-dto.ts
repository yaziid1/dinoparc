export interface ClanDisplayDto {
  clanId: string;
  clanName: boolean;
  creatorName: string;
  creationDate: string;

  nbPlayers: number;
  position: number;
  nbDinoz: number;
  nbDinozWarrior: number;
  pointsTotal: number;
  allies: unknown[];
}
