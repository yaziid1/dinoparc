import { Totem } from "./totem";

export interface Clan {
  id: unknown;
  creatorId: unknown;
  name: string;
  nbDinozTotal: number;
  nbDinozWarriors: number;
  nbPointsOccupation: number;
  nbPointsFights: number;
  nbPointsTotem: number;
  position: number;
  faction: string;
  creationDate: string;
  pages: unknown;
  totem: Totem;
  chest: Map<string, number>;
  messages: unknown[];
  members: unknown[];
  candidates: unknown[];
  allies: unknown[];
}
