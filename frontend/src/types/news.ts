export interface News {
  dateCh: null;
  dateEn: string;
  dateEs: string;
  dateFr: string;
  image: string;
  textCh: null;
  textEn: string;
  textEs: string;
  textFr: string;
  timestamp: number;
  titleCh: null;
  titleEn: string;
  titleEs: string;
  titleFr: string;
}
