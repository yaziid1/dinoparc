export interface ClanPage {
  pageTitle: string;
  isPrivate: boolean;
  rawContent: string;
  pageId: string;
}
