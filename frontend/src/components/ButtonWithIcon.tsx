import React from "react";
import Button, { Props as ButtonProps } from "./Button";

import "./ButtonWithIcon.scss";

type Props = ButtonProps & {
  iconSrc: string;
  children: React.ReactNode;
};

export default function ButtonWithIcon({
  iconSrc,
  className = "",
  children,
  ...attrs
}: Props) {
  return (
    <Button className={`buttonWithIcon ${className}`.trim()} {...attrs}>
      <div className="buttonWithIcon-main">
        <img alt="" src={iconSrc} />
        {children}
        <img alt="" src={iconSrc} />
      </div>
    </Button>
  );
}
