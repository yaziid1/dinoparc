import React from "react";
import iconDefault from "../assets/hist_default.gif";
import iconDanger from "../assets/hist_error.gif";

import "./Message.css";

type Props = React.HTMLAttributes<HTMLDivElement> & {
  type?: "info" | "danger" | "success";
  iconSrc?: string;
  children: React.ReactNode;
};

export default function Message({
  type = "info",
  iconSrc,
  className = "",
  children,
}: Props) {
  if (!iconSrc) {
    iconSrc = type === "danger" ? iconDanger : iconDefault;
  }
  return (
    <div className={`message is-${type} ${className}`.trim()}>
      <img className="message-icon" alt="" src={iconSrc} />
      <div>{children}</div>
    </div>
  );
}
