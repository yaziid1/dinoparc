import React from "react";

import "./Progress.scss";

type Props = React.HTMLAttributes<HTMLProgressElement> & {
  value: number;
  max: number;
  expanded?: boolean;
};

export default function Progress({
  value,
  max,
  expanded,
  className = "",
  ...attrs
}: Props) {
  return (
    <progress
      className={`progress ${
        expanded ? "is-expanded" : ""
      } ${className}`.trim()}
      value={value}
      max={max}
      {...attrs}
    />
  );
}
