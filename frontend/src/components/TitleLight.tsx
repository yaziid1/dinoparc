import React from "react";

type Props = React.HTMLAttributes<HTMLHeadingElement> & {
  tagName?: "h3" | "div";
  children: React.ReactNode;
};
export default function ({
  tagName = "h3",
  children,
  className = "",
  ...attrs
}: Props) {
  const Element = tagName;
  return (
    <Element className={`header4 ${className}`.trim()} {...attrs}>
      {children}
    </Element>
  );
}
