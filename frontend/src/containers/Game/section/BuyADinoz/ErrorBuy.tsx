import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import { useTranslation } from "react-i18next";

export default function ErrorBuy() {
  const [open, setOpen] = React.useState(true);
  let { t } = useTranslation();

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent className="badLoginDialog">
          <p className="badLoginDialog">{t("alertCantBuy")}</p>
        </DialogContent>
      </Dialog>
    </div>
  );
}
