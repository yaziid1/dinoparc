import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../../index";
import { MoonLoader } from "react-spinners";
import ErrorBuy from "./ErrorBuy";
import getCorrectLevelUrlFromInteger from "../../../utils/LevelUrlByInteger";
import ReactTooltip from "react-tooltip";
import actBuy from "../../../../media/actions/act_buy.gif";
import tinycoins from "../../../../media/minis/tiny_coin.gif";
import champifuz from "../../../../media/minis/minichampi.png";
import elem0 from "../../../../media/dinoz/element0.gif";
import elem1 from "../../../../media/dinoz/element1.gif";
import elem2 from "../../../../media/dinoz/element2.gif";
import elem3 from "../../../../media/dinoz/element3.gif";
import elem4 from "../../../../media/dinoz/element4.gif";
import { confirmAlert } from "react-confirm-alert";
import DinozRenderTile from "../../shared/DinozRenderTile";

export default function BuyADinoz(props) {
  const { t, i18n } = useTranslation();
  const store = Store.getInstance();
  const [dinozBuyList, setDinozBuyList] = useState([]);
  const [errorBuy, setErrorBuy] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [quantityMap, setQuantityMap] = useState({});

  useEffect(() => {
    const store = Store.getInstance();

    axios.get(apiUrl + "/account/" + store.getAccountId() + "/inventory")
        .then(({data}) => {
          setQuantityMap(data.inventoryItemsMap);
          axios.get(apiUrl + "/player-dinoz-store/" + store.getAccountId())
              .then(({data}) => {
                setDinozBuyList(data);
                setIsLoading(false);
                window.scrollTo(0, 0);
              });
        });
  }, [setIsLoading]);

  function NameDinoz(newBornDinozId) {
    confirmAlert({
      customUI: ({ onClose }) => {
        let nameInput = "";
        return (
          <form
            className="neoparcConfirmOverlay"
            action="#"
            onSubmit={(e) => {
              e.preventDefault();
              confirm(newBornDinozId, nameInput.substring(0, 15));
              onClose();
            }}
          >
            <div className="promptName">
              <p>{t("giveNameToDinoz")}</p>
              <input
                id="nameChange"
                type="text"
                maxLength={15}
                onChange={(e) => {
                  nameInput = e.target.value;
                }}
              />
              <button className="btnOkPrompt" type="submit">
                Ok!
              </button>
            </div>
          </form>
        );
      },
      closeOnEscape: true,
    });
  }

  async function confirm(newBornDinozId, nameInput) {
    window.scrollTo(0, 0);
    setIsLoading(true);
    const store = Store.getInstance();
    await axios
      .post(apiUrl + "/player-dinoz-store", {
        accountId: store.getAccountId(),
        newBornDinozId: newBornDinozId,
        dinozName: nameInput,
      })
      .then(({ data }) => {
        if (data) {
          setErrorBuy(true);
          setIsLoading(false);
          setTimeout(function () {
            setErrorBuy(false);
          }, 6000);
        } else {
          store.setSelectedDinoz(newBornDinozId);
          props.refreshHistory();
          props.callBack();
        }
      });
  }

  function spendChampifuz() {
    if (window.confirm(t("confirm"))) {
      setIsLoading(true);
      const store = Store.getInstance();
      axios
        .put(apiUrl + "/account/" + store.getAccountId() + "/champifuz")
        .then(({ data }) => {
          if (data) {
            window.scrollTo(0, 0);
            axios
              .get(apiUrl + "/account/" + store.getAccountId() + "/inventory")
              .then(({ data }) => {
                setQuantityMap(data.inventoryItemsMap);
              });

            axios
              .get(apiUrl + "/player-dinoz-store/" + store.getAccountId())
              .then(({ data }) => {
                setDinozBuyList(data);
                setIsLoading(false);
              });
          }
        });
    }
  }

  function getAvailableTickets() {
    if (
      quantityMap["Ticket Champifuz"] !== null &&
      quantityMap["Ticket Champifuz"] >= 1
    ) {
      return quantityMap["Ticket Champifuz"];
    }
    return 0;
  }

  return (
    <>
      {isLoading === true && (
        <MoonLoader color="#c37253" css="margin-left : 240px;" />
      )}
      {isLoading === false && (
        <div>
          <header className="pageCategoryHeader">{t("buyADinozLabel")}</header>
          <p className="chapter">{t("textBuyDinoz")}</p>

          <p className="chapter">
            {t("youHave") +
              " " +
              getAvailableTickets() +
              " " +
              t("boutique.Ticket Champifuz.plural")}
            {getAvailableTickets() >= 1 && (
              <>
                <a
                  className="clickablePageFromClan"
                  onClick={(e) => spendChampifuz()}
                >
                  {t("use") + "!"}
                </a>
                <img
                  className="minichampifuz"
                  src={champifuz}
                  alt={"Champifuz icon"}
                ></img>
              </>
            )}
          </p>
          <header className="pageCategoryHeader">{t("listeLabel")}</header>
          {errorBuy && <ErrorBuy />}
          <div>
            {dinozBuyList.map(function (newBornDinoz, idx) {
              return (
                <div className="chapter" key={idx}>
                  <div className="borderDinoz">
                    <div className="tbodyBuy">
                      <tr>
                        <td className="picBox">
                          <div className="dinoSheet">
                            <div className="dinoSheetWrap">
                              <DinozRenderTile
                                appCode={newBornDinoz.appearanceCode}
                                size={100}
                              />
                            </div>
                            <div
                              className="actionDiv"
                              onClick={(e) => {
                                NameDinoz(newBornDinoz.id);
                              }}
                            >
                              <img
                                alt=""
                                className="buyButtonBox"
                                src={actBuy}
                              />
                              <span className="buyThisDinoz">
                                {t("buyThisDinoz")}
                              </span>
                            </div>
                          </div>
                        </td>
                        <td className="infoBox">
                          <div className="fiche">{t("fiche")}</div>
                          <table className="ficheTable">
                            <tbody>
                              <tr>
                                <th>{t("niveau")}</th>
                                <th className="menuDinozFiche">1</th>
                              </tr>
                              <tr>
                                <th>{t("race")}</th>
                                <th className="menuDinozFiche">
                                  <span className="paddingRace">{newBornDinoz.race}</span>
                                  <span
                                    className="imageSpan"
                                    lang={i18n.language}
                                    data-place="right"
                                    data-tip={t("tooltip." + newBornDinoz.race)}
                                  />
                                </th>
                              </tr>
                              <tr>
                                <th>{t("prix")}</th>
                                <th className="menuDinozPrice">
                                  {newBornDinoz.price}
                                  <img
                                    alt=""
                                    className="coin"
                                    src={tinycoins}
                                  />
                                </th>
                              </tr>
                            </tbody>
                          </table>
                          <ul className="elements">
                            <li className="elementsLi">
                              <img alt="" src={elem0} />{" "}
                              {newBornDinoz.initialElementsValues.Feu}
                            </li>
                            <li className="elementsLi">
                              <img alt="" src={elem1} />{" "}
                              {newBornDinoz.initialElementsValues.Terre}
                            </li>
                            <li className="elementsLi">
                              <img alt="" src={elem2} />{" "}
                              {newBornDinoz.initialElementsValues.Eau}
                            </li>
                            <li className="elementsLi">
                              <img alt="" src={elem3} />{" "}
                              {newBornDinoz.initialElementsValues.Foudre}
                            </li>
                            <li className="elementsLi">
                              <img alt="" src={elem4} />{" "}
                              {newBornDinoz.initialElementsValues.Air}
                            </li>
                          </ul>
                        </td>
                        <td>
                          <div className="competences">{t("competences")}</div>
                          <div className="comp">
                            <ul className="ulComp">
                              {Object.keys(newBornDinoz.initialCompList).map(
                                function (key) {
                                  return (
                                    <div>
                                      <li className="liComp">
                                        {t(key.toString())}
                                        <img
                                          alt=""
                                          className="imgOffset"
                                          src={getCorrectLevelUrlFromInteger(
                                            newBornDinoz.initialCompList[key]
                                          )}
                                        />
                                      </li>
                                    </div>
                                  );
                                }
                              )}
                            </ul>
                          </div>
                        </td>
                        <ReactTooltip
                          className="largetooltip"
                          html={true}
                          backgroundColor="transparent"
                        />
                      </tr>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      )}
    </>
  );
}
