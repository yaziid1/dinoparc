import { useTranslation } from "react-i18next";
import React from "react";
import axios from "axios";
import { apiUrl } from "../../../../index";
import { useUserData } from "../../../../context/userData";
import Section from "../../../../components/Section";
import { ImageMode } from "../../../../types/account";
import Button from "../../../../components/Button";
import Content from "../../../../components/Content";
import Row from "../../../../components/Row";

function getImageTranslationKey({ mode }: { mode: ImageMode }) {
  const base = "moncompte.imgMode.";
  switch (mode) {
    case "RUFFLE":
      return base + "ruffle";
    case "BASE_64":
      return base + "base64";
  }
}

type Props = {};

export default function AccountParametersSection(props: Props) {
  const { t } = useTranslation();
  const { accountId, imageMode, setImageMode } = useUserData();

  function getImageText({ mode }: { mode: ImageMode }) {
    const key = getImageTranslationKey({ mode });
    const text = t(key);
    return mode === imageMode ? "🟢 " + text : text;
  }

  return (
    <Section title={t("moncompte.gestion")}>
      <Content>
        <p>{t("moncompte.imgMode.header")}</p>
        <Row gap="large" vcenter={true}>
          {(["BASE_64", "RUFFLE"] as const).map((mode) => (
            <Button
              key={mode}
              onClick={() => setImageMode(mode)}
              active={imageMode === mode}
              disabled={imageMode === mode}
            >
              {getImageText({ mode })}
            </Button>
          ))}
        </Row>
      </Content>
    </Section>
  );
}
