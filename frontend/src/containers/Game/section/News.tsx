import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../index";
import { News as TNews } from "../../../types/news";
import Section, { SectionContentNoPadding } from "../../../components/Section";
import Message from "../../../components/Message";
import { Loader } from "../../../components/Loader";

import "./News.css";
import Content from "../../../components/Content";

type TranslatedNews = {
  image: string;
  timestamp: number;
  title: string;
  text: string;
  /** @deprecated use Intl.DateTimeFormat */
  stringDate: string;
};

type Props = {};

export default function News(props: Props) {
  const { t, i18n } = useTranslation();
  const [newsList, setNewsList] = useState<TNews[]>([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    axios.get<TNews[]>(apiUrl + "/utils/news").then(({ data }) => {
      setNewsList(data);
      setIsLoading(false);
      window.scrollTo(0, 0);
    });
  }, []);

  function getTranslatedNews(news: TNews, lang: string): TranslatedNews {
    switch (lang) {
      case "fr":
        return {
          image: news.image,
          timestamp: news.timestamp,
          stringDate: news.dateFr,
          title: news.titleFr,
          text: news.textFr,
        };
      case "es":
        return {
          image: news.image,
          timestamp: news.timestamp,
          stringDate: news.dateEs,
          title: news.titleEs,
          text: news.textEs,
        };
      case "en":
      default:
        return {
          image: news.image,
          timestamp: news.timestamp,
          stringDate: news.dateEn,
          title: news.titleEn,
          text: news.textEn,
        };
    }
  }

  return (
    <div className="TEMP_mainContainer">
      <Section title={t("newsLabel")}>
        {/*<SectionContentNoPadding>*/}
        {/*  <Message>*/}
        {/*    {t("etwinBrowserAd")}*/}
        {/*    <a*/}
        {/*      target="_blank"*/}
        {/*      href="https://eternal-twin.net/docs/desktop"*/}
        {/*      rel="noreferrer"*/}
        {/*    >*/}
        {/*      {t("etwinBrowserAdDownload")}*/}
        {/*    </a>*/}
        {/*  </Message>*/}
        {/*</SectionContentNoPadding>*/}

        {isLoading && <Loader css="margin: 2rem auto" />}

        {newsList.map((news, idx) => (
          <NewsItem key={idx} news={getTranslatedNews(news, i18n.language)} />
        ))}
      </Section>
    </div>
  );
}

const NewsItem = ({
  news: { image, title, stringDate, text },
}: {
  news: TranslatedNews;
}) => {
  const [imageSrc, setImageSrc] = useState("");

  useEffect(() => {
    let ignore = false;
    import(`../../../media/news/${image || "tech.gif"}`).then((src) => {
      if (!ignore) {
        setImageSrc(src.default);
      }
    });

    return () => {
      ignore = true;
    };
  }, [image]);

  return (
    <div className="newsItem">
      {imageSrc && <img alt="" src={imageSrc} width="100" height="100" />}
      <div className="newsItem-content">
        <h3 className="newsItem-title">{title}</h3>
        <h6 className="newsItem-date">{stringDate}</h6>
        <Content className="newsItem-text">
          <p>{text}</p>
        </Content>
      </div>
    </div>
  );
};
