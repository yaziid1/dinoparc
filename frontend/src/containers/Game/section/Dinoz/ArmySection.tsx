import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../../index";
import Row, { Col } from "../../../../components/Row";
import Content from "../../../../components/Content";
import { ArmyOverview, Dinoz } from "../../../../types/dinoz";
import DinozRenderTile from "../../shared/DinozRenderTile";
import Table from "../../../../components/Table";
import Message from "../../../../components/Message";
import Button from "../../../../components/Button";
import Progress from "../../../../components/Progress";
import GameSection from "./shared/GameSection";

type Props = {
  dinoz: Dinoz;
  processAction: (dinozId: string, actionId: string, e: any) => void;
  returnToDinoz: () => void;
};

export default function ArmySection(props: Props) {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(true);
  const [armyOverview, setArmyOverview] = useState<null | ArmyOverview>(null);

  const dinozId = props.dinoz.id;
  const bossId = props.dinoz.bossInfos?.bossId;
  const mainActionKey =
    props.dinoz.actionsMap.Army_Quit === true
      ? "Army_Quit"
      : props.dinoz.actionsMap.Army_Join === true
      ? "Army_Join"
      : undefined;
  const bossCurrentLife = armyOverview?.boss?.life || 0;
  const bossMaxLife = armyOverview?.boss?.maxLife || 0;

  useEffect(() => {
    setIsLoading(true);

    axios
      .get<ArmyOverview>(
        `${apiUrl}/boss/armyDinoz/${bossId}/${dinozId}/overview`
      )
      .then(({ data }) => {
        setArmyOverview(data);
        setIsLoading(false);
      });
  }, [bossId, dinozId]);

  return (
    <GameSection
      isLoading={isLoading}
      title={t("boss.raid.nature")}
      returnToDinoz={props.returnToDinoz}
    >
      <Row className="mb-4">
        <div>
          <DinozRenderTile
            appCode={armyOverview?.boss?.appearanceCode}
            className="dinoPic"
            size={200}
          />
          <div className="has-text-centered mt-2 px-4">
            <Progress
              value={bossCurrentLife}
              max={bossMaxLife}
              expanded={true}
            />
            <div>
              {bossCurrentLife !== bossMaxLife
                ? `${bossCurrentLife.toLocaleString()} / ${bossMaxLife.toLocaleString()}`
                : bossMaxLife.toLocaleString()}
            </div>
          </div>
        </div>
        <Col grow={true}>
          <Content>
            <div
              dangerouslySetInnerHTML={{
                __html: t("boss.raid.description"),
              }}
            ></div>
          </Content>

          {mainActionKey != null && (
            <div className="mt-4 has-text-centered">
              <Button
                onClick={(e) => props.processAction(dinozId, mainActionKey, e)}
              >
                {t(mainActionKey)}
              </Button>
            </div>
          )}
        </Col>
      </Row>

      {armyOverview?.armyDinozs?.length === 0 ? (
        <Message>{t("boss.raid.emptyArmy")}</Message>
      ) : (
        <Table>
          <thead>
            <tr>
              <th className="has-text-centered">{t("boss.raid.rank")}</th>
              <th>{t("boss.raid.dinozName")}</th>
              <th>{t("boss.raid.masterName")}</th>
              <th className="has-text-centered">{t("boss.raid.nbAttacks")}</th>
              <th className="has-text-centered">
                {t("boss.raid.maxNbAttacks")}
              </th>
              <th className="has-text-right">{t("boss.raid.bossLifeLost")}</th>
            </tr>
          </thead>
          <tbody>
            {armyOverview?.armyDinozs?.map((dinoz) => (
              <tr key={dinoz.armyDinozId}>
                <td className="has-text-centered">{dinoz.rank}</td>
                <td>{dinoz.dinozName}</td>
                <td>{dinoz.masterName}</td>
                <td className="has-text-centered">{dinoz.nbAttacks}</td>
                <td className="has-text-centered">{dinoz.maxNbAttacks}</td>
                <td className="has-text-right">
                  {dinoz.bossLifeLost.toLocaleString()}
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}
    </GameSection>
  );
}
