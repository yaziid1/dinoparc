import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import ruines from "../../../../media/lieux/zone13.gif";
import axios from "axios";
import { apiUrl } from "../../../../index";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import { CraftResult } from "../../../../types/craft-result";
import { useUserData } from "../../../../context/userData";
import Button from "../../../../components/Button";
import Section from "../../../../components/Section";
import Row, { Col } from "../../../../components/Row";
import Content from "../../../../components/Content";
import { Loader } from "../../../../components/Loader";

const enum PageSection {
  cauldron,
  recipes,
}

type Objects = {
  "Feuille de Pacifique": number;
  "Oréade blanc": number;
  "Tige de Roncivore": number;
  "Anémone solitaire": number;
  "Perche perlée": number;
  "Grémille grelottante": number;
  "Cube de glu": number;
};

type Props = {
  dinozId: string;
};

export default function ChaudronMayinca({ dinozId }: Props) {
  const { t, i18n } = useTranslation();
  const { accountId } = useUserData();
  const [isLoading, setIsLoading] = useState(true);
  const [craftSuccess, setCraftSuccess] = useState<boolean | null>(null);
  const [sectionCurrentlyActive, setSectionCurrentlyActive] =
    useState<PageSection>(PageSection.cauldron);
  let [craftableObjectsMap, setCraftableObjectsMap] = useState<Objects | {}>(
    {}
  );
  const craftableObjectsKeys = Object.keys(
    craftableObjectsMap
  ) as (keyof Objects)[];
  let [craftResult, setCraftResult] = useState<CraftResult | null>(null);

  //Hint : Declare the new objects here :
  const [numberOfPlant1ToGive, setNumberOfPlant1ToGive] = useState(0);
  const [numberOfPlant2ToGive, setNumberOfPlant2ToGive] = useState(0);
  const [numberOfPlant3ToGive, setNumberOfPlant3ToGive] = useState(0);
  const [numberOfPlant4ToGive, setNumberOfPlant4ToGive] = useState(0);
  const [numberOfFish1ToGive, setNumberOfFish1ToGive] = useState(0);
  const [numberOfFish2ToGive, setNumberOfFish2ToGive] = useState(0);
  const [numberOfFish3ToGive, setNumberOfFish3ToGive] = useState(0);

  useEffect(() => {
    axios
      .get<Objects>(
        apiUrl +
          "/account/" +
          accountId +
          "/" +
          dinozId +
          "/chaudron/craftables"
      )
      .then(({ data }) => {
        setCraftableObjectsMap(data || {});
        setCraftableObjectsMap({
          "Feuille de Pacifique": 99,
          "Oréade blanc": 99,
          "Tige de Roncivore": 99,
          "Anémone solitaire": 99,
          "Perche perlée": 99,
          "Grémille grelottante": 99,
          "Cube de glu": 99,
        });
        setIsLoading(false);
      });
  }, [dinozId]);

  //Hint : Declare the new objects here :
  function sellQtyChange(e, key: keyof Objects) {
    if (key === "Feuille de Pacifique") {
      setNumberOfPlant1ToGive(e.target.value);
    } else if (key === "Oréade blanc") {
      setNumberOfPlant2ToGive(e.target.value);
    } else if (key === "Tige de Roncivore") {
      setNumberOfPlant3ToGive(e.target.value);
    } else if (key === "Anémone solitaire") {
      setNumberOfPlant4ToGive(e.target.value);
    } else if (key === "Perche perlée") {
      setNumberOfFish1ToGive(e.target.value);
    } else if (key === "Grémille grelottante") {
      setNumberOfFish2ToGive(e.target.value);
    } else if (key === "Cube de glu") {
      setNumberOfFish3ToGive(e.target.value);
    }
  }

  function confirmCraftAndSendToServer() {
    setCraftSuccess(null);
    setIsLoading(true);
    const objects = {
      "Feuille de Pacifique": numberOfPlant1ToGive,
      "Oréade blanc": numberOfPlant2ToGive,
      "Tige de Roncivore": numberOfPlant3ToGive,
      "Anémone solitaire": numberOfPlant4ToGive,
      "Perche perlée": numberOfFish1ToGive,
      "Grémille grelottante": numberOfFish2ToGive,
      "Cube de glu": numberOfFish3ToGive,
    };

    setNumberOfFish1ToGive(0);
    setNumberOfFish2ToGive(0);
    setNumberOfFish3ToGive(0);
    setNumberOfPlant1ToGive(0);
    setNumberOfPlant2ToGive(0);
    setNumberOfPlant3ToGive(0);
    setNumberOfPlant4ToGive(0);

    axios
      .post<CraftResult>(
        apiUrl +
          "/account/" +
          accountId +
          "/" +
          dinozId +
          "/chaudron/postUpForCraft",
        { objects }
      )
      .then(({ data }) => {
        if (data.success) {
          setCraftSuccess(true);
          setIsLoading(false);
          setCraftResult(data);
        } else {
          setCraftSuccess(false);
          setIsLoading(false);
        }
      });
  }

  function changeSection(section: PageSection) {
    setNumberOfFish1ToGive(0);
    setNumberOfFish2ToGive(0);
    setNumberOfFish3ToGive(0);
    setNumberOfPlant1ToGive(0);
    setNumberOfPlant2ToGive(0);
    setNumberOfPlant3ToGive(0);
    setNumberOfPlant4ToGive(0);
    setCraftSuccess(null);
    setSectionCurrentlyActive(section);
  }

  return (
    <div className="TEMP_mainContainer">
      <Section title={t("Chaudron")}>
        <Row>
          <Col>
            <img alt="" className="imgIrma" src={ruines} />
          </Col>
          <Col grow={true}>
            <Content>
              <p>{t("Chaudron.details") + " " + t("Chaudron.details2")}</p>
            </Content>
          </Col>
        </Row>

        {isLoading === true ? (
          <Loader css="margin: 2rem auto;" />
        ) : (
          <div>
            <Row justify="space-evenly" className="my-4">
              <Button
                size="medium"
                active={sectionCurrentlyActive === PageSection.cauldron}
                onClick={() => {
                  changeSection(PageSection.cauldron);
                }}
              >
                {t("chaudron.etiquette1")}
              </Button>
              <Button
                size="medium"
                active={sectionCurrentlyActive === PageSection.recipes}
                onClick={() => {
                  changeSection(PageSection.recipes);
                }}
              >
                {t("chaudron.etiquette2")}
              </Button>
            </Row>
            {sectionCurrentlyActive === PageSection.cauldron && (
              <div className="chaudron my-4">
                <div>
                  {craftableObjectsKeys.map(function (key: keyof Objects) {
                    return (
                      <div key={key} className="blockBonusBazar">
                        {craftableObjectsMap[key] > 0 && (
                          <div>
                            <img alt="" src={getConsumableByKey(key)} />
                            <input
                              className="numberFieldBazarBis"
                              type="number"
                              min="0"
                              max={craftableObjectsMap[key]}
                              placeholder={"0"}
                              onInput={(e) => {
                                sellQtyChange(e, key);
                              }}
                            />
                          </div>
                        )}
                      </div>
                    );
                  })}
                </div>
                <Button
                  className="buttonChaudron"
                  size="large"
                  onClick={(e) => {
                    if (window.confirm(t("confirm"))) {
                      confirmCraftAndSendToServer();
                    }
                  }}
                >
                  {t("putCauldron")}
                </Button>
              </div>
            )}

            {sectionCurrentlyActive === PageSection.recipes && (
              <div>
                <div className="recettes">
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Perche perlée")} />
                    x50
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Grémille grelottante")}
                    />
                    x10
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Feuille de Pacifique")}
                    />
                    x50
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Oréade blanc")} />
                    x10
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Nuage Burger")} />
                    x10
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Perche perlée")} />
                    x75
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Grémille grelottante")}
                    />
                    x15
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Feuille de Pacifique")}
                    />
                    x60
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Oréade blanc")} />
                    x20
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tige de Roncivore")} />
                    x1
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tarte Viande")} />
                    x10
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Feuille de Pacifique")}
                    />
                    x90
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Oréade blanc")} />
                    x45
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tige de Roncivore")} />
                    x10
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Médaille Chocolat")} />
                    x10
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Feuille de Pacifique")}
                    />
                    x20
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Oréade blanc")} />
                    x5
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tige de Roncivore")} />
                    x3
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Anémone solitaire")} />
                    x1
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tisane des bois")} />
                    x1
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Grémille grelottante")}
                    />
                    x20
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Cube de glu")} />
                    x1
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tige de Roncivore")} />
                    x20
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Anémone solitaire")} />
                    x2
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Bière de Dinojak")} />
                    x1
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Cube de glu")} />
                    x25
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Anémone solitaire")} />
                    x25
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("unknown")} />
                  </a>
                </div>
              </div>
            )}

            {craftSuccess && craftResult !== null && (
              <div>
                <div className="merchantMessage">{t("accept")}</div>
                <a className="recetteElement">
                  <div className="resultCraftChaudron">
                    <img
                      alt=""
                      src={getConsumableByKey(craftResult.generatedObject)}
                    />
                    <>{"   "}</>
                    <span className="espaceNom">
                      {"x" + craftResult.generatedObjectQty}
                    </span>
                    <>{"   "}</>
                    <>{"   "}</>
                    <span className="espaceNom">
                      {t("boutique." + craftResult.generatedObject)}
                    </span>
                    <span
                      className="imageSpan"
                      lang={i18n.language}
                      data-place="right"
                      data-tip={t(
                        "boutique.tooltip." + craftResult.generatedObject
                      )}
                    />
                    <ReactTooltip
                      className="largetooltip"
                      html={true}
                      backgroundColor="transparent"
                    />
                  </div>
                </a>
              </div>
            )}
            {craftSuccess != null && craftSuccess !== true && (
              <div className="errorResultFuzPrix">{t("refuser")}</div>
            )}
          </div>
        )}
        <ReactTooltip
          className="largetooltip"
          html={true}
          backgroundColor="transparent"
        />
      </Section>
    </div>
  );
}
