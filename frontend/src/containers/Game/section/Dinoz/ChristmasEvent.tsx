import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import axios from "axios";
import { apiUrl } from "../../../../index";
import PrizeWheel from "./shared/PrizeWheel";
import getActionImageFromActionString from "../../../utils/ActionImageFromString";
import getLocationImageByNumber, {
  PlaceNumber,
} from "../../../utils/LocationImageByPlaceNumber";

export default function ChristmasEvent(props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(false);
  const [displayWheel, setDisplayWheel] = useState(false);
  const [displayEndText, setDisplayEndText] = useState(false);
  const [displayErrorNoMore, setDisplayErrorNoMore] = useState(false);
  const [prx, setPrix] = useState(null);
  const [prxAmount, setPrixAmount] = useState(null);
  const [nbTicketsCadeau, setNbTicketsCadeau] = useState(0);
  const [cantClick, setCantClick] = useState("");

  useEffect(() => {
    axios
      .get(apiUrl + "/account/" + store.getAccountId() + "/gift-tickets")
      .then(({ data }) => {
        setNbTicketsCadeau(data);
      });
    window.scrollTo(0, 0);
  }, []);

  function drawRandomPrize() {
    setIsLoading(true);
    axios
      .get(apiUrl + "/utils/prize/" + store.getAccountId())
      .then(({ data }) => {
        setPrix(data.prix);
        setPrixAmount(data.qtee);
        setNbTicketsCadeau(nbTicketsCadeau - 1);
        setIsLoading(false);
        setDisplayWheel(true);
      });
  }

  function doSomething() {
    setTimeout(function () {
      setDisplayEndText(true);
    }, 4250);
    setCantClick("CC");
  }

  function forceUpdate() {
    axios
      .get(apiUrl + "/account/" + store.getAccountId() + "/gift-tickets")
      .then(({ data }) => {
        setDisplayWheel(false);
        setDisplayEndText(false);
        setPrix(null);
        setPrixAmount(null);
        setNbTicketsCadeau(data);
        setCantClick("");
        if (nbTicketsCadeau <= 0) {
          setDisplayErrorNoMore(true);
        }
        props.refreshCash();
      });
  }

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("Gift")}</header>
        <div className="displayFusions">
          <img
            alt=""
            className="imgIrma"
            src={getLocationImageByNumber(PlaceNumber.Dinoville)}
          />
          <span className="textIrma">{t("textTownXmas")}</span>
        </div>
        <div>
          <p className="text">{t("textTownXmas_p1")}</p>
          <p className="text">
            {t("youHave")}
            {nbTicketsCadeau} {t("boutique.Ticket Cadeau")}
            {"(s)."}
            {displayWheel && <> {t("clickOnWheel")}</>}
          </p>

          {displayErrorNoMore && (
            <div className="errorResultFuzPrix">{t("errorCantPlay")}</div>
          )}

          {!displayWheel && !displayErrorNoMore && (
            <button
              id="btnGive"
              className="buttonGiftPrize"
              onClick={(e) => {
                if (window.confirm(t("confirm"))) {
                  drawRandomPrize();
                }
              }}
            >
              <img alt="Gift" src={getActionImageFromActionString("Gift")} />
              <span className="verticalAlignCenter">{t("Gift") + "!"}</span>
              <img alt="Gift" src={getActionImageFromActionString("Gift")} />
            </button>
          )}

          {isLoading === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoading === false && (
            <div
              className={"textPrize" + cantClick}
              onClick={(e) => {
                doSomething();
              }}
            >
              {displayWheel && (
                <PrizeWheel
                  chosen={prx}
                  items={[
                    "Potion Ange",
                    "Pain Chaud",
                    "COINS",
                    "Ticket Champifuz Special",
                    "Ticket Champifuz",
                    "COINS",
                    "Eternity Pill",
                    "EGG_11",
                    "EGG_18",
                    "COINS",
                  ]}
                />
              )}
            </div>
          )}

          {displayWheel && displayEndText && (
            <div className="textPrize">
                {(prxAmount !== null && prxAmount === 1) && <p>{t("clickOnWheelEnd")}</p>}
                {(prxAmount !== null && prxAmount > 1) && <p>{t("win.wheel")}{prxAmount}{" " + t("phrase.buyDinoz2")}</p>}
              <button
                id="btnGive"
                className="buttonSacrifice"
                onClick={(e) => {
                  forceUpdate();
                }}
              >
                <img alt="Gift" src={getActionImageFromActionString("Gift")} />
                <span className="verticalAlignCenter">{t("rejouer")}</span>
                <img alt="Gift" src={getActionImageFromActionString("Gift")} />
              </button>
            </div>
          )}
        </div>
        <ReactTooltip
          className="largetooltip"
          html={true}
          backgroundColor="transparent"
        />
      </div>
    </div>
  );
}
