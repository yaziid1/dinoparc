import { useTranslation } from "react-i18next";
import React, { useState } from "react";
import GameSection from "./shared/GameSection";
import Button from "../../../../components/Button";
import { DialogData } from "../../../../types/dialog";
import { PlaceNumber } from "../../../utils/LocationImageByPlaceNumber";

import signDialog from "../../../../data/dialogs/signDialog";
import oldManDialog from "../../../../data/dialogs/oldManDialog";
import touristGuideDialog from "../../../../data/dialogs/touristGuideDialog";

export const placeDialogs = {
  [PlaceNumber.Clairiere]: signDialog,
  [PlaceNumber.hutte]: oldManDialog,
  [PlaceNumber.bordeciel]: touristGuideDialog,
};

export const placeWithDialogs = Object.keys(placeDialogs).map(
  (key) => Number(key) as keyof typeof placeDialogs
);

type Props = {
  placeNumber: PlaceNumber;
  returnToDinoz: () => void;
};

export default function Dialog(props: Props) {
  const { t } = useTranslation();
  const [npcText, setNpcText] = useState(0);

  const placeNumber = props.placeNumber;
  const dialogData: DialogData | null =
    placeNumber in placeDialogs ? placeDialogs[placeNumber] : null;
  const dialogId = dialogData?.id;
  const imageNpc = dialogData?.[npcText]?.img || dialogData?.img;
  const options = dialogData?.[npcText]?.options || [];

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    const selection = (e.target as HTMLFormElement).selection?.value;
    const selectedAnswer = selection ? Number(selection) : null;

    if (selectedAnswer == null) {
      return;
    }

    setNpcText(selectedAnswer);
  }

  return (
    <GameSection title={t(dialogId)} returnToDinoz={props.returnToDinoz}>
      {dialogData == null ? (
        <></>
      ) : (
        <>
          <div className="flexDivsIrma">
            <img alt="" src={imageNpc} className="imgIrma imgFixedSize" />
            <div>
              <div className="dialogNPC">
                <p className="textNpc italic">{t("N" + dialogId + npcText)}</p>
              </div>
            </div>
          </div>

          {options.length !== 0 && (
            <form onSubmit={handleSubmit}>
              {options.map((dialogKey) => (
                <div key={npcText + "_" + dialogKey} className="alignedOptions">
                  <label>
                    <input
                      type="radio"
                      name="selection"
                      required
                      value={dialogKey}
                    />

                    {t("P" + dialogId + dialogKey)}
                  </label>
                </div>
              ))}
              <div className="has-text-centered mt-4">
                <Button type="submit">{t("sayToNpc")}</Button>
              </div>
            </form>
          )}
        </>
      )}
    </GameSection>
  );
}
