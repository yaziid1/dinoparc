import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../../index";
import { MoonLoader } from "react-spinners";
import ReactSWF from "react-swf";
import ReactTooltip from "react-tooltip";
import getCorrectLevelUrlFromInteger from "../../../utils/LevelUrlByInteger";
import getLocationByNumber from "../../../utils/LocationByNumber";
import getActionImageFromActionString from "../../../utils/ActionImageFromString";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import getElementImageByString from "../../../utils/ElementImageByString";
import getElementStringByCompetence from "../../../utils/ElementNameByCompetence";
import getElementPositionByCompetence from "../../../utils/ElementPositionByCompetence";
import getTotemImageByInteger from "../../../utils/TotemImageByInteger";
import getUltraRareByInteger from "../../../utils/UltraRareImageByInteger";
import getCollectionImageByString from "../../../utils/CollectionImageByInteger";
import getLocationImageByNumber, {
  PlaceNumber,
} from "../../../utils/LocationImageByPlaceNumber";
import getHistoryImageFromKey from "../../../utils/HistoryImageFromKey";
import getPassiveIconByKey from "../../../utils/PassiveIconByKey";
import fightSwf from "../../../../media/swf/fight.swf";
import tinycoins from "../../../../media/minis/tiny_coin.gif";
import wbf from "../../../../media/game/winBannerFight.png";
import lbf from "../../../../media/game/lossBannerFight.png";
import miniElems from "../../../../media/dinoz/allCharms.png";
import fish1 from "../../../../media/cuisine/fish1.png";
import fish2 from "../../../../media/cuisine/fish2.png";
import fish3 from "../../../../media/cuisine/fish3.png";
import fish4 from "../../../../media/consumables/egg_chocolate.png";
import plant1 from "../../../../media/cuisine/picking1.png";
import plant2 from "../../../../media/cuisine/picking2.png";
import plant3 from "../../../../media/cuisine/picking3.png";
import plant4 from "../../../../media/cuisine/picking4.png";
import plant5 from "../../../../media/consumables/ticket_cadeau.gif";
import emptyDinoz from "../../../../media/dinoz/emptyDinoz.png";

import CraterShop from "./CraterShop";
import AnomalyShop from "./AnomalyShop";
import Dialog from "./Dialog";
import FusionCenter from "./FusionCenter";
import NPCDinozBuyer from "./NPCDinozBuyer";
import Sacrifices from "./Sacrifices";
import Merchant from "./Merchant";
import DinoBath from "./DinoBath";
import DinoFountain from "./DinoFountain";
import HermitArena from "./HermitArena";
import Bazar from "./Bazar";
import Credit from "./Credit";
import { Dinoz as DinozType } from "../../../../types/dinoz";
import { FightDataPayload } from "../../../../types/fight-data-payload";
import ChristmasEvent from "./ChristmasEvent";
import EggHatcher from "./EggHatcher";
import PlainsShop from "./PlainsShop";
import RaidExchange from "./RaidExchange";
import { confirmAlert } from "react-confirm-alert";
import ChaudronMayinca from "./ChaudronMayinca";
import Telescope from "./Telescope";
import DinotownMissionPost from "./DinotownMissionPost";
import SpecialEvent from "./SpecialEvent";
import HeroRareTalk from "./HeroRareTalk";
import CerbereTalk from "./CerbereTalk";
import DemonHideoutEvent from "./DemonHideoutEvent";
import DinozRenderTile from "../../shared/DinozRenderTile";
import HomeSection from "./Home";
import { useUserDinozList } from "../../../../context/userDinozList";
import ArmySection from "./ArmySection";
import Message from "../../../../components/Message";
import { Loader } from "../../../../components/Loader";
import Tournament from "./Tournament";
import FactionOzoniteBase from "./FactionOzoniteBase";
import FactionGorakBase from "./FactionGorakBase";
import FactionBuldozorBase from "./FactionBuldozorBase";

// TODO check good sections an rename sections
const enum PageSection {
  Home = 0,
  Combat,
  Fight,
  FightSummary,
  s4, // TODO: Not used!
  LevelUp,
  RespawnDinoz,
  MadameIrma,
  Catapulte,
  Barrage,
  TournoideDinoville,
  /** @deprecated use TournoideDinoville */
  TournoidesRuinesMayincas,
  /** @deprecated use TournoideDinoville */
  TournoiduTempleBouddhiste,
  /** @deprecated use TournoideDinoville */
  TournoiduMontDino,
  /** @deprecated use TournoideDinoville */
  TournoideDinoplage,
  PassageGranit,
  PruneShop,
  CraterShop,
  AnomalyShop,
  Talk,
  CentreFusions,
  NPCDinozBuyer,
  AutelSacrifices,
  Merchant,
  DinoBath,
  DinoFountain,
  TheArena,
  JazzBazar,
  CreditBank,
  EggHatcher,
  PlainsShop,
  raidExchange,
  Chaudron,
  Telescope,
  PosteMissions,
  HordesHalloweenEvent,
  HeroTalk,
  EnterDemon,
  FactionOzonite,
  FactionGorak,
  FactionBuldozor,
  CerbereTalk,
  Gift = 2512,
  Army,
}

export type ActionKey =
  | "Combat"
  | "Deplacer"
  | "Escalader"
  | "PrendrePrunayer"
  | "PrendreDeltaplane"
  | "Rock"
  | "Fouiller"
  | "Bain de Flammes"
  | "Pêche"
  | "Cueillette"
  | "Potion Irma"
  | "LevelUp"
  | "MadameIrma"
  | "Catapulte"
  | "Barrage"
  | "PassageGranit"
  | "PruneShop"
  | "CraterShop"
  | "AnomalyShop"
  | "Talk"
  | "CentreFusions"
  | "NPCDinozBuyer"
  | "AutelSacrifices"
  | "Merchant"
  | "DinoBath"
  | "DinoFountain"
  | "TheArena"
  | "JazzBazar"
  | "CreditBank"
  | "EggHatcher"
  | "PlainsShop"
  | "raidExchange"
  | "Chaudron"
  | "Telescope"
  | "PosteMissions"
  | "HordesHalloweenEvent"
  | "HeroTalk"
  | "CerbereTalk"
  | "Enter Demon"
  | "Faction_1_Ozonite_Portail"
  | "Faction_2_Gorak_Portail"
  | "Faction_3_Buldozor_Portail"
  | "Gift"
  | "TournoideDinoville"
  | "TournoidesRuinesMayincas"
  | "TournoiduTempleBouddhiste"
  | "TournoiduMontDino"
  | "TournoideDinoplage"
  | "Army_Join"
  | "Army_See"
  | "Army_Quit";
export type ProcessAction = (
  dinozId: string,
  actionId: ActionKey,
  e?: any
) => void;

type Props = {
  dinozId: string;
  reloadCurrentDinoz: boolean;
  /** @deprecated list already updated on setDinoz */
  refreshLocations: () => void;
  refreshCash: () => void;
  refreshHistory: () => void;
  refreshToInventory: () => void;
  refreshToHistory: () => void;
  refreshToShop: () => void;
  refreshAll: () => void;
};
export default function Dinoz(props: Props) {
  const { t, i18n } = useTranslation();
  const { updateDinozIntoList } = useUserDinozList();
  let store = Store.getInstance();
  let [isLoading, setIsLoading] = useState(true);
  let [isLoadingLvlUp, setIsLoadingLvlUp] = useState(false);
  let [isLoadingFight, setIsLoadingFight] = useState(false);
  let [isLoadingDig, setIsLoadingDig] = useState(false);
  let [hasDigged, setHasDigged] = useState(false);
  let [wGone, setWGone] = useState(false);
  let [ennemyHasFled, setEnnemyHasFled] = useState(false);
  let [digCode, setDigCode] = useState<string | number>("");
  let [digObject, setDigObject] = useState("");
  let [isLoadingRock, setIsLoadingRock] = useState(false);
  let [hasRocked, setHasRocked] = useState(false);
  let [noAngelError, setNoAngelError] = useState(false);
  let [rockData, setRockData] = useState("");
  let [sectionCurrentlyActive, setSectionCurrentlyActive] =
    useState<PageSection>(PageSection.Home);
  let [dinoz, _setDinoz] = useState<DinozType | null>(null);
  let [isMoving, setIsMoving] = useState(false);
  let [availableLearning, setAvailableLearning] = useState({});
  let [fightingDinozList, setFightingDinozList] = useState([]);
  let [fightDataPayload, setFightDataPayload] = useState<
    Partial<FightDataPayload>
  >({});
  let [noStockError, setNoStockError] = useState(false);
  let [success, setSuccess] = useState(false);
  let [successMsg, setSuccessMsg] = useState("");
  let [irmaBuy, setIrmaBuy] = useState(0);
  let [irmaTotalPrice, setIrmaTotalPrice] = useState(0);
  let [pruniacBuy, setPruniacBuy] = useState(0);
  let [pruniacTotalPrice, setPruniacTotalPrice] = useState(0);
  let [displayIrmaBuyError, setDisplayIrmaBuyError] = useState(null);
  let [displayPruniacBuyError, setDisplayPruniacBuyError] = useState(null);
  let [barrageSuccess, setBarrageSuccess] = useState(null);
  let [granitSuccess, setGranitSuccess] = useState();
  let [specialMoveSuccess, setSpecialMoveSuccess] = useState(false);
  let [specialMoveFailed, setSpecialMoveFailed] = useState(false);
  let [reload, setReload] = useState(false);
  let [courseMoveAgain, setCourseMoveAgain] = useState(false);
  let [isLoadingBainDeFlammes, setIsLoadingBainDeFlammes] = useState(false);
  let [hasFlamed, setHasFlamed] = useState(false);
  let [bathHealAmount, setBathHealAmount] = useState(0);
  let [isLoadingFish, setIsLoadingFish] = useState(false);
  let [hasFished, setHasFished] = useState(false);
  let [fishingCode, setFishingCode] = useState(0);
  let [isLoadingPick, setIsLoadingPick] = useState(false);
  let [hasPicked, setHasPicked] = useState(false);
  let [pickingCode, setPickingCode] = useState(0);
  let [irmaQty, setIrmaQty] = useState(0);

  const setDinoz = (newDinoz: DinozType) => {
    // TODO check a better solution
    if (JSON.stringify(newDinoz) === JSON.stringify(dinoz)) {
      return;
    }
    updateDinozIntoList(newDinoz);
    _setDinoz(newDinoz);
  };

  useEffect(() => {
    setHasRocked(false);
    setHasDigged(false);
    setHasFlamed(false);
    setHasFished(false);
    setHasPicked(false);
    setHasDigged(false);
    setCourseMoveAgain(false);
    setSuccess(false);
    setBarrageSuccess(null);
    setNoStockError(false);
    setNoAngelError(false);
    setWGone(false);
    setSpecialMoveSuccess(false);
    setSpecialMoveFailed(false);

    axios
      .get<DinozType>(
        apiUrl + "/account/" + store.getAccountId() + "/" + props.dinozId
      )
      .then(({ data }) => {
        refreshComponent(data);
        window.scrollTo(0, 0);
      });
  }, [props.dinozId, props.reloadCurrentDinoz, reload]);

  function refreshComponent(dinoz: DinozType) {
    setIsMoving(false);
    setCourseMoveAgain(false);
    setSectionCurrentlyActive(PageSection.Home);
    setDinoz(dinoz);
    setIsLoading(false);
    setBarrageSuccess(null);
    setGranitSuccess(null);
    setDisplayIrmaBuyError(null);
    setDisplayPruniacBuyError(null);
    setPruniacTotalPrice(0);
    setWGone(false);
    setIrmaQty(0);

    props.refreshLocations();
    ReactTooltip.rebuild();
  }

  function reloadMyDinoz() {
    return axios
      .get<DinozType>(
        apiUrl + "/account/" + store.getAccountId() + "/" + props.dinozId
      )
      .then(({ data }) => {
        refreshComponent(data);
        window.scrollTo(0, 0);
      });
  }

  function loadSummary() {
    props.refreshCash();
    setSectionCurrentlyActive(PageSection.FightSummary);
  }

  function getWidthFromPercentage(percentage) {
    if (percentage < 101) {
      return (percentage / 100) * 60;
    } else {
      return 60;
    }
  }

  function getSuccessMsg(response) {
    if (i18n.language === "fr") {
      return response.successMessageFr;
    } else if (i18n.language === "es") {
      return response.successMessageEs;
    } else if (i18n.language === "en") {
      return response.successMessageEn;
    }
  }

  function processFight(ennemyDinoz) {
    window.scrollTo(0, 0);
    setSectionCurrentlyActive(PageSection.Fight);
    setIsLoadingFight(true);

    if (ennemyDinoz.level === 0 && !isBotFromQuest(ennemyDinoz.id)) {
      let botTrace =
        "BOT" +
        "-" +
        ennemyDinoz.elementsValues.Feu +
        "-" +
        ennemyDinoz.elementsValues.Terre +
        "-" +
        ennemyDinoz.elementsValues.Eau +
        "-" +
        ennemyDinoz.elementsValues.Foudre +
        "-" +
        ennemyDinoz.elementsValues.Air +
        "-" +
        ennemyDinoz.appearanceCode;

      axios
        .get(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/" +
            props.dinozId +
            "/" +
            botTrace
        )
        .then(({ data }) => {
          if (data != "") {
            fightDataPayload = data;
            setFightDataPayload(fightDataPayload);
            setIsLoadingFight(false);
            props.refreshHistory();

            //Quand on clique sur "Lancer le Combat, ne pas faire un refresh de tous les Dinoz".
            //props.refreshLocations();
          } else {
            returnToDinoz();
          }
        });
    } else if (
      ennemyDinoz.id != null &&
      ennemyDinoz.id.toString().startsWith("admin-w-wild")
    ) {
      axios
        .get(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/" +
            props.dinozId +
            "/" +
            ennemyDinoz.id
        )
        .then(({ data }) => {
          if (data != "") {
            fightDataPayload = data;
            fightDataPayload = checkForKabukiName(fightDataPayload);
            setFightDataPayload(fightDataPayload);
            setIsLoadingFight(false);
            props.refreshLocations();
            props.refreshHistory();
          } else {
            returnToDinoz();
            setWGone(true);
          }
        });
    } else {
      axios
        .get(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/" +
            props.dinozId +
            "/" +
            ennemyDinoz.id
        )
        .then(({ data }) => {
          if (data != "") {
            fightDataPayload = data;
            fightDataPayload = checkForKabukiName(fightDataPayload);
            setFightDataPayload(fightDataPayload);
            setIsLoadingFight(false);
            props.refreshLocations();
            props.refreshHistory();
          } else {
            returnToDinoz();
            setEnnemyHasFled(true);
          }
        });
    }
  }

  function returnToDinoz() {
    setSpecialMoveSuccess(false);
    setSpecialMoveFailed(false);
    setSectionCurrentlyActive(PageSection.Home);
    setBarrageSuccess(null);
    setGranitSuccess(null);
    setCourseMoveAgain(false);
    reloadMyDinoz();
  }

  function getElementsInOrder(fightingDinoz: DinozType): any {
    return new Map(
      [...Object.entries(fightingDinoz.elementsValues)].sort(
        (a, b) => b[1] - a[1]
      )
    );
  }

  function getPhraseFromWinOrLoss(condition) {
    if (condition === true) {
      return t("phrase.win");
    } else {
      return t("phrase.loss");
    }
  }

  function getBannerFromFightResult(fightResult) {
    if (fightResult === true) {
      return wbf;
    } else {
      return lbf;
    }
  }

  function getCorrectEffectLang(fightDataPayload) {
    if (i18n.language === "fr") {
      return fightDataPayload.skillsEffectsFr;
    } else if (i18n.language === "es") {
      return fightDataPayload.skillsEffectsEs;
    } else if (i18n.language === "en") {
      return fightDataPayload.skillsEffectsEn;
    }
  }

  function getCorrectCharmLogLineLang(fightDataPayload) {
    if (i18n.language === "fr") {
      return fightDataPayload.charmsEffectsFr;
    } else if (i18n.language === "es") {
      return fightDataPayload.charmsEffectsEs;
    } else if (i18n.language === "en") {
      return fightDataPayload.charmsEffectsEn;
    }
  }

  function augmenterNiveau(skill) {
    axios
      .put(
        apiUrl +
          "/account/" +
          store.getAccountId() +
          "/" +
          props.dinozId +
          "/lvlup/" +
          skill
      )
      .then(() => {
        reloadMyDinoz();
        returnToDinoz();
      });
  }

  function apprendreCompetence(skill) {
    if (
      window.confirm(
        t("confirm") + " " + t("evo.apprendre") + " " + t(skill) + "?"
      )
    ) {
      axios
        .put(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/" +
            props.dinozId +
            "/learn/" +
            skill
        )
        .then(() => {
          reloadMyDinoz();
          returnToDinoz();
        });
    }
  }

  function respawnDinoz(dinozId) {
    setSectionCurrentlyActive(PageSection.RespawnDinoz);
  }

  function respawnDinozAngel() {
    axios
      .put(
        apiUrl + "/account/" + store.getAccountId() + "/ange/" + props.dinozId
      )
      .then(({ data }) => {
        noAngelError = data.noStockError;
        setNoAngelError(noAngelError);
        if (noAngelError === false) {
          reloadMyDinoz();
          returnToDinoz();
        }
      });
  }

  function respawnDinozNormal() {
    axios
      .put(
        apiUrl +
          "/account/" +
          store.getAccountId() +
          "/" +
          props.dinozId +
          "/respawn/"
      )
      .then(() => {
        setReload(!reload);
      });
  }

  function irmaChange(e) {
    setIrmaBuy(e.target.value);
    setIrmaTotalPrice(700 * e.target.value);
  }

  function pruniacChange(e) {
    setPruniacBuy(e.target.value);
    setPruniacTotalPrice(5000 * e.target.value);
  }

  function buyFromIrmaDirectly() {
    axios
      .post(
        apiUrl + "/account/" + store.getAccountId() + "/buyFromIrmaDirectly",
        {
          totalPrice: irmaTotalPrice,
          irmaBuy: irmaBuy,
        }
      )
      .then(({ data }) => {
        if (data === true) {
          axios
            .get(
              apiUrl + "/account/" + store.getAccountId() + "/inventory/irma"
            )
            .then(({ data }) => {
              setIrmaQty(data);
              window.scrollTo(0, 0);
              setDisplayIrmaBuyError(false);
              props.refreshCash();
              props.refreshHistory();
            });
        } else {
          setDisplayIrmaBuyError(true);
        }
      });
  }

  function buyPruniacDirectly() {
    axios
      .post(
        apiUrl + "/account/" + store.getAccountId() + "/buyPruniacDirectly",
        {
          totalPrice: pruniacTotalPrice,
          pruniacBuy: pruniacBuy,
        }
      )
      .then(({ data }) => {
        if (data === true) {
          setDisplayPruniacBuyError(false);
          props.refreshCash();
          props.refreshHistory();
        } else {
          setDisplayPruniacBuyError(true);
        }
      });
  }

  function takeCatapult() {
    axios
      .put(
        apiUrl +
          "/account/" +
          store.getAccountId() +
          "/" +
          props.dinozId +
          "/catapult/"
      )
      .then(() => {
        props.refreshCash();
        // props.refreshLocations();
        reloadMyDinoz();
      });
  }

  function passTheControl() {
    axios
      .put(
        apiUrl +
          "/account/" +
          store.getAccountId() +
          "/" +
          props.dinozId +
          "/control/"
      )
      .then(({ data }) => {
        if (data === true) {
          setBarrageSuccess(true);
        } else {
          setBarrageSuccess(false);
        }
      });
  }

  function passTheGranitDoor() {
    axios
      .put(
        apiUrl +
          "/account/" +
          store.getAccountId() +
          "/" +
          props.dinozId +
          "/passagegranit/"
      )
      .then(({ data }) => {
        setGranitSuccess(data);
      });
  }

  function passTheGranitDoorAndGoToCasino() {
    // props.refreshLocations();
    reloadMyDinoz();
  }

  function continueToCliff() {
    // props.refreshLocations();
    reloadMyDinoz();
  }

  function checkForKabukiName(fightDataPayload) {
    if (fightDataPayload.rightDinozName != undefined) {
      if (fightDataPayload.rightDinozName.charAt(0) == "@") {
        fightDataPayload.rightDinozName = t(fightDataPayload.rightDinozName);
        return fightDataPayload;
      } else {
        return fightDataPayload;
      }
    }
  }

  function isBotShiny(appearanceCode) {
    return appearanceCode.includes("%23");
  }

  function isBotFromQuest(botId) {
    if (botId != null && botId.toString().charAt(0) == "@") {
      return true;
    }
    if (botId != null && botId.toString().startsWith("admin-w-wild")) {
      return true;
    }
    return false;
  }

  function isBotW(botId) {
    if (botId != null && botId.toString().startsWith("admin-w-wild")) {
      return true;
    }
    return false;
  }

  function refreshAfterSpecialBuy() {
    props.refreshCash();
    props.refreshHistory();
    props.refreshToInventory();
  }

  function refreshAfterMerchantBuy() {
    props.refreshCash();
    props.refreshHistory();
  }

  function refreshAfterBathOrFountain() {
    props.refreshCash();
  }

  function refreshAfterFusion() {
    props.refreshCash();
    props.refreshHistory();
    props.refreshToInventory();
  }

  function refreshToHistory() {
    props.refreshCash();
    props.refreshHistory();
    props.refreshToHistory();
  }

  function resetMoveStateOnDinozSummary() {
    if (document.getElementById("Deplacer") !== null) {
      document
        .getElementById("Deplacer")
        .querySelector("img")
        .setAttribute("src", getActionImageFromActionString("Deplacer"));
      setIsMoving(false);
    }
  }

  const processAction: ProcessAction = (dinozId, actionId: ActionKey, e) => {
    setNoStockError(false);
    setHasRocked(false);
    setHasDigged(false);
    setHasFlamed(false);
    setHasFished(false);
    setHasPicked(false);
    setSuccess(false);
    setNoStockError(false);
    setSpecialMoveSuccess(false);
    setSpecialMoveFailed(false);
    setCourseMoveAgain(false);
    setEnnemyHasFled(false);

    if (actionId === "Combat") {
      setFightingDinozList([]);
      setIsLoadingFight(true);
      setIsMoving(false);
      setSectionCurrentlyActive(PageSection.Combat);

      axios
        .get(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/" +
            props.dinozId +
            "/availableEnnemies",
          { headers: { life: e.clientX, xp: e.clientY } }
        )
        .then(({ data }) => {
          setFightingDinozList(data);
          setIsLoadingFight(false);
        });
    } else if (actionId === "Deplacer") {
      if (!isMoving) {
        setIsMoving(true);
      } else {
        setIsMoving(false);
      }
    } else if (actionId === "Escalader" || actionId === "PrendrePrunayer") {
      if (window.confirm(t("confirm"))) {
        axios
          .get(
            apiUrl +
              "/account/" +
              store.getAccountId() +
              "/" +
              props.dinozId +
              "/specialMove/" +
              actionId
          )
          .then(({ data }) => {
            if (data === true) {
              setSpecialMoveSuccess(true);
              // props.refreshLocations();
              reloadMyDinoz();
            } else {
              setSpecialMoveFailed(true);
              reloadMyDinoz();
            }
          });
      }
    } else if (actionId === "PrendreDeltaplane") {
      if (window.confirm(t("PrendreDeltaplane") + "?")) {
        let chosenPlace = 0;
        if (
          dinoz.skillsMap["Saut"] != null &&
          dinoz.skillsMap["Saut"] > 0 &&
          Math.floor(Math.random() * 10) <= dinoz.skillsMap["Saut"]
        ) {
          let location = dinoz.placeNumber;
          if (location === 20) {
            confirmAlert({
              overlayClassName: "neoparcConfirmOverlay",
              message: t("jumpSuccess"),
              buttons: [
                {
                  label: t("location.caverneirma"),
                  onClick: () => {
                    chosenPlace = 1;
                    axios
                      .get(
                        apiUrl +
                          "/account/" +
                          store.getAccountId() +
                          "/" +
                          props.dinozId +
                          "/specialMove/" +
                          actionId +
                          "-" +
                          chosenPlace
                      )
                      .then(({ data }) => {
                        if (data === true) {
                          setSpecialMoveSuccess(true);
                          // props.refreshLocations();
                          reloadMyDinoz();
                        } else {
                          setSpecialMoveFailed(true);
                          reloadMyDinoz();
                        }
                      });
                  },
                },
                {
                  label: t("location.dinoplage"),
                  onClick: () => {
                    chosenPlace = 3;
                    axios
                      .get(
                        apiUrl +
                          "/account/" +
                          store.getAccountId() +
                          "/" +
                          props.dinozId +
                          "/specialMove/" +
                          actionId +
                          "-" +
                          chosenPlace
                      )
                      .then(({ data }) => {
                        if (data === true) {
                          setSpecialMoveSuccess(true);
                          // props.refreshLocations();
                          reloadMyDinoz();
                        } else {
                          setSpecialMoveFailed(true);
                          reloadMyDinoz();
                        }
                      });
                  },
                },
                {
                  label: t("location.porte"),
                  onClick: () => {
                    chosenPlace = 7;
                    axios
                      .get(
                        apiUrl +
                          "/account/" +
                          store.getAccountId() +
                          "/" +
                          props.dinozId +
                          "/specialMove/" +
                          actionId +
                          "-" +
                          chosenPlace
                      )
                      .then(({ data }) => {
                        if (data === true) {
                          setSpecialMoveSuccess(true);
                          // props.refreshLocations();
                          reloadMyDinoz();
                        } else {
                          setSpecialMoveFailed(true);
                          reloadMyDinoz();
                        }
                      });
                  },
                },
              ],
            });
          } else if (location === 21) {
            confirmAlert({
              overlayClassName: "neoparcConfirmOverlay",
              message: t("jumpSuccess"),
              buttons: [
                {
                  label: t("location.barrage"),
                  onClick: () => {
                    chosenPlace = 4;
                    axios
                      .get(
                        apiUrl +
                          "/account/" +
                          store.getAccountId() +
                          "/" +
                          props.dinozId +
                          "/specialMove/" +
                          actionId +
                          "-" +
                          chosenPlace
                      )
                      .then(({ data }) => {
                        if (data === true) {
                          setSpecialMoveSuccess(true);
                          // props.refreshLocations();
                          reloadMyDinoz();
                        } else {
                          setSpecialMoveFailed(true);
                          reloadMyDinoz();
                        }
                      });
                  },
                },
                {
                  label: t("location.dinoville"),
                  onClick: () => {
                    chosenPlace = 0;
                    axios
                      .get(
                        apiUrl +
                          "/account/" +
                          store.getAccountId() +
                          "/" +
                          props.dinozId +
                          "/specialMove/" +
                          actionId +
                          "-" +
                          chosenPlace
                      )
                      .then(({ data }) => {
                        if (data === true) {
                          setSpecialMoveSuccess(true);
                          // props.refreshLocations();
                          reloadMyDinoz();
                        } else {
                          setSpecialMoveFailed(true);
                          reloadMyDinoz();
                        }
                      });
                  },
                },
                {
                  label: t("location.gredins"),
                  onClick: () => {
                    chosenPlace = 8;
                    axios
                      .get(
                        apiUrl +
                          "/account/" +
                          store.getAccountId() +
                          "/" +
                          props.dinozId +
                          "/specialMove/" +
                          actionId +
                          "-" +
                          chosenPlace
                      )
                      .then(({ data }) => {
                        if (data === true) {
                          setSpecialMoveSuccess(true);
                          // props.refreshLocations();
                          reloadMyDinoz();
                        } else {
                          setSpecialMoveFailed(true);
                          reloadMyDinoz();
                        }
                      });
                  },
                },
              ],
            });
          }
        } else {
          axios
            .get(
              apiUrl +
                "/account/" +
                store.getAccountId() +
                "/" +
                props.dinozId +
                "/specialMove/" +
                actionId +
                "-"
            )
            .then(({ data }) => {
              if (data === true) {
                setSpecialMoveSuccess(true);
                // props.refreshLocations();
                reloadMyDinoz();
              } else {
                setSpecialMoveFailed(true);
                reloadMyDinoz();
              }
            });
        }
      }
    } else if (actionId === "Rock") {
      setIsLoadingRock(true);
      resetMoveStateOnDinozSummary();
      axios
        .get(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/" +
            props.dinozId +
            "/rock/" +
            i18n.language
        )
        .then(({ data }) => {
          reloadMyDinoz();
          dinoz.danger = data.newDangerValue;
          isLoadingRock = false;
          setIsLoadingRock(false);
          hasRocked = true;
          setHasRocked(hasRocked);
          rockData = data.rockSummary;
          setRockData(data.rockSummary);
        });
    } else if (actionId === "Fouiller") {
      setIsLoadingDig(true);
      resetMoveStateOnDinozSummary();
      axios
        .get(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/" +
            props.dinozId +
            "/dig/"
        )
        .then(({ data }) => {
          reloadMyDinoz();
          isLoadingDig = false;
          setIsLoadingDig(false);
          hasDigged = true;
          setHasDigged(hasDigged);
          digCode = data.digCode;
          setDigCode(digCode);
          digObject = data.digObject;
          setDigObject(digObject);
        });
    } else if (actionId === "Bain de Flammes") {
      setIsLoadingBainDeFlammes(true);
      resetMoveStateOnDinozSummary();
      axios
        .put(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/firebath/" +
            props.dinozId
        )
        .then(({ data }) => {
          reloadMyDinoz();
          setIsLoadingBainDeFlammes(false);
          setHasFlamed(true);
          setBathHealAmount(data);
        });
    } else if (actionId === "Pêche") {
      setIsLoadingFish(true);
      resetMoveStateOnDinozSummary();
      axios
        .get(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/" +
            props.dinozId +
            "/fish/"
        )
        .then(({ data }) => {
          reloadMyDinoz();
          setIsLoadingFish(false);
          setHasFished(true);
          setFishingCode(data);
        });
    } else if (actionId === "Cueillette") {
      setIsLoadingPick(true);
      resetMoveStateOnDinozSummary();
      axios
        .get(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/" +
            props.dinozId +
            "/pick/"
        )
        .then(({ data }) => {
          reloadMyDinoz();
          setIsLoadingPick(false);
          setHasPicked(true);
          setPickingCode(data);
        });
    } else if (actionId === "Potion Irma") {
      axios
        .put(
          apiUrl + "/account/" + store.getAccountId() + "/irma/" + props.dinozId
        )
        .then(({ data }) => {
          if (data.noStockError === true) {
            props.refreshToShop();
          } else {
            noStockError = data.noStockError;
            setNoStockError(noStockError);

            if (dinoz.inTourney === true) {
              setDinoz({
                ...dinoz,
                actionsMap: { ...dinoz.actionsMap, Combat: true },
              });
            } else {
              reloadMyDinoz();
            }

            success = data.success;
            setSuccess(success);
            setSuccessMsg(getSuccessMsg(data));
          }
        });
    } else if (actionId === "LevelUp") {
      setIsLoadingLvlUp(true);
      setSectionCurrentlyActive(PageSection.LevelUp);
      axios
        .get(
          apiUrl +
            "/account/" +
            store.getAccountId() +
            "/" +
            props.dinozId +
            "/lvlup/"
        )
        .then(({ data }) => {
          availableLearning = data;
          setAvailableLearning(data);
          setIsLoadingLvlUp(false);
        });
    } else if (actionId === "MadameIrma") {
      axios
        .get(apiUrl + "/account/" + store.getAccountId() + "/inventory/irma")
        .then(({ data }) => {
          setIrmaQty(data);
          window.scrollTo(0, 0);
          setSectionCurrentlyActive(PageSection.MadameIrma);
        });
    } else if (actionId === "Catapulte") {
      setSectionCurrentlyActive(PageSection.Catapulte);
    } else if (actionId === "Barrage") {
      setSectionCurrentlyActive(PageSection.Barrage);
    } else if (actionId === "PassageGranit") {
      setSectionCurrentlyActive(PageSection.PassageGranit);
    } else if (actionId === "PruneShop") {
      setSectionCurrentlyActive(PageSection.PruneShop);
    } else if (actionId === "CraterShop") {
      setSectionCurrentlyActive(PageSection.CraterShop);
    } else if (actionId === "AnomalyShop") {
      setSectionCurrentlyActive(PageSection.AnomalyShop);
    } else if (actionId === "Talk") {
      setSectionCurrentlyActive(PageSection.Talk);
    } else if (actionId === "CentreFusions") {
      setSectionCurrentlyActive(PageSection.CentreFusions);
    } else if (actionId === "NPCDinozBuyer") {
      setSectionCurrentlyActive(PageSection.NPCDinozBuyer);
    } else if (actionId === "AutelSacrifices") {
      setSectionCurrentlyActive(PageSection.AutelSacrifices);
    } else if (actionId === "Merchant") {
      setSectionCurrentlyActive(PageSection.Merchant);
    } else if (actionId === "DinoBath") {
      setSectionCurrentlyActive(PageSection.DinoBath);
    } else if (actionId === "DinoFountain") {
      setSectionCurrentlyActive(PageSection.DinoFountain);
    } else if (actionId === "TheArena") {
      setSectionCurrentlyActive(PageSection.TheArena);
    } else if (actionId === "JazzBazar") {
      setSectionCurrentlyActive(PageSection.JazzBazar);
    } else if (actionId === "CreditBank") {
      setSectionCurrentlyActive(PageSection.CreditBank);
    } else if (actionId === "EggHatcher") {
      setSectionCurrentlyActive(PageSection.EggHatcher);
    } else if (actionId === "PlainsShop") {
      setSectionCurrentlyActive(PageSection.PlainsShop);
    } else if (actionId === "raidExchange") {
      setSectionCurrentlyActive(PageSection.raidExchange);
    } else if (actionId === "Chaudron") {
      setSectionCurrentlyActive(PageSection.Chaudron);
    } else if (actionId === "Telescope") {
      setSectionCurrentlyActive(PageSection.Telescope);
    } else if (actionId === "PosteMissions") {
      setSectionCurrentlyActive(PageSection.PosteMissions);
    } else if (actionId === "HordesHalloweenEvent") {
      setSectionCurrentlyActive(PageSection.HordesHalloweenEvent);
    } else if (actionId === "HeroTalk") {
      setSectionCurrentlyActive(PageSection.HeroTalk);
    } else if (actionId === "CerbereTalk") {
      setSectionCurrentlyActive(PageSection.CerbereTalk);
    } else if (actionId === "Faction_1_Ozonite_Portail") {
      setSectionCurrentlyActive(PageSection.FactionOzonite);
    } else if (actionId === "Faction_2_Gorak_Portail") {
      setSectionCurrentlyActive(PageSection.FactionGorak);
    } else if (actionId === "Faction_3_Buldozor_Portail") {
      setSectionCurrentlyActive(PageSection.FactionBuldozor);
    } else if (actionId === "Enter Demon") {
      setSectionCurrentlyActive(PageSection.EnterDemon);
    } else if (actionId === "Gift") {
      setSectionCurrentlyActive(PageSection.Gift);
    } else if (
      [
        "TournoideDinoville",
        "TournoidesRuinesMayincas",
        "TournoiduTempleBouddhiste",
        "TournoiduMontDino",
        "TournoideDinoplage",
      ].includes(actionId)
    ) {
      setSectionCurrentlyActive(PageSection.TournoideDinoville);
    } else if (actionId === "Army_Join") {
      axios
        .post(apiUrl + "/boss/armyDinoz", {
          dinozId: props.dinozId,
          masterId: store.getAccountId(),
          bossId: dinoz.bossInfos?.bossId,
        })
        .then(({ data }) => {
          reloadMyDinoz().then(() =>
            setSectionCurrentlyActive(PageSection.Army)
          );
        });
    } else if (actionId === "Army_See") {
      reloadMyDinoz().then(() => setSectionCurrentlyActive(PageSection.Army));
    } else if (actionId === "Army_Quit") {
      axios
        .delete(
          `${apiUrl}/boss/armyDinoz/${dinoz.bossInfos?.armyDinoz?.armyDinozId}`
        )
        .then(() => {
          reloadMyDinoz();
        });
    }
  };

  function activateTournamentViewPerDinozLocation() {
    setSuccess(false);
    if (dinoz.inTourney === true) {
      setSectionCurrentlyActive(PageSection.TournoideDinoville);
    }
  }

  function refreshAllAndReturnToDinoz() {
    props.refreshAll();
    returnToDinoz();
  }

  function homeDinozStrategyLevel(dinoz, _fightingDinoz) {
    let strategyLevel = 0;
    if (
      dinoz.skillsMap["Stratégie"] !== null &&
      dinoz.skillsMap["Stratégie"] >= 1
    ) {
      strategyLevel = dinoz.skillsMap["Stratégie"];
    }
    return strategyLevel;
  }

  function goToFight(fightPayload: FightDataPayload) {
    console.log("fight :" + fightPayload.rightDinozName);
    setFightDataPayload(fightPayload);
    setSectionCurrentlyActive(PageSection.Fight);
  }

  function getElementMajeurImage(fightingDinoz) {
    return (
      <>
        <img
          alt=""
          src={getElementImageByString(
            Array.from(getElementsInOrder(fightingDinoz).keys())[0]
          )}
        />
      </>
    );
  }

  function getElementSecondImage(fightingDinoz) {
    return (
      <>
        <img
          alt=""
          src={getElementImageByString(
            Array.from(getElementsInOrder(fightingDinoz).keys())[1]
          )}
        />
      </>
    );
  }

  function getElementTierceImage(fightingDinoz) {
    return (
      <>
        <img
          alt=""
          src={getElementImageByString(
            Array.from(getElementsInOrder(fightingDinoz).keys())[2]
          )}
        />
      </>
    );
  }

  function getElementValue(fightingDinoz, index) {
    return Array.from(getElementsInOrder(fightingDinoz).values())[index];
  }

  return dinoz && dinoz.appearanceCode ? (
    <div>
      {isLoading && <Loader css="margin: 2rem auto" />}

      {sectionCurrentlyActive === PageSection.FactionOzonite && (
        <FactionOzoniteBase
          dinoz={dinoz}
          returnToDinoz={returnToDinoz}
          refreshCash={props.refreshCash}
        />
      )}

      {sectionCurrentlyActive === PageSection.FactionGorak && (
        <FactionGorakBase
          dinoz={dinoz}
          returnToDinoz={returnToDinoz}
          refreshCash={props.refreshCash}
        />
      )}

      {sectionCurrentlyActive === PageSection.FactionBuldozor && (
        <FactionBuldozorBase
          dinoz={dinoz}
          returnToDinoz={returnToDinoz}
          refreshCash={props.refreshCash}
        />
      )}

      {sectionCurrentlyActive === PageSection.Army && (
        <ArmySection
          dinoz={dinoz}
          returnToDinoz={returnToDinoz}
          processAction={processAction}
        />
      )}

      {sectionCurrentlyActive === PageSection.Gift && (
        <div>
          <ChristmasEvent refreshCash={props.refreshCash} />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.EnterDemon && (
        <DemonHideoutEvent dinoz={dinoz} returnToDinoz={returnToDinoz} />
      )}

      {sectionCurrentlyActive === PageSection.HeroTalk && (
        <div>
          <HeroRareTalk />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.CerbereTalk && (
        <div>
          <CerbereTalk />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.HordesHalloweenEvent && (
        <div>
          <SpecialEvent />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.PosteMissions && (
        <DinotownMissionPost
          dinoz={dinoz}
          returnToDinoz={returnToDinoz}
          refreshCash={props.refreshCash}
        />
      )}

      {sectionCurrentlyActive === PageSection.Telescope && (
        <div>
          <Telescope
            dinozId={props.dinozId}
            refresh={refreshAfterBathOrFountain}
          />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.Chaudron && (
        <div>
          <ChaudronMayinca dinozId={props.dinozId} />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.raidExchange && (
        <div>
          <RaidExchange
            dinozId={props.dinozId}
            refresh={refreshAfterSpecialBuy}
          />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.PlainsShop && (
        <PlainsShop
          dinozId={props.dinozId}
          refresh={refreshAfterSpecialBuy}
          returnToDinoz={returnToDinoz}
        />
      )}

      {sectionCurrentlyActive === PageSection.EggHatcher && (
        <div>
          <EggHatcher refresh={refreshAllAndReturnToDinoz} />
          <button
            className="hoverReturn"
            onClick={(e) => {
              refreshAllAndReturnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.CreditBank && (
        <div>
          <Credit refresh={refreshAfterSpecialBuy} />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.JazzBazar && (
        <div>
          <Bazar dinozId={dinoz.id} refreshToHistory={refreshToHistory} />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.TheArena && (
        <div>
          <HermitArena
            dinozId={dinoz.id}
            refreshLocations={props.refreshLocations}
            refreshHistory={props.refreshHistory}
            refreshCash={props.refreshCash}
            returnToDinoz={returnToDinoz}
          />
        </div>
      )}

      {sectionCurrentlyActive === PageSection.DinoFountain && (
        <div>
          <DinoFountain dinozId={dinoz.id} refresh={returnToDinoz} />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.DinoBath && (
        <div>
          <DinoBath dinozId={dinoz.id} refresh={refreshAfterBathOrFountain} />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.Merchant && (
        <div>
          <Merchant dinoz={dinoz} refresh={refreshAfterMerchantBuy} />
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.AutelSacrifices && (
        <Sacrifices
          dinoz={dinoz}
          refresh={refreshAfterFusion}
          returnToDinoz={returnToDinoz}
        />
      )}

      {sectionCurrentlyActive === PageSection.NPCDinozBuyer && (
        <div>
          <NPCDinozBuyer
            dinozId={props.dinozId}
            dinozLevel={dinoz.level}
            dinozName={dinoz.name}
            refresh={refreshAfterFusion}
          />
          <button
            id="returnFromFuzMenu"
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.CentreFusions && (
        <div>
          <FusionCenter
            dinozId={props.dinozId}
            dinoz={dinoz}
            refresh={refreshAfterFusion}
            refreshCash={refreshAfterBathOrFountain}
          />
          <button
            id="returnFromFuzMenu"
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.Talk && (
        <Dialog placeNumber={dinoz.placeNumber} returnToDinoz={returnToDinoz} />
      )}

      {sectionCurrentlyActive === PageSection.AnomalyShop && (
        <AnomalyShop
          dinozId={props.dinozId}
          refresh={refreshAfterSpecialBuy}
          returnToDinoz={returnToDinoz}
        />
      )}

      {sectionCurrentlyActive === PageSection.CraterShop && (
        <CraterShop
          dinozId={props.dinozId}
          refresh={refreshAfterSpecialBuy}
          returnToDinoz={returnToDinoz}
        />
      )}

      {/* TODO: set in a component */}
      {sectionCurrentlyActive === PageSection.PruneShop && (
        <div>
          <div>
            <header className="pageCategoryHeader">
              {t("boutiquePruniac")}
            </header>
            <div className="flexDivsIrma">
              <img
                alt=""
                className="imgIrma"
                src={getLocationImageByNumber(PlaceNumber.portv2)}
              />
              <p className="textIrma">{t("textBoutiquePruniac")}</p>
            </div>
            <p className="textIrma2">{t("paroleVendeurPruniac")}</p>
            <div>
              <td className="iconIrma">
                <img alt="" src={getConsumableByKey("Pruniac")} />
              </td>
              <td className="priceIrma">
                <span className="colorBlackPO">
                  {5000}
                  <img alt="" className="coin" src={tinycoins} />
                </span>
              </td>
              <td className="countIrmaShop">
                <input
                  className="numberField"
                  type="number"
                  min="0"
                  max="99"
                  id="pruniac"
                  onInput={(e) => {
                    pruniacChange(e);
                  }}
                />
              </td>
              <td className="priceIrma">
                <span className="colorBlackPO">
                  Total = {pruniacTotalPrice}
                  <img alt="" className="coin" src={tinycoins} />
                </span>
              </td>
              <td className="priceIrma">
                <button
                  onClick={(e) => {
                    buyPruniacDirectly();
                  }}
                  className="buttonGeneral"
                >
                  {t("buy")}
                </button>
              </td>
            </div>
            {displayPruniacBuyError === false && (
              <div className="fusionMessage">{t("buyPruniacGood")}</div>
            )}
            {displayPruniacBuyError === true && (
              <div className="fusionMessage">{t("buyPruniacError")}</div>
            )}
            <button
              className="hoverReturn"
              onClick={(e) => {
                returnToDinoz();
              }}
            >
              <img alt="" src={getActionImageFromActionString("🡸")} />
            </button>
          </div>
        </div>
      )}

      {/* TODO: set in a component */}
      {sectionCurrentlyActive === PageSection.PassageGranit && (
        <div>
          <div>
            <header className="pageCategoryHeader">
              {t("location.porte")}
            </header>
            <div className="flexDivsIrma">
              <img
                alt=""
                className="imgIrma"
                src={getLocationImageByNumber(PlaceNumber.porte)}
              />
              <p className="textIrma">{t("location.porte.inscriptions")}</p>
            </div>
            <br />
            <button
              onClick={() => {
                passTheGranitDoor();
              }}
              className="buttonControl"
            >
              {t("showMedal")}
            </button>

            {granitSuccess === true && (
              <div>
                <div className="fusionMessage">{t("textGraniteOpened")}</div>
                <button
                  onClick={(e) => {
                    passTheGranitDoorAndGoToCasino();
                  }}
                  className="buttonControl"
                >
                  {t("continue")}
                </button>
              </div>
            )}

            {granitSuccess === false && (
              <div>
                <Message type="danger">{t("textGraniteFailed")}</Message>
              </div>
            )}
          </div>
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {[
        PageSection.TournoideDinoplage,
        PageSection.TournoiduMontDino,
        PageSection.TournoiduTempleBouddhiste,
        PageSection.TournoidesRuinesMayincas,
        PageSection.TournoideDinoville,
      ].includes(sectionCurrentlyActive) && (
        <Tournament
          dinoz={dinoz}
          processAction={processAction}
          returnToDinoz={returnToDinoz}
          goToFight={goToFight}
          refreshHistory={props.refreshHistory}
          reloadMyDinoz={reloadMyDinoz}
        />
      )}

      {/* TODO: set in a component */}
      {sectionCurrentlyActive === PageSection.Barrage && (
        <div>
          <header className="pageCategoryHeader">
            {t("location.barrage")}
          </header>
          <div className="flexDivsIrma">
            <img
              alt=""
              className="imgIrma"
              src={getLocationImageByNumber(PlaceNumber.Barrage)}
            ></img>
            <div>
              <p className="textIrma">{t("textBarrage")}</p>
              <div className="radioChoices">
                <input type="checkbox" />
                <label>{t("bar1")}</label>
                <br></br>
                <input type="checkbox" checked={true} />
                <label>{t("bar2")}</label>
                <br></br>
                <input type="checkbox" />
                <label>{t("bar3")}</label>
                <br></br>
                <input type="checkbox" />
                <label>{t("bar4")}</label>
                <br></br>
              </div>
            </div>
          </div>
          <br></br>
          <button
            onClick={(e) => {
              passTheControl();
            }}
            className="buttonControl"
          >
            {t("declare")}
          </button>
          <br></br>
          {barrageSuccess === true && (
            <div>
              <div className="fusionMessage">{t("controlSuccess")}</div>
              <button
                onClick={(e) => {
                  continueToCliff();
                }}
                className="buttonControl"
              >
                {t("continue")}
              </button>
            </div>
          )}
          {barrageSuccess === false &&
            !dinoz.malusList.includes("effect_poison") &&
            !dinoz.malusList.includes("effect_virus") && (
              <div>
                <div className="fusionMessage">{t("controlFailed")}</div>
                <br></br>
              </div>
            )}

          {((barrageSuccess === false &&
            dinoz.malusList.includes("effect_poison")) ||
            dinoz.malusList.includes("effect_virus")) && (
            <div>
              <div className="fusionMessage">{t("controlFailed.effects")}</div>
              <br></br>
            </div>
          )}
          <button
            className="hoverReturn"
            onClick={(e) => {
              returnToDinoz();
            }}
          >
            <img alt="" src={getActionImageFromActionString("🡸")} />
          </button>
        </div>
      )}

      {/* TODO: set in a component */}
      {sectionCurrentlyActive === PageSection.Catapulte && (
        <div>
          <div>
            <header className="pageCategoryHeader">{t("Catapulte")}</header>
            <div className="flexDivsIrma">
              <img
                alt=""
                className="imgIrma"
                src={getLocationImageByNumber(PlaceNumber.islandView)}
              />
              <p className="textIrma">{t("textCatapulte")}</p>
            </div>
            <p
              onClick={(e) => {
                takeCatapult();
              }}
              className="textCatapulte"
            >
              {t("goIsland")}
              <span className="colorBlackPOCatapulte">
                {50}
                <img alt="" className="coin" src={tinycoins} />
              </span>
            </p>
            <button
              className="hoverReturn"
              onClick={(e) => {
                returnToDinoz();
              }}
            >
              <img alt="" src={getActionImageFromActionString("🡸")} />
            </button>
          </div>
        </div>
      )}

      {/* TODO: set in a component */}
      {sectionCurrentlyActive === PageSection.MadameIrma && (
        <div>
          <div>
            <header className="pageCategoryHeader">{t("boutiqueMI")}</header>
            <div className="flexDivsIrma">
              <img
                alt=""
                className="imgIrma"
                src={getLocationImageByNumber(PlaceNumber.Imra)}
              />
              <p className="textIrma">{t("textIrma")}</p>
            </div>
            <p className="textIrma2">{t("textIrma2")}</p>
            <div>
              <td className="iconIrma">
                <img alt="" src={getConsumableByKey("Potion Irma")} />
              </td>
              <td className="priceIrma">
                <span className="colorBlackPO">
                  {700}
                  <img alt="" className="coin" src={tinycoins} />
                </span>
              </td>
              <td className="countIrmaShop">
                <input
                  className="numberField"
                  type="number"
                  min="0"
                  max="99"
                  id="irma"
                  onInput={(e) => {
                    irmaChange(e);
                  }}
                />
              </td>
              <td className="priceIrma">
                <span className="colorBlackPO">
                  Total = {irmaTotalPrice}
                  <img alt="" className="coin" src={tinycoins} />
                </span>
              </td>
              <td className="priceIrma">
                <button
                  onClick={(e) => {
                    buyFromIrmaDirectly();
                  }}
                  className="buttonGeneral"
                >
                  {t("buy")}
                </button>
              </td>
            </div>
            <div className="irmaQtyCaverne">
              <div className="possession">
                {t("boutique.possession")}
                {irmaQty}
              </div>
            </div>
            {displayIrmaBuyError === false && (
              <div className="fusionMessage">{t("buyIrmaGood")}</div>
            )}
            {displayIrmaBuyError === true && (
              <div className="errorResultFuzPrix">{t("buyIrmaError")}</div>
            )}
            <button
              className="hoverReturn"
              onClick={(e) => {
                returnToDinoz();
              }}
            >
              <img alt="" src={getActionImageFromActionString("🡸")} />
            </button>
          </div>
        </div>
      )}

      {/* TODO: set in a component */}
      {sectionCurrentlyActive === PageSection.RespawnDinoz && (
        <div>
          <div>
            <header className="pageCategoryHeader">{t("respawn")}</header>
            <p className="text">{t("respawn.title")}</p>
            <h2 className="miniHeaders">
              {t("respawn.choix")}
              {"1"}
            </h2>
            <p className="text">{t("respawn.choix1")}</p>
            <div
              className="actionDiv"
              onClick={(e) => {
                respawnDinozAngel();
              }}
            >
              <img
                alt=""
                className="buyButtonBox"
                src={getActionImageFromActionString("Potion Ange")}
              />
              <span className="resThisDinoz">{t("respawn.ange")}</span>
            </div>

            {noAngelError && (
              <Message type="danger">{t("respawn.noangel")}</Message>
            )}

            <h2 className="miniHeaders">
              {t("respawn.choix")}
              {"2"}
            </h2>
            <p className="text">{t("respawn.choix2")}</p>
            <div
              className="actionDiv"
              onClick={(e) => {
                respawnDinozNormal();
              }}
            >
              <img
                alt=""
                className="buyButtonBox"
                src={getActionImageFromActionString("Default")}
              />
              <span className="resThisDinoz">{t("respawn.default")}</span>
            </div>

            <h2 className="miniHeaders">
              {t("continueFiche")} {dinoz.name}
            </h2>
            <button
              className="hoverReturn"
              onClick={(e) => {
                returnToDinoz();
              }}
            >
              <img alt="" src={getActionImageFromActionString("🡸")} />
            </button>
          </div>
        </div>
      )}

      {/* TODO: set in a component */}
      {sectionCurrentlyActive === PageSection.LevelUp && (
        <div>
          {isLoadingLvlUp === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoadingLvlUp === false && (
            <div>
              <header className="pageCategoryHeader">{t("LevelUp")}</header>
              <p className="winLevelText">
                {dinoz.name}
                {t("lvlup")}
              </p>
              <div className="picCenterFightPlacer">
                <div className="picCenterFight">
                  <table>
                    <tbody>
                      <tr>
                        <td className="charms">
                          <p
                            className="passiveTitle"
                            data-place="right"
                            data-tip={t("tooltip.bonus")}
                          >
                            {t("bonusLabel")}
                          </p>
                          {Object.keys(dinoz.passiveList).map(function (key) {
                            return (
                              <>
                                {dinoz.passiveList[key] > 0 && (
                                  <img
                                    alt=""
                                    className="transformCharm"
                                    src={getPassiveIconByKey(key)}
                                    data-place="right"
                                    data-tip={
                                      t("tooltip." + key) +
                                      t("tooltip.quantity") +
                                      dinoz.passiveList[key] +
                                      "</strong>"
                                    }
                                  ></img>
                                )}
                              </>
                            );
                          })}
                        </td>
                        <td>
                          <div className="dinoSheetWrap">
                            <DinozRenderTile
                              appCode={dinoz.appearanceCode}
                              size={100}
                            />
                          </div>
                        </td>
                        <td className="charms">
                          <p
                            className="passiveTitle"
                            data-place="right"
                            data-tip={t("tooltip.autre")}
                          >
                            {t("autreLabel")}
                          </p>
                          {dinoz.malusList.map((passive, i) => {
                            return (
                              <img
                                alt=""
                                className="transformCharm"
                                src={getPassiveIconByKey(passive)}
                                data-place="right"
                                data-tip={t("tooltip." + passive)}
                              ></img>
                            );
                          })}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div>
                  <div className="elementsDisplay">
                    <div className="elementsLiFiche">
                      <img alt="" src={getElementImageByString("Feu")} />{" "}
                      {dinoz.elementsValues.Feu}
                    </div>
                    <div className="elementsLiFiche">
                      <img alt="" src={getElementImageByString("Terre")} />{" "}
                      {dinoz.elementsValues.Terre}
                    </div>
                    <div className="elementsLiFiche">
                      <img alt="" src={getElementImageByString("Eau")} />{" "}
                      {dinoz.elementsValues.Eau}
                    </div>
                    <div className="elementsLiFiche">
                      <img alt="" src={getElementImageByString("Foudre")} />{" "}
                      {dinoz.elementsValues.Foudre}
                    </div>
                    <div className="elementsLiFiche">
                      <img alt="" src={getElementImageByString("Air")} />{" "}
                      {dinoz.elementsValues.Air}
                    </div>
                  </div>
                  <table className="defFight">
                    <tbody>
                      <tr className="minibottom">
                        <th className="dinoTh">{t("vie")}</th>
                        <td className="dinoTd">
                          <div className="bar">
                            {dinoz.life <= 0 ? (
                              <div className="skin" />
                            ) : (
                              <div className="skin">
                                <div className="start" />
                                <div
                                  className="fill"
                                  style={{
                                    width: getWidthFromPercentage(dinoz.life),
                                  }}
                                />
                              </div>
                            )}
                          </div>
                          <span className="percentages">{dinoz.life}%</span>
                        </td>
                      </tr>

                      <tr className="minibottom">
                        <th className="dinoTh">{t("niveau")}</th>
                        <td className="dinoTd">
                          <span className="minispace">{dinoz.level}</span>
                        </td>
                      </tr>

                      <tr className="minibottom">
                        <th className="dinoTh">{t("Suivant")}</th>
                        <td className="dinoTd">
                          <div className="bar">
                            {dinoz.experience <= 0 ? (
                              <div className="skin" />
                            ) : (
                              <div className="skin">
                                <div className="start" />
                                <div
                                  className="fill"
                                  style={{
                                    width: getWidthFromPercentage(
                                      dinoz.experience
                                    ),
                                  }}
                                />
                              </div>
                            )}
                          </div>
                          <span className="percentages">
                            {dinoz.experience}%
                          </span>
                        </td>
                      </tr>

                      <tr className="minibottom">
                        <th className="dinoTh">{t("Danger")}</th>
                        <td className="dinoTd">
                          <span className="minispace">{dinoz.danger}</span>
                          <span
                            className="imageSpan"
                            lang={i18n.language}
                            data-place="right"
                            data-tip={t("tooltip.danger")}
                          />
                          <ReactTooltip
                            className="largetooltip"
                            html={true}
                            backgroundColor="transparent"
                          />
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <table className="evoTable">
                <tbody>
                  <tr className="evoTableHeaders">
                    <td className="evoInfo">Infos</td>
                    <td className="evoName">{t("evo.name")}</td>
                    <td className="evoElem">{t("evo.element")}</td>
                    <td className="evoNiv">{t("evo.niveau")}</td>
                    <td className="evoEvo">{t("evo.evolution")}</td>
                  </tr>

                  {Object.keys(dinoz.skillsMap)
                    .sort((a, b) =>
                      getElementPositionByCompetence(a).localeCompare(
                        getElementPositionByCompetence(b)
                      )
                    )
                    .map(function (key) {
                      return (
                        <tr className="evoEntry">
                          {dinoz.skillsMap[key] > 0 && (
                            <td className="evoInfoData">
                              <span
                                className="imageSpan"
                                lang={i18n.language}
                                data-place="right"
                                data-tip={t("tooltip." + key)}
                              />
                            </td>
                          )}
                          {dinoz.skillsMap[key] > 0 && (
                            <td className="evoNameData">{t(key)}</td>
                          )}
                          {dinoz.skillsMap[key] > 0 && (
                            <td className="evoElemData">
                              <img
                                alt=""
                                className="evoImg"
                                src={getElementImageByString(
                                  getElementStringByCompetence(key)
                                )}
                              />
                            </td>
                          )}
                          {dinoz.skillsMap[key] > 0 && (
                            <td className="evoNivData">
                              <img
                                alt=""
                                className="evoImg"
                                src={getCorrectLevelUrlFromInteger(
                                  dinoz.skillsMap[key]
                                )}
                              />
                            </td>
                          )}
                          {dinoz.skillsMap[key] > 0 &&
                            dinoz.skillsMap[key] < 5 && (
                              <td
                                className="evoEvoData"
                                onClick={(e) => {
                                  augmenterNiveau(key);
                                }}
                              >
                                {t("evo.augmenter")}
                              </td>
                            )}
                          {dinoz.skillsMap[key] === 5 && (
                            <td className="evoEvoDataMaximum">Maximum</td>
                          )}
                        </tr>
                      );
                    })}
                  {Object.keys(availableLearning)
                    .sort((a, b) =>
                      getElementPositionByCompetence(a).localeCompare(
                        getElementPositionByCompetence(b)
                      )
                    )
                    .map(function (key) {
                      return (
                        <tr className="evoEntry">
                          {availableLearning[key] === 1 && (
                            <td className="evoInfoData">
                              <span
                                className="imageSpan"
                                lang={i18n.language}
                                data-place="right"
                                data-tip={t("tooltip." + key)}
                              />
                            </td>
                          )}
                          {availableLearning[key] === 1 && (
                            <td className="evoNameData">{t(key)}</td>
                          )}
                          {availableLearning[key] === 1 && (
                            <td className="evoElemData">
                              <img
                                alt=""
                                className="evoImg"
                                src={getElementImageByString(
                                  getElementStringByCompetence(key)
                                )}
                              />
                            </td>
                          )}
                          {availableLearning[key] === 1 && (
                            <td className="evoNivData">
                              {t("evo.notlearned")}
                            </td>
                          )}
                          {availableLearning[key] === 1 && (
                            <td
                              className="evoEvoDataLearn"
                              onClick={(e) => {
                                apprendreCompetence(key);
                              }}
                            >
                              {t("evo.apprendre")}
                            </td>
                          )}
                          <ReactTooltip
                            className="largetooltip"
                            html={true}
                            backgroundColor="transparent"
                          />
                        </tr>
                      );
                    })}
                </tbody>
                <ReactTooltip
                  className="largetooltip"
                  html={true}
                  backgroundColor="transparent"
                />
              </table>

              <button
                className="hoverReturn"
                onClick={(e) => {
                  returnToDinoz();
                }}
              >
                <img alt="" src={getActionImageFromActionString("🡸")} />
              </button>
            </div>
          )}
        </div>
      )}

      {/* TODO: set in a component */}
      {sectionCurrentlyActive === PageSection.FightSummary && (
        <div>
          {isLoadingFight === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoadingFight === false && (
            <div className="battle">
              <header className="pageCategoryHeader">{t("bilan")}</header>
              <div className="result">
                {t("VotreDinoz")}{" "}
                {" " +
                  fightDataPayload.leftDinozName +
                  getPhraseFromWinOrLoss(fightDataPayload.haveWon)}
                {" (" +
                  fightDataPayload.scoreFinalHome +
                  " : " +
                  +fightDataPayload.scoreFinalEnnemy +
                  ")"}
              </div>
              <table className="layout">
                <tbody>
                  <div className="flexDivs">
                    <img
                      alt=""
                      src={getBannerFromFightResult(fightDataPayload.haveWon)}
                      className="columnWinner"
                    />
                    <div className="rewardsOffset">
                      <div className="layoutRewards">
                        <img
                          alt=""
                          className="imgSummary"
                          src={getHistoryImageFromKey("hist_fight.gif")}
                        />
                        <div className="overflowHidden">
                          <span className="textSummary">
                            <span>{t("vieperdue")}</span>
                            <br />
                            <span className="colorBlack">
                              {"-"}
                              {fightDataPayload.finalLifeLoss}
                              {" %"}
                            </span>
                            <br />
                            <span className="colorBeige">
                              {t("dinozadverse")}
                            </span>
                            <br />
                            <span className="colorBlack">
                              {fightDataPayload.rightDinozName +
                                " (" +
                                t("controlepar")}
                            </span>
                            <span className="masterName">
                              {fightDataPayload.ennemyMasterName}
                            </span>
                            <span className="colorBlackNP">{")"}</span>
                          </span>
                        </div>
                      </div>

                      <div className="layoutRewards">
                        <img
                          alt=""
                          className="imgSummary"
                          src={getHistoryImageFromKey("hist_xp.gif")}
                        />
                        <div className="overflowHidden">
                          <span className="textSummary">
                            <span>{t("expgagnée")}</span>
                            <br />
                            <span className="colorBlack">
                              {"+"}
                              {fightDataPayload.experienceWon}
                              {" %"}
                            </span>
                          </span>
                        </div>
                      </div>

                      <div className="layoutRewards">
                        <img
                          alt=""
                          className="imgSummary"
                          src={getHistoryImageFromKey("hist_error.gif")}
                        />
                        <div className="overflowHidden">
                          <span className="textSummary">
                            <span>{t("dangerAcc")}</span>
                            <br />
                            {fightDataPayload.camouflageBuff === 0 && (
                              <span className="colorBlack">
                                {"+"}
                                {fightDataPayload.dangerAccumulated}{" "}
                                {t("points")}
                              </span>
                            )}

                            {fightDataPayload.camouflageBuff > 0 && (
                              <span className="colorBlack">
                                {"+"}
                                {fightDataPayload.dangerAccumulated -
                                  fightDataPayload.camouflageBuff}{" "}
                                {t("points")}
                              </span>
                            )}
                          </span>
                        </div>
                      </div>

                      <div className="layoutRewards">
                        <img
                          alt=""
                          className="imgSummary"
                          src={getHistoryImageFromKey("hist_buy.gif")}
                        />
                        <div className="overflowHidden">
                          <span className="textSummary">
                            <span>{t("potrouvées")}</span>
                            <br />
                            <span className="colorBlack">{"+"}</span>
                            <span className="colorBlackPO">
                              {fightDataPayload.moneyWon}
                              <img alt="" className="coin" src={tinycoins} />
                            </span>
                          </span>
                        </div>
                      </div>

                      {fightDataPayload.stolenObject != null && (
                        <div className="layoutRewards">
                          <img
                            alt=""
                            className="imgSummary"
                            src={getConsumableByKey(
                              fightDataPayload.stolenObject.toString()
                            )}
                          />
                          <div className="overflowHidden">
                            <span className="textSummary">
                              <span>{t("objetVole")}</span>
                              <br />
                              <span className="colorBlack">
                                {"+1 "}
                                {t(
                                  "boutique." +
                                    fightDataPayload.stolenObject.toString()
                                )}
                              </span>
                            </span>
                          </div>
                        </div>
                      )}

                      {fightDataPayload.charmsEffectsFr.length > 0 && (
                        <div className="layoutRewards">
                          <img
                            alt=""
                            className="imgSummary"
                            src={getHistoryImageFromKey("hist_charm.gif")}
                          />
                          <div className="overflowHidden">
                            <span className="textSummary">
                              <span>{t("charmsUsed")}</span>
                              <br></br>
                              {getCorrectCharmLogLineLang(fightDataPayload).map(
                                function (effect, idx) {
                                  return (
                                    <ul className="marginList">
                                      <li
                                        className="listBlack"
                                        dangerouslySetInnerHTML={{
                                          __html: effect,
                                        }}
                                      />
                                    </ul>
                                  );
                                }
                              )}
                            </span>
                          </div>
                        </div>
                      )}

                      <div className="layoutRewards">
                        <img
                          alt=""
                          className="imgSummary"
                          src={getHistoryImageFromKey("hist_report.gif")}
                        />
                        <div className="overflowHidden">
                          {fightDataPayload.skillsEffectsFr.length > 0 && (
                            <span className="textSummary">
                              <span>{t("rapportcombat")}</span>
                              <br></br>
                              {getCorrectEffectLang(fightDataPayload).map(
                                function (effect, idx) {
                                  return (
                                    <ul className="marginList">
                                      <li
                                        className="listBlack"
                                        dangerouslySetInnerHTML={{
                                          __html: effect,
                                        }}
                                      />
                                    </ul>
                                  );
                                }
                              )}
                            </span>
                          )}
                          {fightDataPayload.skillsEffectsFr.length === 0 && (
                            <span className="textSummary">
                              <span>{t("rapportcombat")}</span>
                              <br />
                              <ul>
                                <li className="listBlack">
                                  {t("nothingToSignale")}
                                </li>
                              </ul>
                            </span>
                          )}
                        </div>
                      </div>

                      <div
                        className="returnFiche"
                        onClick={(e) => {
                          reloadMyDinoz();
                        }}
                      >
                        <p>
                          {t("continueFiche") + fightDataPayload.leftDinozName}
                        </p>
                      </div>
                    </div>
                  </div>
                </tbody>
              </table>
            </div>
          )}
        </div>
      )}

      {/* TODO: set in a component */}
      {sectionCurrentlyActive === PageSection.Fight && (
        <div>
          {isLoadingFight === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoadingFight === false && (
            <div>
              <header className="pageCategoryHeader">
                {t("Combat") +
                  ": " +
                  fightDataPayload.leftDinozName +
                  " vs " +
                  fightDataPayload.rightDinozName}
              </header>
              {store.getImgMode() === "RUFFLE" && (
                <ReactSWF
                  className="swf"
                  src={fightSwf}
                  width="580"
                  height="363"
                  flashVars={
                    "swf_url=/fight.swf" +
                    "&r=" +
                    fightDataPayload.leftDinozAppCode +
                    ":" +
                    fightDataPayload.rightDinozAppCode +
                    ":" +
                    fightDataPayload.leftDinozName +
                    ":" +
                    fightDataPayload.rightDinozName +
                    ":0:0" +
                    ":" +
                    fightDataPayload.initialHpLeft +
                    ":" +
                    fightDataPayload.initialHpRight +
                    ":" +
                    fightDataPayload.firstElementLeft +
                    ":" +
                    fightDataPayload.firstElementRight +
                    ":" +
                    fightDataPayload.roundOneScore +
                    ":" +
                    fightDataPayload.charmFirstRoundForLoser +
                    ":" +
                    fightDataPayload.lifeLossFirstRoundByLoser +
                    ":" +
                    fightDataPayload.secondElementLeft +
                    ":" +
                    fightDataPayload.secondElementRight +
                    ":" +
                    fightDataPayload.roundTwoScore +
                    ":" +
                    fightDataPayload.charmSecondRoundForLoser +
                    ":" +
                    fightDataPayload.lifeLossSecondRoundByLoser +
                    ":" +
                    fightDataPayload.thirdElementLeft +
                    ":" +
                    fightDataPayload.thirdElementRight +
                    ":" +
                    fightDataPayload.roundThreeScore +
                    ":" +
                    fightDataPayload.charmThirdRoundForLoser +
                    ":" +
                    fightDataPayload.lifeLossThirdRoundByLoser +
                    ":" +
                    fightDataPayload.leftDinozBeginMessage +
                    ":" +
                    fightDataPayload.rightDinozBeginMessage +
                    ":" +
                    fightDataPayload.leftDinozEndMessage +
                    ":" +
                    fightDataPayload.rightDinozEndMessage +
                    "&dino_url=dinoz.swf"
                  }
                  allowScriptAccess="never"
                />
              )}
              {store.getImgMode() === "NONE" && (
                <img alt="" src={emptyDinoz} width="580" height="363" />
              )}
              {store.getImgMode() === "BASE_64" && (
                <img alt="" src={emptyDinoz} width="580" height="363" />
              )}
              <button
                className="continueAfterFight"
                onClick={(e) => {
                  loadSummary();
                }}
              >
                <p className="textContinueFight">{t("continueBilan")}</p>
              </button>
            </div>
          )}
        </div>
      )}

      {/* TODO: set in a component */}
      {sectionCurrentlyActive === PageSection.Combat && (
        <div>
          <header className="pageCategoryHeader">
            {t("VotreDinoz") + " (" + dinoz.name + ")"}
          </header>
          {isLoadingFight === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoadingFight === false && (
            <div className="flex">
              <div className="picCenterFightPlacer">
                <div className="picCenterFight">
                  <table>
                    <tbody>
                      <tr>
                        <td className="charms">
                          <p
                            className="passiveTitle"
                            data-place="right"
                            data-tip={t("tooltip.bonus")}
                          >
                            {t("bonusLabel")}
                          </p>
                          {Object.keys(dinoz.passiveList).map(function (key) {
                            return (
                              <>
                                {dinoz.passiveList[key] > 0 && (
                                  <img
                                    alt=""
                                    className="transformCharm"
                                    src={getPassiveIconByKey(key)}
                                    data-place="right"
                                    data-tip={
                                      t("tooltip." + key) +
                                      t("tooltip.quantity") +
                                      dinoz.passiveList[key] +
                                      "</strong>"
                                    }
                                  ></img>
                                )}
                              </>
                            );
                          })}
                        </td>
                        <td>
                          <div className="dinoSheetWrap">
                            <DinozRenderTile
                              appCode={dinoz.appearanceCode}
                              size={100}
                            />
                          </div>
                        </td>
                        <td className="charms">
                          <p
                            className="passiveTitle"
                            data-place="right"
                            data-tip={t("tooltip.autre")}
                          >
                            {t("autreLabel")}
                          </p>
                          {dinoz.malusList.map((passive, i) => {
                            return (
                              <img
                                alt=""
                                className="transformCharm"
                                src={getPassiveIconByKey(passive)}
                                data-place="right"
                                data-tip={t("tooltip." + passive)}
                              ></img>
                            );
                          })}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div>
                  <div className="elementsDisplay">
                    <div className="elementsLiFiche">
                      <img alt="" src={getElementImageByString("Feu")} />{" "}
                      {dinoz.elementsValues.Feu}
                    </div>
                    <div className="elementsLiFiche">
                      <img alt="" src={getElementImageByString("Terre")} />{" "}
                      {dinoz.elementsValues.Terre}
                    </div>
                    <div className="elementsLiFiche">
                      <img alt="" src={getElementImageByString("Eau")} />{" "}
                      {dinoz.elementsValues.Eau}
                    </div>
                    <div className="elementsLiFiche">
                      <img alt="" src={getElementImageByString("Foudre")} />{" "}
                      {dinoz.elementsValues.Foudre}
                    </div>
                    <div className="elementsLiFiche">
                      <img alt="" src={getElementImageByString("Air")} />{" "}
                      {dinoz.elementsValues.Air}
                    </div>
                  </div>
                  <table className="defFight">
                    <tbody>
                      <tr className="minibottomVD">
                        <th className="dinoTh">{t("vie")}</th>
                        <td className="dinoTd">
                          <div className="bar">
                            {dinoz.life <= 0 ? (
                              <div className="skin"></div>
                            ) : (
                              <div className="skin">
                                <div className="start" />
                                <div
                                  className="fill"
                                  style={{
                                    width: getWidthFromPercentage(dinoz.life),
                                  }}
                                />
                              </div>
                            )}
                          </div>
                          <span className="percentages">{dinoz.life}%</span>
                        </td>
                      </tr>

                      <tr className="minibottomVD">
                        <th className="dinoTh">{t("niveau")}</th>
                        <td className="dinoTd">
                          <span className="minispace">{dinoz.level}</span>
                        </td>
                      </tr>

                      <tr className="minibottomVD">
                        <th className="dinoTh">{t("Suivant")}</th>
                        <td className="dinoTd">
                          <div className="bar">
                            {dinoz.experience <= 0 ? (
                              <div className="skin" />
                            ) : (
                              <div className="skin">
                                <div className="start" />
                                <div
                                  className="fill"
                                  style={{
                                    width: getWidthFromPercentage(
                                      dinoz.experience
                                    ),
                                  }}
                                />
                              </div>
                            )}
                          </div>
                          <span className="percentages">
                            {dinoz.experience}%
                          </span>
                        </td>
                      </tr>

                      <tr className="minibottomVD">
                        <th className="dinoTh">{t("Danger")}</th>
                        <td className="dinoTd">
                          <span className="minispace">{dinoz.danger}</span>
                          <span
                            className="imageSpan"
                            lang={i18n.language}
                            data-place="right"
                            data-tip={t("tooltip.danger")}
                          />
                          <ReactTooltip
                            className="largetooltip"
                            html={true}
                            backgroundColor="transparent"
                          />
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <img
                onClick={(e) => {
                  returnToDinoz();
                }}
                alt=""
                className="imgLieuxFight"
                src={getLocationImageByNumber(dinoz.placeNumber)}
              />
            </div>
          )}

          <header className="pageCategoryHeader">{t("adversaires")}</header>
          <div className="textbox">
            <span className="fightDescription">{t("adversaires_text")}</span>
          </div>
          <div>
            {fightingDinozList.map(function (fightingDinoz, idx) {
              return (
                <div className="chapter" key={idx}>
                  <div className="borderDinozFight">
                    <div className="tbodyFight">
                      <td className="picBoxFight">
                        <div className="dinoSheet">
                          <div className="dinoSheetWrapFight">
                            <DinozRenderTile
                              appCode={fightingDinoz.appearanceCode}
                              size={100}
                            />
                          </div>
                        </div>
                      </td>
                      <td className="infoBoxFight">
                        <div className="fiche">{t("fiche")}</div>
                        <table className="ficheTableFight">
                          <tbody>
                            <tr>
                              <th>Dinoz</th>
                              <th className="menuDinozFiche">
                                {fightingDinoz.level === 0 &&
                                  isBotFromQuest(fightingDinoz.id) && (
                                    <span className="paddingRace">
                                      {t(fightingDinoz.id)}
                                    </span>
                                  )}
                                {fightingDinoz.level === 0 &&
                                  !isBotFromQuest(fightingDinoz.id) && (
                                    <span className="paddingRace">
                                      {fightingDinoz.race}
                                    </span>
                                  )}
                                {fightingDinoz.level > 0 && (
                                  <span className="paddingRace">
                                    {fightingDinoz.name}
                                  </span>
                                )}
                              </th>
                            </tr>
                            <tr>
                              <th>{t("niveau")}</th>
                              <th className="menuDinozFicheNiveau">
                                {fightingDinoz.level === 0 &&
                                  !fightingDinoz.appearanceCode.startsWith(
                                    "F"
                                  ) &&
                                  !fightingDinoz.appearanceCode.startsWith(
                                    "H"
                                  ) && (
                                    <a className="levelColor">{dinoz.level}</a>
                                  )}
                                {fightingDinoz.level === 0 &&
                                  fightingDinoz.appearanceCode.startsWith(
                                    "F"
                                  ) && <a className="levelColorWis">{"???"}</a>}
                                {fightingDinoz.level === 0 &&
                                  fightingDinoz.appearanceCode.startsWith(
                                    "H"
                                  ) && (
                                    <a className="levelColorKab">
                                      {t("inconnu")}
                                    </a>
                                  )}
                                {fightingDinoz.level > 0 && (
                                  <a className="levelColor">
                                    {fightingDinoz.level}
                                  </a>
                                )}
                              </th>
                            </tr>
                            <tr>
                              {fightingDinoz.level === 0 && (
                                <th>{t("sauvage")}</th>
                              )}
                              {fightingDinoz.level > 0 &&
                                fightingDinoz.id !== "admin-b-raid" && (
                                  <>
                                    <th>{t("maitre")}</th>
                                    <th className="playerLink">
                                      {fightingDinoz.masterName}
                                    </th>
                                  </>
                                )}
                              {fightingDinoz.level > 0 &&
                                fightingDinoz.id === "admin-b-raid" && (
                                  <th>{t("sauvage")}</th>
                                )}
                            </tr>
                          </tbody>
                        </table>
                        <div className="attack">
                          <div
                            className="lancercombat"
                            onClick={(e) => {
                              processFight(fightingDinoz);
                            }}
                          >
                            {t("lancercombat")}
                          </div>
                        </div>
                      </td>

                      <td className="infoBoxRight">
                        <div className="ficheInfosComp">{t("infoComp")}</div>

                        {fightingDinoz.level > 0 &&
                          fightingDinoz.id !== "admin-b-raid" &&
                          fightingDinoz.taggedAsClanEnnemy &&
                          getElementMajeurImage(fightingDinoz) && (
                            <a className="clanEnemy">{t("clanEnemy")}</a>
                          )}

                        <table className="ficheTableFight">
                          <tbody>
                            <tr>
                              {(fightingDinoz.level > 0 ||
                                !isBotW(fightingDinoz.id)) &&
                                fightingDinoz.id !== "admin-b-raid" && (
                                  <th className="majorElement">
                                    {t("elementMajeur")}
                                  </th>
                                )}

                              {fightingDinoz.level > 0 &&
                                fightingDinoz.id !== "admin-b-raid" &&
                                getElementMajeurImage(fightingDinoz)}

                              {fightingDinoz.level > 0 &&
                                fightingDinoz.id !== "admin-b-raid" &&
                                homeDinozStrategyLevel(dinoz, fightingDinoz) >=
                                  1 && (
                                  <th className="majorElement">
                                    {"(" +
                                      getElementValue(fightingDinoz, 0) +
                                      ")"}
                                  </th>
                                )}

                              {fightingDinoz.level === 0 &&
                                !isBotW(fightingDinoz.id) && (
                                  <th className="majorElement">
                                    {t("inconnu")}
                                  </th>
                                )}
                            </tr>

                            {/*Strategy displays more informations on the PVP opponent :*/}
                            {fightingDinoz.level > 0 &&
                              !isBotW(fightingDinoz.id) &&
                              fightingDinoz.id !== "admin-b-raid" && (
                                <>
                                  {homeDinozStrategyLevel(
                                    dinoz,
                                    fightingDinoz
                                  ) >= 2 && (
                                    <tr>
                                      <th className="majorElement">
                                        {t("elementSecond")}
                                      </th>
                                      {getElementSecondImage(fightingDinoz)}
                                      {homeDinozStrategyLevel(
                                        dinoz,
                                        fightingDinoz
                                      ) >= 3 && (
                                        <th className="majorElement">
                                          {"(" +
                                            getElementValue(fightingDinoz, 1) +
                                            ")"}
                                        </th>
                                      )}
                                    </tr>
                                  )}

                                  {homeDinozStrategyLevel(
                                    dinoz,
                                    fightingDinoz
                                  ) >= 4 && (
                                    <tr>
                                      <th className="majorElement">
                                        {t("elementTierce")}
                                      </th>
                                      {getElementTierceImage(fightingDinoz)}
                                      {homeDinozStrategyLevel(
                                        dinoz,
                                        fightingDinoz
                                      ) >= 5 && (
                                        <th className="majorElement">
                                          {"(" +
                                            getElementValue(fightingDinoz, 2) +
                                            ")"}
                                        </th>
                                      )}
                                    </tr>
                                  )}
                                </>
                              )}

                            {fightingDinoz.level === 0 &&
                              isBotShiny(fightingDinoz.appearanceCode) && (
                                <tr>
                                  <th className="majorElement">{t("shiny")}</th>
                                </tr>
                              )}

                            {fightingDinoz.level === 0 &&
                              isBotW(fightingDinoz.id) && (
                                <>
                                  <tr>
                                    <th className="majorElement">
                                      {t("capturable")}
                                      {" ("}
                                      {fightingDinoz.life + " HP"}
                                      {")"}
                                    </th>
                                  </tr>
                                  <tr>
                                    <th className="majorElement">
                                      {t("charmsLeft")}
                                    </th>
                                  </tr>
                                  <div className="miniElemsDiv">
                                    <tr>
                                      <th className="majorElement">
                                        <img
                                          alt=""
                                          className="miniElems"
                                          src={miniElems}
                                        />
                                      </th>
                                    </tr>
                                    <tr>
                                      <th className="majorElementML">
                                        {fightingDinoz.passiveList[
                                          "bonus_fire"
                                        ] < 4 && (
                                          <a>
                                            {fightingDinoz.passiveList[
                                              "bonus_fire"
                                            ].toString()}
                                          </a>
                                        )}
                                        {fightingDinoz.passiveList[
                                          "bonus_fire"
                                        ] >= 4 && <a>{"?"}</a>}
                                        <a className="paddingSpace">{"-"}</a>
                                        {fightingDinoz.passiveList[
                                          "bonus_wood"
                                        ] < 4 && (
                                          <a>
                                            {fightingDinoz.passiveList[
                                              "bonus_wood"
                                            ].toString()}
                                          </a>
                                        )}
                                        {fightingDinoz.passiveList[
                                          "bonus_wood"
                                        ] >= 4 && <a>{"?"}</a>}
                                        <a className="paddingSpace">{"-"}</a>
                                        {fightingDinoz.passiveList[
                                          "bonus_water"
                                        ] < 4 && (
                                          <a>
                                            {fightingDinoz.passiveList[
                                              "bonus_water"
                                            ].toString()}
                                          </a>
                                        )}
                                        {fightingDinoz.passiveList[
                                          "bonus_water"
                                        ] >= 4 && <a>{"?"}</a>}
                                        <a className="paddingSpace">{"-"}</a>
                                        {fightingDinoz.passiveList[
                                          "bonus_thunder"
                                        ] < 4 && (
                                          <a>
                                            {fightingDinoz.passiveList[
                                              "bonus_thunder"
                                            ].toString()}
                                          </a>
                                        )}
                                        {fightingDinoz.passiveList[
                                          "bonus_thunder"
                                        ] >= 4 && <a>{"?"}</a>}
                                        <a className="paddingSpace">{"-"}</a>
                                        {fightingDinoz.passiveList[
                                          "bonus_air"
                                        ] < 4 && (
                                          <a>
                                            {fightingDinoz.passiveList[
                                              "bonus_air"
                                            ].toString()}
                                          </a>
                                        )}
                                        {fightingDinoz.passiveList[
                                          "bonus_air"
                                        ] >= 4 && <a>{"?"}</a>}
                                      </th>
                                    </tr>
                                  </div>
                                </>
                              )}

                            {fightingDinoz.id === "admin-b-raid" && (
                              <>
                                <tr>
                                  <th className="majorElement">
                                    {t("raidBoss")}
                                    {" ("}
                                    {fightingDinoz.life + " HP"}
                                    {")"}
                                  </th>
                                </tr>
                                <tr>
                                  <th className="majorElement">
                                    {t("charmsLeft")}
                                  </th>
                                </tr>
                                <div className="miniElemsDiv">
                                  <tr>
                                    <th className="majorElement">
                                      <img
                                        alt=""
                                        className="miniElems"
                                        src={miniElems}
                                      />
                                    </th>
                                  </tr>
                                  <tr>
                                    <th className="majorElementML">
                                      {fightingDinoz.passiveList["bonus_fire"] <
                                        50 && (
                                        <a>
                                          {fightingDinoz.passiveList[
                                            "bonus_fire"
                                          ].toString()}
                                        </a>
                                      )}
                                      {fightingDinoz.passiveList[
                                        "bonus_fire"
                                      ] >= 50 && <a>{"?"}</a>}
                                      <a className="paddingSpace">{"-"}</a>
                                      {fightingDinoz.passiveList["bonus_wood"] <
                                        50 && (
                                        <a>
                                          {fightingDinoz.passiveList[
                                            "bonus_wood"
                                          ].toString()}
                                        </a>
                                      )}
                                      {fightingDinoz.passiveList[
                                        "bonus_wood"
                                      ] >= 50 && <a>{"?"}</a>}
                                      <a className="paddingSpace">{"-"}</a>
                                      {fightingDinoz.passiveList[
                                        "bonus_water"
                                      ] < 50 && (
                                        <a>
                                          {fightingDinoz.passiveList[
                                            "bonus_water"
                                          ].toString()}
                                        </a>
                                      )}
                                      {fightingDinoz.passiveList[
                                        "bonus_water"
                                      ] >= 50 && <a>{"?"}</a>}
                                      <a className="paddingSpace">{"-"}</a>
                                      {fightingDinoz.passiveList[
                                        "bonus_thunder"
                                      ] < 50 && (
                                        <a>
                                          {fightingDinoz.passiveList[
                                            "bonus_thunder"
                                          ].toString()}
                                        </a>
                                      )}
                                      {fightingDinoz.passiveList[
                                        "bonus_thunder"
                                      ] >= 50 && <a>{"?"}</a>}
                                      <a className="paddingSpace">{"-"}</a>
                                      {fightingDinoz.passiveList["bonus_air"] <
                                        50 && (
                                        <a>
                                          {fightingDinoz.passiveList[
                                            "bonus_air"
                                          ].toString()}
                                        </a>
                                      )}
                                      {fightingDinoz.passiveList["bonus_air"] >=
                                        50 && <a>{"?"}</a>}
                                    </th>
                                  </tr>
                                </div>
                              </>
                            )}

                            {fightingDinoz.level === 0 &&
                              isBotFromQuest(fightingDinoz.id) &&
                              fightingDinoz.id != null &&
                              fightingDinoz.id.toString().charAt(0) == "@" && (
                                <tr>
                                  <th className="majorElement">
                                    {t("questElement")}
                                  </th>
                                </tr>
                              )}
                          </tbody>
                        </table>
                      </td>
                    </div>
                    <ReactTooltip
                      className="largetooltip"
                      html={true}
                      backgroundColor="transparent"
                    />
                  </div>
                </div>
              );
            })}
          </div>
          <button className="hoverReturn">
            <img
              alt=""
              onClick={(e) => {
                returnToDinoz();
              }}
              src={getActionImageFromActionString("🡸")}
            />
          </button>
        </div>
      )}

      {sectionCurrentlyActive === PageSection.Home && (
        <HomeSection
          dinoz={dinoz}
          processAction={processAction}
          reloadLocations={props.refreshLocations}
          reloader={{ reload, setReload }}
          activateTournamentViewPerDinozLocation={
            activateTournamentViewPerDinozLocation
          }
          respawnDinoz={respawnDinoz}
          setDinoz={setDinoz}
          setCourseMoveAgain={(value: boolean) => {
            setCourseMoveAgain(value);
            setIsMoving(false);
          }}
          isMoving={isMoving}
          isLoading={dinoz?.id !== props.dinozId}
          hasActionLoading={
            isLoadingDig ||
            isLoadingRock ||
            isLoadingBainDeFlammes ||
            isLoadingFish ||
            isLoadingPick
          }
          top={
            <>
              {isLoadingRock === false && hasRocked === true && (
                <Message>{rockData}</Message>
              )}

              {isLoadingDig === false && hasDigged === true && (
                <Message
                  iconSrc={
                    digCode === 1
                      ? getCollectionImageByString(digObject.toString())
                      : digCode === 2
                      ? getTotemImageByInteger(digObject)
                      : digCode === 3
                      ? getHistoryImageFromKey("hist_error.gif")
                      : getUltraRareByInteger(digObject)
                  }
                >
                  <span>
                    {digCode === 1
                      ? `${dinoz.name}${t("fouiller.collection")}`
                      : digCode === 2
                      ? `${dinoz.name}${t("fouiller.totem")}`
                      : digCode === 3
                      ? `${dinoz.name}${t("fouiller.rien")}`
                      : `${t("fouiller.ultra.1")}${dinoz.name}${t(
                          "fouiller.ultra.2"
                        )}`}
                  </span>
                </Message>
              )}

              {isLoadingFish === false && hasFished === true && (
                <Message
                  iconSrc={
                    fishingCode === 0
                      ? getHistoryImageFromKey("hist_error.gif")
                      : fishingCode === 1
                      ? fish1
                      : fishingCode === 2
                      ? fish2
                      : fishingCode === 3
                      ? fish3
                      : fish4
                  }
                >
                  <span>
                    {fishingCode === 0
                      ? `${dinoz.name}${t("fishing.failure")}`
                      : `${dinoz.name}${t("fishing.success")}`}
                  </span>
                </Message>
              )}

              {isLoadingPick === false && hasPicked === true && (
                <Message
                  iconSrc={
                    pickingCode === 0
                      ? getHistoryImageFromKey("hist_error.gif")
                      : pickingCode === 1
                      ? plant1
                      : pickingCode === 2
                      ? plant2
                      : pickingCode === 3
                      ? plant3
                      : pickingCode === 4
                      ? plant4
                      : plant5
                  }
                >
                  <span>
                    {pickingCode === 0
                      ? `${dinoz.name}${t("picking.failure")}`
                      : `${dinoz.name}${t("picking.success")}`}
                  </span>
                </Message>
              )}

              {isLoadingBainDeFlammes === false && hasFlamed === true && (
                <Message>{t("firebath") + bathHealAmount + " HP(s)."}</Message>
              )}

              {specialMoveSuccess === true && (
                <Message>
                  {t("newPlaceNotice")}
                  {getLocationByNumber(dinoz.placeNumber)}
                </Message>
              )}

              {specialMoveFailed === true && (
                <Message type="danger">{t("tentativeEchouee")}</Message>
              )}

              {wGone === true && <Message>{t("wGone")}</Message>}

              {courseMoveAgain === true && <Message>{t("runSuccess")}</Message>}

              {ennemyHasFled === true && (
                <div className="agilityResult">{t("agilityStatus")}</div>
              )}

              {success && <Message>{successMsg}</Message>}
              {noStockError && (
                <Message type="danger">{t("noStockError")}</Message>
              )}
            </>
          }
        />
      )}
    </div>
  ) : (
    <Loader css="margin: 2rem auto" />
  );
}
