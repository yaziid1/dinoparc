import React, { useEffect, useState } from "react";
import crowd from "../../../../media/game/crowd.png";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { apiUrl } from "../../../../index";

export default function SpecialEvent() {
  let { t } = useTranslation();
  let [actualEventRanking, setActualEventRanking] = useState([]);

  useEffect(() => {
    axios.get(apiUrl + "/utils/event-stats").then(({ data }) => {
      actualEventRanking = data;
      setActualEventRanking(actualEventRanking);
    });
  }, []);

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("Special")}</header>
        <div className="displayFusions">
          <img alt="" className="imgIrma" src={crowd} />
          <span className="textNarrow">
            {t("Halloween.partOne")}
            <br />
            <br />
            {t("Halloween.partTwo")}
            <br />
            <br />
            {t("Halloween.partThree")}
          </span>
        </div>
        <h2 className="miniHeaders2">{t("rankingsLabel")}</h2>
        <table className="rankingsTable">
          <tr className="trRankings">
            <th className="position">{t("classement.position")}</th>
            <th className="pseudo">{t("classement.pseudo")}</th>
            <th className="nbPoints">{t("Halloween.huntingPoints")}</th>
          </tr>
          {actualEventRanking.map((player, i) => {
            return (
              <tr className="selectedEntry">
                <td className="position">{i + 1}</td>
                <td className="pseudo">{player.accountName}</td>
                <td className="nbPoints">{player.objectsRetrievedInEvent}</td>
              </tr>
            );
          })}
        </table>
      </div>
      <br />
    </div>
  );
}
