import React from "react";
import "../../../../../assets/prizeWheel.css";
import getConsumableByKey from "../../../../utils/ConsumableByKey";

interface PrizeWheelProps {
  chosen: string;
  items: string[];
  onSelectItem?: (selectedItem: string) => unknown;
}

interface PrizeWheelState {
  selectedItem: string;
}

export default class PrizeWheel extends React.Component<
  PrizeWheelProps,
  PrizeWheelState
> {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null,
    };
    this.selectItem = this.selectItem.bind(this);
  }

  selectItem() {
    if (this.state.selectedItem === null) {
      const selectedItem = this.props.chosen;
      if (this.props.onSelectItem) {
        this.props.onSelectItem(selectedItem);
      }
      this.setState({ selectedItem });
    } else {
      this.setState({ selectedItem: null });
      setTimeout(this.selectItem, 500);
    }
  }

  render() {
    const { selectedItem } = this.state;
    const { items } = this.props;
    const wheelVars = {
      "--nb-item": items.length,
      "--selected-item": selectedItem,
    };

    const spinning = selectedItem !== null ? "spinning" : "";
    return (
      <div className="wheel-container">
        <div
          className={`wheel ${spinning}`}
          style={wheelVars as any}
          onClick={this.selectItem}
        >
          {items.map((item, index) => (
            <div
              className="wheel-item"
              key={index}
              style={{ "--item-nb": index } as any}
            >
              <img src={getConsumableByKey(item)} alt="" />
            </div>
          ))}
        </div>
      </div>
    );
  }
}
