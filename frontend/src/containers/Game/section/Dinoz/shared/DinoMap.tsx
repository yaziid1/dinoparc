import "../../../../../assets/mapStyle.css";
import React from "react";
import map1 from "../../../../../media/lieux/map1.png";
import map2 from "../../../../../media/lieux/map2.png";
import map3 from "../../../../../media/lieux/map3.png";
import map4 from "../../../../../media/lieux/map4.png";
import getLocationImageByNumber, {
  PlaceNumber,
} from "../../../../utils/LocationImageByPlaceNumber";
import getLocationByNumber from "../../../../utils/LocationByNumber";
import { Loader } from "../../../../../components/Loader";
import Tooltip from "../../../../../components/Tooltip";

type Map = {
  image: string;
  locations: {
    position: { top: string; left: string };
    placeNumber: PlaceNumber;
  }[];
};
const maps: Map[] = [
  {
    image: map1,
    locations: [
      {
        position: { top: `171px`, left: `188px` },
        placeNumber: PlaceNumber.Dinoville,
      },

      {
        position: { top: `212px`, left: `149px` },
        placeNumber: PlaceNumber.Imra,
      },

      {
        position: { top: `165px`, left: `84px` },
        placeNumber: PlaceNumber.Clairiere,
      },

      {
        position: { top: `133px`, left: `29px` },
        placeNumber: PlaceNumber.Dinoplage,
      },

      {
        position: { top: `117px`, left: `130px` },
        placeNumber: PlaceNumber.Barrage,
      },

      {
        position: { top: `46px`, left: `66px` },
        placeNumber: PlaceNumber.Falaise,
      },

      {
        position: { top: `19px`, left: `115px` },
        placeNumber: PlaceNumber.montdino,
      },

      {
        position: { top: `26px`, left: `197px` },
        placeNumber: PlaceNumber.porte,
      },

      {
        position: { top: `63px`, left: `247px` },
        placeNumber: PlaceNumber.gredins,
      },

      {
        position: { top: `56px`, left: `293px` },
        placeNumber: PlaceNumber.foret,
      },

      {
        position: { top: `20px`, left: `374px` },
        placeNumber: PlaceNumber.temple,
      },

      {
        position: { top: `116px`, left: `303px` },
        placeNumber: PlaceNumber.port,
      },

      {
        position: { top: `205px`, left: `376px` },
        placeNumber: PlaceNumber.pitie,
      },

      {
        position: { top: `191px`, left: `298px` },
        placeNumber: PlaceNumber.mayinca,
      },

      {
        position: { top: `216px`, left: `247px` },
        placeNumber: PlaceNumber.credit,
      },

      {
        position: { top: `111px`, left: `239px` },
        placeNumber: PlaceNumber.cratere,
      },

      {
        position: { top: `113px`, left: `375px` },
        placeNumber: PlaceNumber.plaines,
      },
    ],
  },
  {
    image: map2,
    locations: [
      {
        position: { top: `118px`, left: `342px` },
        placeNumber: PlaceNumber.bazar,
      },
      {
        position: { top: `111px`, left: `178px` },
        placeNumber: PlaceNumber.marais,
      },
      {
        position: { top: `169px`, left: `148px` },
        placeNumber: PlaceNumber.jungle,
      },
      {
        position: { top: `75px`, left: `180px` },
        placeNumber: PlaceNumber.bordeciel,
      },
      {
        position: { top: `118px`, left: `35px` },
        placeNumber: PlaceNumber.source,
      },
      {
        position: { top: `172px`, left: `248px` },
        placeNumber: PlaceNumber.anomalie,
      },
      {
        position: { top: `78px`, left: `74px` },
        placeNumber: PlaceNumber.hutte,
      },
    ],
  },
  // TODO finish locations
  {
    image: map3,
    locations: [
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
    ],
  },
  // TODO finish locations
  {
    image: map4,
    locations: [
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
      { position: { top: "0px", left: "0px" }, placeNumber: PlaceNumber.Dinoville },
    ],
  },
];

type Props = {
  currentLocation: PlaceNumber;
  availableLocations: PlaceNumber[];
  processLocationClick: (palceNumber: PlaceNumber) => void;
};

export default function DinoMap({
  currentLocation,
  availableLocations,
  processLocationClick,
}: Props) {
  const currentMap = maps.find((map) =>
    map.locations.some((location) => location.placeNumber === currentLocation)
  );

  function getLocationStateClass(placeNumber: PlaceNumber) {
    if (placeNumber === currentLocation) {
      return "current";
    }
    if (availableLocations.includes(placeNumber)) {
      return "available";
    }

    return "";
  }

  function handleLocationClick(placeNumber: PlaceNumber) {
    if (!availableLocations.includes(placeNumber)) {
      return;
    }

    processLocationClick(placeNumber);
  }

  function getTooltipLocationHtml(placeNumber: PlaceNumber) {
    return `<img alt=""  class="imageLieuResize" src="${getLocationImageByNumber(
      placeNumber
    )}" /><p class="lieuxNom">${getLocationByNumber(placeNumber)}</p>`;
  }

  return (
    <div
      id="map"
      style={{ height: `250px`, width: `400px`, position: "relative" }}
    >
      {currentMap == null ? (
        <Loader />
      ) : (
        <div>
          <img
            className="borderMap"
            id="mainMap"
            src={currentMap.image}
            alt=""
          />

          {currentMap.locations.map(({ position, placeNumber }) => {
            return (
              <div
                key={placeNumber}
                className={"places " + getLocationStateClass(placeNumber)}
                style={position}
                data-place="right"
                data-tip={getTooltipLocationHtml(placeNumber)}
                data-for="dinoMapTooltip"
                data-arrow-color="#c37253"
                onClick={() => handleLocationClick(placeNumber)}
              ></div>
            );
          })}
        </div>
      )}
      <Tooltip id="dinoMapTooltip" custom={true} />
    </div>
  );
}
