import i18n from "i18next";

export default function getLocationDetailsByNumber(placeNumber) {
  switch (placeNumber) {
    case 0:
      return i18n.t("location.dinoville.details");
    case 1:
      return i18n.t("location.caverneirma.details");
    case 2:
      return i18n.t("location.clairiere.details");
    case 3:
      return i18n.t("location.dinoplage.details");
    case 4:
      return i18n.t("location.barrage.details");
    case 5:
      return i18n.t("location.falaise.details");
    case 6:
      return i18n.t("location.montdino.details");
    case 7:
      return i18n.t("location.porte.details");
    case 8:
      return i18n.t("location.gredins.details");
    case 9:
      return i18n.t("location.foret.details");
    case 10:
      return i18n.t("location.temple.details");
    case 11:
      return i18n.t("location.port.details");
    case 12:
      return i18n.t("location.pitie.details");
    case 13:
      return i18n.t("location.ruines.details");
    case 14:
      return i18n.t("location.credit.details");
    case 15:
      return i18n.t("location.bazar.details");
    case 16:
      return i18n.t("location.marais.normal.details");
    case 17:
      return i18n.t("location.jungle.details");
    case 18:
      return i18n.t("location.bordeciel.details");
    case 19:
      return i18n.t("location.source.details");
    case 20:
      return i18n.t("location.anomalie.details");
    case 21:
      return i18n.t("location.hutte.details");
    case 22:
      return i18n.t("location.toutchaud.details");
    case 37:
      return i18n.t("location.plaines.details");
    default:
      break;
  }
}
