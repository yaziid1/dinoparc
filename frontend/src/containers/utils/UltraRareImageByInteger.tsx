import ange from "../../media/consumables/Potion Ange.gif";
import bons from "../../media/consumables/bons.gif";
import pain from "../../media/consumables/Pain Chaud.gif";

export default function getUltraRareByInteger(key) {
  switch (key) {
    case 1:
      return ange;
    case 2:
      return bons;
    case 3:
      return pain;
    default:
      return ange;
  }
}
