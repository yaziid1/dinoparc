import elem0 from "../../media/dinoz/element0.gif";
import elem1 from "../../media/dinoz/element1.gif";
import elem2 from "../../media/dinoz/element2.gif";
import elem3 from "../../media/dinoz/element3.gif";
import elem4 from "../../media/dinoz/element4.gif";

export default function getElementImageByString(element) {
  if (element === "Feu") {
    return elem0;
  } else if (element === "Terre") {
    return elem1;
  } else if (element === "Eau") {
    return elem2;
  } else if (element === "Foudre") {
    return elem3;
  } else if (element === "Air") {
    return elem4;
  }
}
