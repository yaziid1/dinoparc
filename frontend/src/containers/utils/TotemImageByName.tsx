import buze from "../../media/totem/totem_buze.png";
import feather from "../../media/totem/totem_feather.png";
import tooth from "../../media/totem/totem_tooth.png";
import crins from "../../media/totem/totem_unicorn.png";
import bois from "../../media/totem/totem_wood.png";

export default function getTotemImageByName(name) {
  if (name === "totemBuze") {
    return buze;
  } else if (name === "totemPlume") {
    return feather;
  } else if (name === "totemDent") {
    return tooth;
  } else if (name === "totemCrin") {
    return crins;
  } else if (name === "totemBois") {
    return bois;
  }
}
