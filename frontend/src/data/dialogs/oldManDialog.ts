import { DialogData } from "../../types/dialog";

const data: DialogData = {
  id: "oldManDialog",
  img: require("../../media/pnj/vieux1.jpg"),

  0: { options: [10, 2] },
  2: { options: [3] },
  3: { options: [4] },
  4: { options: [10] },
  10: { options: [12, 11, 13, 20] },
  11: { options: [18, 12, 13, 20] },
  18: { options: [12, 13, 20] },
  12: { options: [14, 15, 11, 13, 20] },
  14: { options: [15, 11, 13, 20] },
  15: { options: [16, 11, 13, 20] },
  16: { options: [17], img: require("../../media/pnj/vieux2.jpg") },
  17: { options: [11, 13, 20], img: require("../../media/pnj/vieux2.jpg") },
  13: { options: [19, 11, 12, 20] },
  19: {
    options: [60, 61, 11, 12, 20],
    img: require("../../media/pnj/edmond.gif"),
  },
  60: { options: [63, 11, 12, 20], img: require("../../media/pnj/vieux2.jpg") },
  61: { options: [62, 50] },
  62: { options: [60, 50], img: require("../../media/pnj/edmond.gif") },
  63: { options: [11, 12, 20], img: require("../../media/pnj/vieux2.jpg") },
  20: {
    options: [21, 31, 22, 23, 24, 50],
    img: require("../../media/pnj/vieux2.jpg"),
  },
  23: { options: [25, 21, 22, 24, 31, 50] },
  24: { options: [21, 22, 23, 31, 50] },
  25: { options: [26, 21, 22, 24, 31, 50] },
  26: { options: [27, 21, 22, 24, 31, 50] },
  27: { options: [21, 22, 24, 31, 50] },
  21: { options: [30, 22, 23, 24, 31, 50] },
  30: { options: [22, 23, 24, 31, 50] },
  31: {
    options: [21, 22, 23, 24, 50],
    img: require("../../media/pnj/vieux2.jpg"),
  },
  22: {
    options: [33, 34, 35, 38, 32, 50],
    img: require("../../media/pnj/vieux2.jpg"),
  },
  32: {
    options: [21, 31, 22, 23, 24, 50],
    img: require("../../media/pnj/vieux2.jpg"),
  },
  33: { options: [36, 37, 34, 35, 38, 32, 50] },
  36: { options: [34, 35, 38, 35, 32, 50] },
  37: { options: [34, 35, 38, 32, 50] },
  34: {
    options: [39, 33, 35, 38, 32, 50],
    img: require("../../media/pnj/vieux2.jpg"),
  },
  39: {
    options: [33, 35, 38, 32, 50],
    img: require("../../media/pnj/vieux2.jpg"),
  },
  35: { options: [33, 34, 38, 32, 50] },
  38: { options: [37, 33, 34, 35, 32, 50] },
  50: { img: require("../../media/pnj/vieux2.jpg") },
};

export default data;
