create table bazar
(
  id                         uuid             primary key not null,
  active                     boolean          default true,
  dinoz_id                   text,
  objects                    jsonb,
  seller_id                  text             not null,
  seller_name                text             default '' not null,
  initial_duration_in_days   integer          default 3 not null,
  end_date_in_epoch_seconds  bigint           not null,
  last_bider_id              text,
  last_bider_name            text,
  last_price                 integer          not null
);