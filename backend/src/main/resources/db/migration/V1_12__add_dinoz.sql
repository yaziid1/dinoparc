create table dinoz
(
  id                                   text             primary key not null,
  master_id                            text             not null,
  master_name                          text             not null,
  name                                 text             not null,
  race                                 text             not null,
  appearance_code                      text             not null,
  begin_message                        text             not null,
  end_message                          text             not null,
  life                                 integer          not null,
  level                                integer          not null,
  experience                           integer          not null,
  danger                               integer          not null,
  place_number                         integer          not null,
  is_dark                              boolean          not null,
  is_in_tourney                        boolean          not null,
  kabuki_progression                   integer          not null,
  last_valid_bot_hash                  integer,
  skips                                integer          not null,
  is_in_bazar                          boolean          not null,
  epoch_seconds_end_of_cherry_effect   decimal,
  cherry_effect_minutes_left           decimal,
  last_fled_ennemy                     text,
  passive_list                         jsonb            not null,
  malus_list                           jsonb            not null,
  elements_values                      jsonb            not null,
  skills_map                           jsonb            not null,
  actions_map                          jsonb            not null,
  tournaments                          jsonb
);

create index dinoz_master_id on dinoz(master_id);