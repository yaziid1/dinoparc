create table player_dinoz_store
(
  id                                   uuid             primary key not null,
  player_id                            uuid             not null,
  price                                integer          not null,
  rank                                 integer          not null,
  race                                 text             not null,
  appearance_code                      text             not null,
  skills_map                           jsonb            not null,
  elements_values                      jsonb            not null
);

alter table player drop column shop_dinoz_list;
alter table player add column last_generated_store timestamp with time zone;