create table client_information
(
  username                                   text             not null,
  first                                      bytea,
  second                                     bytea,
  third                                      bytea,
  retrieved_date                             timestamp with time zone not null,
  retrieved_date_txt                         text
);