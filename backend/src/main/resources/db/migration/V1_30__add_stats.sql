alter table player_stat add column nb_daily_raid_fight     bigint           not null default 0;
alter table player_stat add column nb_war_fight            bigint           not null default 0;
alter table player_stat add column nb_champifuz            bigint           not null default 0;
alter table player_stat add column nb_catapult             bigint           not null default 0;
alter table player_stat add column nb_climb                bigint           not null default 0;
alter table player_stat add column nb_prunayer             bigint           not null default 0;
alter table player_stat add column nb_fishing              bigint           not null default 0;
alter table player_stat add column nb_picking              bigint           not null default 0;