create table wild_wistiti
(
  appearance_code              text             not null,
  name                         text             not null,
  place_number                 integer          not null default 22,
  life                         integer          not null default 0,
  bonus_fire                   integer          not null default 0,
  bonus_wood                   integer          not null default 0,
  bonus_water                  integer          not null default 0,
  bonus_thunder                integer          not null default 0,
  bonus_air                    integer          not null default 0,
  fight_count                  integer          not null default 0
);

INSERT INTO public.wild_wistiti
(appearance_code, "name", place_number, life, bonus_fire, bonus_wood, bonus_water, bonus_thunder, bonus_air, fight_count)
VALUES('FbU8Qo1M6RLNWzW', '???', 17, 0, 0, 0, 0, 0, 0, 0);
