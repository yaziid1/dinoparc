package com.dinoparc.api.rand

interface RngCore {
    fun randUint(): UInt
}
