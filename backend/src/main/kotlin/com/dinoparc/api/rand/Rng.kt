package com.dinoparc.api.rand

import com.dinoparc.api.rand.distributions.UniformUint

class Rng(val inner: RngCore) : RngCore {
    override fun randUint(): UInt {
        return this.inner.randUint()
    }

    override fun toString(): String {
        return "Rng(${this.inner})"
    }

    /**
     * Picks a random integer `x` such that `low <= x < high`
     */
    fun randIntBetween(low: Int, high: Int): Int {
        assert(low < high);
        return low + this.randUintLower((high - low).toUInt()).toInt();
    }

    /**
     * Picks a random integer `x` such that `0 <= x < high`
     */
    fun randUintLower(high: UInt): UInt {
        assert(high > 0U);
        return UniformUint(0U, high - 1U).sample(this.inner)
    }

    /**
     * Picks a random integer `x` such that `0 <= x <= high`
     */
    fun randUintLowerEq(high: UInt): UInt {
        return UniformUint(0U, high).sample(this.inner)
    }

    /**
     * Modify an array in place to shuffle order of elements.
     */
    fun<T> shuffleInPlace(array: ArrayList<T>) {
        var end: Int = array.size;
        while (end >= 2) {
            val idx = this.randUintLower(end.toUInt()).toInt();
            end -= 1;
            if (idx != end) {
                val tmp = array[end];
                array[end] = array[idx];
                array[idx] = tmp;
            }
        }
    }
}
