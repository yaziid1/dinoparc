package com.dinoparc.api.rand.distributions

import com.dinoparc.api.rand.Distribution
import com.dinoparc.api.rand.RngCore

/**
 * Wrapper for `WeightedUint` providing compatibility with signed ints.
 */
class WeightedInt(weights: List<Int>) : Distribution<Int> {
    val inner: WeightedUint

    init {
        this.inner = WeightedUint(weights.map { w -> w.toUInt() })
    }

    override fun sample(rng: RngCore): Int {
        return this.inner.sample(rng).toInt()
    }
}
