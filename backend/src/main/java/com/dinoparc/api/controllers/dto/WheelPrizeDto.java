package com.dinoparc.api.controllers.dto;

public class WheelPrizeDto {

    private Integer prix;
    private Integer qtee;

    public WheelPrizeDto() {

    }

    public WheelPrizeDto(Integer prix, Integer qtee) {
        this.prix = prix;
        this.qtee = qtee;
    }

    public Integer getPrix() {
        return prix;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public Integer getQtee() {
        return qtee;
    }

    public void setQtee(Integer qtee) {
        this.qtee = qtee;
    }
}
