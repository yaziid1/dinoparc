package com.dinoparc.api.controllers.dto;

import java.util.HashMap;
import java.util.Map;
import com.dinoparc.api.domain.account.DinoparcConstants;

public class FusionSummaryDto {
  
  private String resultingAppCode;
  private Integer newLevel;
  private Integer champifuzzAmount;
  private Integer specialChampifuzzAmount;
  private Integer eternityPillsAmount;
  private Map<String, Integer> elementsDeltas;
  
  public FusionSummaryDto() {
    elementsDeltas = new HashMap<>();
    elementsDeltas.put(DinoparcConstants.FEU, 0);
    elementsDeltas.put(DinoparcConstants.TERRE, 0);
    elementsDeltas.put(DinoparcConstants.EAU, 0);
    elementsDeltas.put(DinoparcConstants.FOUDRE, 0);
    elementsDeltas.put(DinoparcConstants.AIR, 0);
  }

  public String getResultingAppCode() {
    return resultingAppCode;
  }

  public void setResultingAppCode(String resultingAppCode) {
    this.resultingAppCode = resultingAppCode;
  }

  public Integer getNewLevel() {
    return newLevel;
  }

  public void setNewLevel(Integer newLevel) {
    this.newLevel = newLevel;
  }

  public Map<String, Integer> getElementsDeltas() {
    return elementsDeltas;
  }

  public void setElementsDeltas(Map<String, Integer> elementsDeltas) {
    this.elementsDeltas = elementsDeltas;
  }

  public Integer getChampifuzzAmount() {
    return champifuzzAmount;
  }

  public void setChampifuzzAmount(Integer champifuzzAmount) {
    this.champifuzzAmount = champifuzzAmount;
  }

  public Integer getSpecialChampifuzzAmount() {
    return specialChampifuzzAmount;
  }

  public void setSpecialChampifuzzAmount(Integer specialChampifuzzAmount) {
    this.specialChampifuzzAmount = specialChampifuzzAmount;
  }

  public Integer getEternityPillsAmount() {
    return eternityPillsAmount;
  }

  public void setEternityPillsAmount(Integer eternityPillsAmount) {
    this.eternityPillsAmount = eternityPillsAmount;
  }
}
