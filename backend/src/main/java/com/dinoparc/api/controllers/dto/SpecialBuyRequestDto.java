package com.dinoparc.api.controllers.dto;

public class SpecialBuyRequestDto {

  private String totalPrice;

  private String buffAggroBuy;
  private String buffWaterBuy;
  private String buffNatureBuy;
  private String liquorBuy;
  private String feuBuy;
  private String terreBuy;
  private String eauBuy;
  private String foudreBuy;
  private String airBuy;

  public SpecialBuyRequestDto() {

  }

  public String getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(String totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getBuffAggroBuy() {
    return buffAggroBuy;
  }

  public void setBuffAggroBuy(String buffAggroBuy) {
    this.buffAggroBuy = buffAggroBuy;
  }

  public String getBuffWaterBuy() {
    return buffWaterBuy;
  }

  public void setBuffWaterBuy(String buffWaterBuy) {
    this.buffWaterBuy = buffWaterBuy;
  }

  public String getBuffNatureBuy() {
    return buffNatureBuy;
  }

  public void setBuffNatureBuy(String buffNatureBuy) {
    this.buffNatureBuy = buffNatureBuy;
  }

  public String getLiquorBuy() {
    return liquorBuy;
  }

  public void setLiquorBuy(String liquorBuy) {
    this.liquorBuy = liquorBuy;
  }

  public String getFeuBuy() {
    return feuBuy;
  }

  public void setFeuBuy(String feuBuy) {
    this.feuBuy = feuBuy;
  }

  public String getTerreBuy() {
    return terreBuy;
  }

  public void setTerreBuy(String terreBuy) {
    this.terreBuy = terreBuy;
  }

  public String getEauBuy() {
    return eauBuy;
  }

  public void setEauBuy(String eauBuy) {
    this.eauBuy = eauBuy;
  }

  public String getFoudreBuy() {
    return foudreBuy;
  }

  public void setFoudreBuy(String foudreBuy) {
    this.foudreBuy = foudreBuy;
  }

  public String getAirBuy() {
    return airBuy;
  }

  public void setAirBuy(String airBuy) {
    this.airBuy = airBuy;
  }

}
