package com.dinoparc.api.controllers.dto;

import com.dinoparc.api.domain.dinoz.Dinoz;

import java.util.*;

public class Sellables {

    private Dinoz sellableDinoz;
    private Map<String, Integer> sellableObjects;

    public Sellables() {
        sellableObjects = new HashMap<String, Integer>();
    }

    public Dinoz getSellableDinoz() {
        return sellableDinoz;
    }

    public void setSellableDinoz(Dinoz sellableDinoz) {
        this.sellableDinoz = sellableDinoz;
    }

    public Map<String, Integer> getSellableObjects() {
        return sellableObjects;
    }
}
