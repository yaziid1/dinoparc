package com.dinoparc.api.controllers.dto;

public class PlayerClanInfoDto {

    private Boolean isInAClan;
    private String clanName;

    public PlayerClanInfoDto() {

    }

    public Boolean getInAClan() {
        return isInAClan;
    }

    public void setInAClan(Boolean inAClan) {
        isInAClan = inAClan;
    }

    public String getClanName() {
        return clanName;
    }

    public void setClanName(String clanName) {
        this.clanName = clanName;
    }
}
