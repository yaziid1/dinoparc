package com.dinoparc.api.controllers.dto;

import java.util.ArrayList;
import java.util.List;

public class CreateListDto {

  private List<CreateDinozDto> dinozList;
  
  public CreateListDto() {
    this.dinozList = new ArrayList<CreateDinozDto>();
  }

  public List<CreateDinozDto> getDinozList() {
    return dinozList;
  }

  public void setDinozList(List<CreateDinozDto> dinozList) {
    this.dinozList = dinozList;
  }
}
