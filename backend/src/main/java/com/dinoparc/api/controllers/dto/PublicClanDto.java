package com.dinoparc.api.controllers.dto;

import com.dinoparc.api.domain.clan.Page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PublicClanDto {

    private String clanId;
    private String name;
    private Integer nbDinozTotal;
    private Integer nbDinozWarriors;
    private Integer nbPointsOccupation;
    private Integer nbPointsFights;
    private Integer nbPointsTotem;
    private Integer position;
    private String faction;
    private String creationDate;
    private Map<String, Page> publicPages;
    private List<String> members;
    private List<String> allies;

    public PublicClanDto() {
        this.members = new ArrayList<>();
        this.publicPages = new HashMap<>();
        this.allies = new ArrayList<>();
    }

    public String getClanId() {
        return clanId;
    }

    public void setClanId(String clanId) {
        this.clanId = clanId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNbDinozTotal() {
        return nbDinozTotal;
    }

    public void setNbDinozTotal(Integer nbDinozTotal) {
        this.nbDinozTotal = nbDinozTotal;
    }

    public Integer getNbDinozWarriors() {
        return nbDinozWarriors;
    }

    public void setNbDinozWarriors(Integer nbDinozWarriors) {
        this.nbDinozWarriors = nbDinozWarriors;
    }

    public Integer getNbPointsOccupation() {
        return nbPointsOccupation;
    }

    public void setNbPointsOccupation(Integer nbPointsOccupation) {
        this.nbPointsOccupation = nbPointsOccupation;
    }

    public Integer getNbPointsFights() {
        return nbPointsFights;
    }

    public void setNbPointsFights(Integer nbPointsFights) {
        this.nbPointsFights = nbPointsFights;
    }

    public Integer getNbPointsTotem() {
        return nbPointsTotem;
    }

    public void setNbPointsTotem(Integer nbPointsTotem) {
        this.nbPointsTotem = nbPointsTotem;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public Map<String, Page> getPublicPages() {
        return publicPages;
    }

    public void setPublicPages(Map<String, Page> publicPages) {
        this.publicPages = publicPages;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public List<String> getAllies() {
        return allies;
    }

    public void setAllies(List<String> allies) {
        this.allies = allies;
    }
}
