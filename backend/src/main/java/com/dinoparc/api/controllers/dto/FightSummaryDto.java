package com.dinoparc.api.controllers.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

public class FightSummaryDto {
  // Section #1 : SWF Fight animation data:
  private String leftDinozName;
  private String rightDinozName;
  private String leftDinozAppCode;
  private String rightDinozAppCode;
  private String leftDinozBeginMessage;
  private String rightDinozBeginMessage;
  private String leftDinozEndMessage;
  private String rightDinozEndMessage;

  private Integer initialHpLeft;
  private Integer initialHpRight;

  private Integer firstElementLeft;
  private Integer firstElementRight;
  private Integer roundOneScore;
  private Integer charmFirstRoundForLoser;
  private Integer lifeLostPreventedFirstRound;
  private String charmTypeFirstRound;
  private Integer lifeLossFirstRoundByLoser;

  private Integer secondElementLeft;
  private Integer secondElementRight;
  private Integer roundTwoScore;
  private Integer charmSecondRoundForLoser;
  private Integer lifeLostPreventedSecondRound;
  private String charmTypeSecondRound;
  private Integer lifeLossSecondRoundByLoser;

  private Integer thirdElementLeft;
  private Integer thirdElementRight;
  private Integer roundThreeScore;
  private Integer charmThirdRoundForLoser;
  private Integer lifeLostPreventedThirdRound;
  private String charmTypeThirdRound;
  private Integer lifeLossThirdRoundByLoser;

  // Section #2 : Summary page at the end:
  private String ennemyMasterName;
  private String ennemyMasterDinozName;
  private Integer finalLifeLoss;

  @JsonIgnore
  private Integer eventDinozLifeLoss;
  private boolean haveWon;
  private Integer experienceWon;
  private Integer dangerAccumulated;
  private Integer camouflageBuff;
  private String stolenObject;
  private Integer moneyWon;
  private List<String> skillsEffectsFr;
  private List<String> skillsEffectsEs;
  private List<String> skillsEffectsEn;
  private List<String> charmsEffectsFr;
  private List<String> charmsEffectsEs;
  private List<String> charmsEffectsEn;
  private Integer scoreFinalHome;
  private Integer scoreFinalEnnemy;

  public FightSummaryDto() {

  }

  public String getLeftDinozName() {
    return leftDinozName;
  }

  public void setLeftDinozName(String leftDinozName) {
    this.leftDinozName = leftDinozName;
  }

  public String getRightDinozName() {
    return rightDinozName;
  }

  public void setRightDinozName(String rightDinozName) {
    this.rightDinozName = rightDinozName;
  }

  public String getLeftDinozAppCode() {
    return leftDinozAppCode;
  }

  public void setLeftDinozAppCode(String leftDinozAppCode) {
    this.leftDinozAppCode = leftDinozAppCode;
  }

  public String getRightDinozAppCode() {
    return rightDinozAppCode;
  }

  public void setRightDinozAppCode(String rightDinozAppCode) {
    this.rightDinozAppCode = rightDinozAppCode;
  }

  public String getLeftDinozBeginMessage() {
    return leftDinozBeginMessage;
  }

  public void setLeftDinozBeginMessage(String leftDinozBeginMessage) {
    this.leftDinozBeginMessage = leftDinozBeginMessage;
  }

  public String getRightDinozBeginMessage() {
    return rightDinozBeginMessage;
  }

  public void setRightDinozBeginMessage(String rightDinozBeginMessage) {
    this.rightDinozBeginMessage = rightDinozBeginMessage;
  }

  public String getLeftDinozEndMessage() {
    return leftDinozEndMessage;
  }

  public void setLeftDinozEndMessage(String leftDinozEndMessage) {
    this.leftDinozEndMessage = leftDinozEndMessage;
  }

  public String getRightDinozEndMessage() {
    return rightDinozEndMessage;
  }

  public void setRightDinozEndMessage(String rightDinozEndMessage) {
    this.rightDinozEndMessage = rightDinozEndMessage;
  }

  public Integer getInitialHpLeft() {
    return initialHpLeft;
  }

  public void setInitialHpLeft(Integer initialHpLeft) {
    this.initialHpLeft = initialHpLeft;
  }

  public Integer getInitialHpRight() {
    return initialHpRight;
  }

  public void setInitialHpRight(Integer initialHpRight) {
    this.initialHpRight = initialHpRight;
  }

  public Integer getFirstElementLeft() {
    return firstElementLeft;
  }

  public void setFirstElementLeft(Integer firstElementLeft) {
    this.firstElementLeft = firstElementLeft;
  }

  public Integer getFirstElementRight() {
    return firstElementRight;
  }

  public void setFirstElementRight(Integer firstElementRight) {
    this.firstElementRight = firstElementRight;
  }

  public Integer getRoundOneScore() {
    return roundOneScore;
  }

  public void setRoundOneScore(Integer roundOneScore) {
    this.roundOneScore = roundOneScore;
  }

  public Integer getCharmFirstRoundForLoser() {
    return charmFirstRoundForLoser;
  }

  public void setCharmFirstRoundForLoser(Integer charmFirstRoundForLoser) {
    this.charmFirstRoundForLoser = charmFirstRoundForLoser;
  }

  public Integer getLifeLostPreventedFirstRound() {
    return lifeLostPreventedFirstRound;
  }

  public void setLifeLostPreventedFirstRound(Integer lifeLostPreventedFirstRound) {
    this.lifeLostPreventedFirstRound = lifeLostPreventedFirstRound;
  }

  public String getCharmTypeFirstRound() {
    return charmTypeFirstRound;
  }

  public void setCharmTypeFirstRound(String charmTypeFirstRound) {
    this.charmTypeFirstRound = charmTypeFirstRound;
  }

  public Integer getLifeLossFirstRoundByLoser() {
    return lifeLossFirstRoundByLoser;
  }

  public void setLifeLossFirstRoundByLoser(Integer lifeLossFirstRoundByLoser) {
    this.lifeLossFirstRoundByLoser = lifeLossFirstRoundByLoser;
  }

  public Integer getSecondElementLeft() {
    return secondElementLeft;
  }

  public void setSecondElementLeft(Integer secondElementLeft) {
    this.secondElementLeft = secondElementLeft;
  }

  public Integer getSecondElementRight() {
    return secondElementRight;
  }

  public void setSecondElementRight(Integer secondElementRight) {
    this.secondElementRight = secondElementRight;
  }

  public Integer getRoundTwoScore() {
    return roundTwoScore;
  }

  public void setRoundTwoScore(Integer roundTwoScore) {
    this.roundTwoScore = roundTwoScore;
  }

  public Integer getCharmSecondRoundForLoser() {
    return charmSecondRoundForLoser;
  }

  public void setCharmSecondRoundForLoser(Integer charmSecondRoundForLoser) {
    this.charmSecondRoundForLoser = charmSecondRoundForLoser;
  }

  public Integer getLifeLostPreventedSecondRound() {
    return lifeLostPreventedSecondRound;
  }

  public void setLifeLostPreventedSecondRound(Integer lifeLostPreventedSecondRound) {
    this.lifeLostPreventedSecondRound = lifeLostPreventedSecondRound;
  }

  public String getCharmTypeSecondRound() {
    return charmTypeSecondRound;
  }

  public void setCharmTypeSecondRound(String charmTypeSecondRound) {
    this.charmTypeSecondRound = charmTypeSecondRound;
  }

  public Integer getLifeLossSecondRoundByLoser() {
    return lifeLossSecondRoundByLoser;
  }

  public void setLifeLossSecondRoundByLoser(Integer lifeLossSecondRoundByLoser) {
    this.lifeLossSecondRoundByLoser = lifeLossSecondRoundByLoser;
  }

  public Integer getThirdElementLeft() {
    return thirdElementLeft;
  }

  public void setThirdElementLeft(Integer thirdElementLeft) {
    this.thirdElementLeft = thirdElementLeft;
  }

  public Integer getThirdElementRight() {
    return thirdElementRight;
  }

  public void setThirdElementRight(Integer thirdElementRight) {
    this.thirdElementRight = thirdElementRight;
  }

  public Integer getRoundThreeScore() {
    return roundThreeScore;
  }

  public void setRoundThreeScore(Integer roundThreeScore) {
    this.roundThreeScore = roundThreeScore;
  }

  public Integer getCharmThirdRoundForLoser() {
    return charmThirdRoundForLoser;
  }

  public void setCharmThirdRoundForLoser(Integer charmThirdRoundForLoser) {
    this.charmThirdRoundForLoser = charmThirdRoundForLoser;
  }

  public Integer getLifeLostPreventedThirdRound() {
    return lifeLostPreventedThirdRound;
  }

  public void setLifeLostPreventedThirdRound(Integer lifeLostPreventedThirdRound) {
    this.lifeLostPreventedThirdRound = lifeLostPreventedThirdRound;
  }

  public String getCharmTypeThirdRound() {
    return charmTypeThirdRound;
  }

  public void setCharmTypeThirdRound(String charmTypeThirdRound) {
    this.charmTypeThirdRound = charmTypeThirdRound;
  }

  public Integer getLifeLossThirdRoundByLoser() {
    return lifeLossThirdRoundByLoser;
  }

  public void setLifeLossThirdRoundByLoser(Integer lifeLossThirdRoundByLoser) {
    this.lifeLossThirdRoundByLoser = lifeLossThirdRoundByLoser;
  }

  public boolean isHaveWon() {
    return haveWon;
  }

  public void setHaveWon(boolean haveWon) {
    this.haveWon = haveWon;
  }

  public Integer getFinalLifeLoss() {
    return finalLifeLoss;
  }

  public void setFinalLifeLoss(Integer finalLifeLoss) {
    this.finalLifeLoss = finalLifeLoss;
  }

  public String getEnnemyMasterName() {
    return ennemyMasterName;
  }

  public void setEnnemyMasterName(String ennemyMasterName) {
    this.ennemyMasterName = ennemyMasterName;
  }

  public String getEnnemyMasterDinozName() {
    return ennemyMasterDinozName;
  }

  public void setEnnemyMasterDinozName(String ennemyMasterDinozName) {
    this.ennemyMasterDinozName = ennemyMasterDinozName;
  }

  public Integer getExperienceWon() {
    return experienceWon;
  }

  public void setExperienceWon(Integer experienceWon) {
    this.experienceWon = experienceWon;
  }

  public Integer getMoneyWon() {
    return moneyWon;
  }

  public void setMoneyWon(Integer moneyWon) {
    this.moneyWon = moneyWon;
  }

  public List<String> getSkillsEffectsFr() {
    return skillsEffectsFr;
  }

  public void setSkillsEffectsFr(List<String> skillsEffectsFr) {
    this.skillsEffectsFr = skillsEffectsFr;
  }

  public List<String> getSkillsEffectsEs() {
    return skillsEffectsEs;
  }

  public void setSkillsEffectsEs(List<String> skillsEffectsEs) {
    this.skillsEffectsEs = skillsEffectsEs;
  }

  public List<String> getSkillsEffectsEn() {
    return skillsEffectsEn;
  }

  public void setSkillsEffectsEn(List<String> skillsEffectsEn) {
    this.skillsEffectsEn = skillsEffectsEn;
  }

  public Integer getDangerAccumulated() {
    return dangerAccumulated;
  }

  public void setDangerAccumulated(Integer dangerAccumulated) {
    this.dangerAccumulated = dangerAccumulated;
  }

  public String getStolenObject() {
    return stolenObject;
  }

  public void setStolenObject(String stolenObject) {
    this.stolenObject = stolenObject;
  }

  public Integer getCamouflageBuff() {
    return camouflageBuff;
  }

  public void setCamouflageBuff(Integer camouflageBuff) {
    this.camouflageBuff = camouflageBuff;
  }

  public List<String> getCharmsEffectsFr() {
    return charmsEffectsFr;
  }

  public void setCharmsEffectsFr(List<String> charmsEffectsFr) {
    this.charmsEffectsFr = charmsEffectsFr;
  }

  public List<String> getCharmsEffectsEs() {
    return charmsEffectsEs;
  }

  public void setCharmsEffectsEs(List<String> charmsEffectsEs) {
    this.charmsEffectsEs = charmsEffectsEs;
  }

  public List<String> getCharmsEffectsEn() {
    return charmsEffectsEn;
  }

  public void setCharmsEffectsEn(List<String> charmsEffectsEn) {
    this.charmsEffectsEn = charmsEffectsEn;
  }

  public Integer getScoreFinalHome() {
    return scoreFinalHome;
  }

  public void setScoreFinalHome(Integer scoreFinalHome) {
    this.scoreFinalHome = scoreFinalHome;
  }

  public Integer getScoreFinalEnnemy() {
    return scoreFinalEnnemy;
  }

  public void setScoreFinalEnnemy(Integer scoreFinalEnnemy) {
    this.scoreFinalEnnemy = scoreFinalEnnemy;
  }

  public Integer getEventDinozLifeLoss() {
    return eventDinozLifeLoss;
  }

  public void setEventDinozLifeLoss(Integer eventDinozLifeLoss) {
    this.eventDinozLifeLoss = eventDinozLifeLoss;
  }
}
