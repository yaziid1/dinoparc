package com.dinoparc.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/error")
public class ErrorController {

  @Autowired
  public ErrorController() {}

  @GetMapping
  public String getDefaultErrorFallback() {
    return "The page you are trying to access doesn't exist.";
  }
}
