package com.dinoparc.api.controllers.dto;

public class CreateDinozDto {

    //Determined by the datamine:
    public String name;
    public int level;

    //Determined by the player:
    public String race;
    public boolean isDark;

    public CreateDinozDto() {

    }
}
