package com.dinoparc.api.controllers.dto;

import java.util.HashMap;
import java.util.Map;

public class HermitStageDto {
    private Integer actualStage;
    private Integer actualWins;
    private Integer neededWins;
    private String actualFighterId;
    private String actualFighterName;

    private Integer ennemyLevel;
    private String ennemyNameFr;
    private String ennemyNameEs;
    private String ennemyNameEn;
    private String appearanceCode;
    private String elementMajeur;
    private Map<String, Integer> elementsValues;
    private Map<String, Integer> rewards;

    private boolean levelGapIsValid;
    private boolean fighterIsValid;
    private boolean isDisplayingReward;

    public HermitStageDto() {
        this.elementsValues = new HashMap<>();
        this.levelGapIsValid = true;
        this.fighterIsValid = true;
        this.isDisplayingReward = false;
        this.rewards = new HashMap<String, Integer>();
    }

    public Map<String, Integer> getRewards() {
        return rewards;
    }

    public Integer getActualStage() {
        return actualStage;
    }

    public void setActualStage(Integer actualStage) {
        this.actualStage = actualStage;
    }

    public Integer getActualWins() {
        return actualWins;
    }

    public void setActualWins(Integer actualWins) {
        this.actualWins = actualWins;
    }

    public Integer getNeededWins() {
        return neededWins;
    }

    public void setNeededWins(Integer neededWins) {
        this.neededWins = neededWins;
    }

    public Map<String, Integer> getElementsValues() {
        return elementsValues;
    }

    public String getActualFighterId() {
        return actualFighterId;
    }

    public void setActualFighterId(String actualFighterId) {
        this.actualFighterId = actualFighterId;
    }

    public String getActualFighterName() {
        return actualFighterName;
    }

    public void setActualFighterName(String actualFighterName) {
        this.actualFighterName = actualFighterName;
    }

    public String getEnnemyNameFr() {
        return ennemyNameFr;
    }

    public void setEnnemyNameFr(String ennemyNameFr) {
        this.ennemyNameFr = ennemyNameFr;
    }

    public String getEnnemyNameEs() {
        return ennemyNameEs;
    }

    public void setEnnemyNameEs(String ennemyNameEs) {
        this.ennemyNameEs = ennemyNameEs;
    }

    public String getEnnemyNameEn() {
        return ennemyNameEn;
    }

    public void setEnnemyNameEn(String ennemyNameEn) {
        this.ennemyNameEn = ennemyNameEn;
    }

    public Integer getEnnemyLevel() {
        return ennemyLevel;
    }

    public void setEnnemyLevel(Integer ennemyLevel) {
        this.ennemyLevel = ennemyLevel;
    }

    public String getAppearanceCode() {
        return appearanceCode;
    }

    public void setAppearanceCode(String appearanceCode) {
        this.appearanceCode = appearanceCode;
    }

    public String getElementMajeur() {
        return elementMajeur;
    }

    public void setElementMajeur(String elementMajeur) {
        this.elementMajeur = elementMajeur;
    }

    public boolean isLevelGapIsValid() {
        return levelGapIsValid;
    }

    public void setLevelGapIsValid(boolean levelGapIsValid) {
        this.levelGapIsValid = levelGapIsValid;
    }

    public boolean isFighterIsValid() {
        return fighterIsValid;
    }

    public void setFighterIsValid(boolean fighterIsValid) {
        this.fighterIsValid = fighterIsValid;
    }

    public boolean isDisplayingReward() {
        return isDisplayingReward;
    }

    public void setDisplayingReward(boolean displayingReward) {
        isDisplayingReward = displayingReward;
    }
}
