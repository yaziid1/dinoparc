package com.dinoparc.api.controllers.dto;

public class MessageDto {

  private String accountDestination;
  private String accountSender;
  private String messageBody;


  public MessageDto() {

  }

  public MessageDto(String accountDestination, String accountSender, String messageBody) {
    super();
    this.accountDestination = accountDestination;
    this.accountSender = accountSender;
    this.messageBody = messageBody;
  }

  public String getAccountDestination() {
    return accountDestination;
  }

  public void setAccountDestination(String accountDestination) {
    this.accountDestination = accountDestination;
  }

  public String getAccountSender() { return accountSender; }

  public void setAccountSender(String accountSender) { this.accountSender = accountSender; }

  public String getMessageBody() { return messageBody; }

  public void setMessageBody(String messageBody) { this.messageBody = messageBody;}

}
