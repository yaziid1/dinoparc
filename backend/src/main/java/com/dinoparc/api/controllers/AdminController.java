package com.dinoparc.api.controllers;

import com.dinoparc.api.configuration.NeoparcConfig;
import com.dinoparc.api.controllers.dto.*;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.History;
import com.dinoparc.api.domain.account.News;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.clan.Clan;
import com.dinoparc.api.domain.clan.PlayerClan;
import com.dinoparc.api.domain.clan.Totem;
import com.dinoparc.api.domain.clan.TotemService;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.DinozUtils;
import com.dinoparc.api.domain.dinoz.EventDinozDto;
import com.dinoparc.api.repository.*;
import com.dinoparc.api.schedulers.RaidBossArmyScheduler;
import com.dinoparc.api.services.*;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static com.dinoparc.tables.PlayerStat.PLAYER_STAT;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/admin")
public class AdminController {
  @Value("${server.test}")
  public String st;

  private final UtilsService utilsService;
  private final AccountService accountService;
  private final RaidBossArmyScheduler raidBossArmyScheduler;
  private final PgPlayerRepository playerRepository;
  private final BazarService bazarService;
  private final ClanService clanService;
  private final PgCollectionRepository collectionRepository;
  private final PgDinozRepository dinozRepository;
  private final PgInventoryRepository inventoryRepository;
  private final PgNewsRepository newsRepository;
  private final PgBossRepository raidBossRepository;
  private final PgWildWistitiRepository wildWistitiRepository;
  private final PgClanRepository clanRepository;
  private final PgHistoryRepository historyRepository;
  private final TotemService totemService;
  private final NeoparcConfig neoparcConfig;

  @Autowired
  public AdminController(UtilsService utilsService,
                         AccountService accountService,
                         PgPlayerRepository playerRepository,
                         BazarService bazarService,
                         ClanService clanService,
                         RaidBossArmyScheduler raidBossArmyScheduler,
                         PgCollectionRepository collectionRepository,
                         PgDinozRepository dinozRepository,
                         PgInventoryRepository inventoryRepository,
                         PgNewsRepository newsRepository,
                         PgBossRepository raidBossRepository,
                         PgWildWistitiRepository wildWistitiRepository,
                         PgClanRepository clanRepository,
                         PgHistoryRepository historyRepository,
                         TotemService totemService,
                         NeoparcConfig neoparcConfig) {

    this.utilsService = utilsService;
    this.accountService = accountService;
    this.playerRepository = playerRepository;
    this.bazarService = bazarService;
    this.clanService = clanService;
    this.raidBossArmyScheduler = raidBossArmyScheduler;
    this.collectionRepository = collectionRepository;
    this.dinozRepository = dinozRepository;
    this.inventoryRepository = inventoryRepository;
    this.newsRepository = newsRepository;
    this.raidBossRepository = raidBossRepository;
    this.wildWistitiRepository = wildWistitiRepository;
    this.clanRepository = clanRepository;
    this.historyRepository = historyRepository;
    this.totemService = totemService;
    this.neoparcConfig = neoparcConfig;
  }

  @GetMapping("/trigger/maj/{maj}")
  public ResponseEntity<String> triggerManualMAJ(@PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      try {
        utilsService.maintenanceGenerale();
        return new ResponseEntity<>("MAJ Success", HttpStatus.OK);

      } catch (Exception e) {
        String stacktrace = ExceptionUtils.getStackTrace(e);
        return new ResponseEntity<>(e.getMessage() + stacktrace, HttpStatus.CONFLICT);
      }
    } else {
      return new ResponseEntity<>("MAJ Unauthorized", HttpStatus.UNAUTHORIZED);
    }
  }

  @PostMapping("/news/{maj}")
  public HttpStatus createANews(@RequestBody News news, @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      try {
        utilsService.createANews(news);
        return HttpStatus.OK;

      } catch (Exception e) {
        return HttpStatus.BAD_REQUEST;
      }
    } else {
      return HttpStatus.UNAUTHORIZED;
    }
  }

  @PatchMapping("/news/{newsId}/{maj}")
  public HttpStatus fixANewsLabels(@RequestBody News news, @PathVariable String newsId, @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      this.newsRepository.updateNewsLabels(newsId, news);
      return HttpStatus.OK;
    } else {
      return HttpStatus.UNAUTHORIZED;
    }
  }

  @GetMapping("/trigger/goldbonus/{goldAmount}/{maj}")
  public ResponseEntity<String> triggerManualGoldBonus(@PathVariable Integer goldAmount, @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      try {
        playerRepository.updateCashAll(goldAmount);
        return new ResponseEntity<>("Gold Given!", HttpStatus.OK);

      } catch (Exception e) {
        String stacktrace = ExceptionUtils.getStackTrace(e);
        return new ResponseEntity<>(e.getMessage() + stacktrace, HttpStatus.CONFLICT);
      }
    } else {
      return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
    }
  }

  @DeleteMapping("/imports/{accountId}/{server}/{maj}")
  public HttpStatus eraseImportByIdAndServer(@PathVariable String accountId,
                                             @PathVariable String server,
                                             @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      try {
        accountService.deleteImportByIdAndServer(accountId, server);
        return HttpStatus.OK;

      } catch (Exception e) {
        return HttpStatus.CONFLICT;
      }
    } else {
      return HttpStatus.UNAUTHORIZED;
    }
  }

  @PutMapping("/lonely/{accountId}/{dinozId}/{maj}")
  public HttpStatus putBackAloneDinozInRightAccount(@PathVariable String accountId, @PathVariable String dinozId, @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      Player account = accountService.getAccountById(accountId).get();
      Dinoz dinozAlone = accountService.getDinozById(dinozId);

      try {
        playerRepository.addDinoz(account.getId(), dinozAlone.getId());
      } catch (Exception e) {

      }

      dinozRepository.setMasterInfos(dinozId, accountId, account.getName());
      return HttpStatus.OK;

    } else {
      return HttpStatus.UNAUTHORIZED;
    }
  }


  @DeleteMapping("/money/{accountId}/{cashToSub}/{maj}")
  public HttpStatus deleteMoneyFromAccount(@PathVariable String accountId,
                                           @PathVariable Integer cashToSub,
                                           @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      try {
        playerRepository.updateCash(accountId, -cashToSub);
        return HttpStatus.OK;

      } catch (Exception e) {
        return HttpStatus.CONFLICT;
      }
    } else {
      return HttpStatus.UNAUTHORIZED;
    }
  }

  @GetMapping("/consult-activity/{maj}")
  public List<List<String>> checkActivityStats(@PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      if (maj.equalsIgnoreCase(st)) {
        List stats = new ArrayList<ArrayList<String>>();
        stats.add(AccountController.enrolledToday);
        stats.add(AccountController.connectedToday);
        return stats;
      }
    }
    return null;
  }

  @DeleteMapping("/ghost-dinoz/{dinozId}/{maj}")
  public String deleteGhostDinozById(@PathVariable String maj, @PathVariable String dinozId) {
    String response = "";
    if (maj.equalsIgnoreCase(st)) {
      try {
        dinozRepository.deleteById(dinozId);
        response += "Success! Dinoz deleted : " + dinozId;

      } catch (Exception e) {
        response += "Exception : " + e.getMessage();
      }
    }
    return response;
  }

  @GetMapping("/consult-daily/{maj}")
  public ResponseEntity<String> checkDinozStats(@PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      EventDinozDto wildWistiti = wildWistitiRepository.getWildWistiti();
      if (wildWistiti.getLife() > 0) {
        String lifeLeft = String.valueOf(wildWistiti.getLife());
        String fCount = String.valueOf(wildWistiti.getFightCount());
        return new ResponseEntity<>("He's at " + lifeLeft + " HP left. "
                + "\nHe was fought " + fCount + " times in total."
                + "\nFire charms left : " + wildWistiti.getBonusFire()
                + "\nWood charms left : " + wildWistiti.getBonusWood()
                + "\nWater charms left : " + wildWistiti.getBonusWater()
                + "\nThunder charms left : " + wildWistiti.getBonusThunder()
                + "\nWind charms left : " + wildWistiti.getBonusAir()
                + "\n\nCOMBAT LOGS :\n" + Arrays.toString(EventFightService.wistitiLogs.toArray())
                , HttpStatus.OK);

      } else {
        return new ResponseEntity<>("The Wistiti was caught! Come back tomorrow! " + "\n\nCOMBAT LOGS :\n" + Arrays.toString(EventFightService.wistitiLogs.toArray()), HttpStatus.OK);
      }
    }
    return new ResponseEntity<>("No Active Wistiti...", HttpStatus.NO_CONTENT);
  }

  @GetMapping("/consult-raidboss/{maj}")
  public ResponseEntity<String> checkRaidBossStats(@PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      EventDinozDto raidBoss = raidBossRepository.getRaidBoss();
      if (raidBoss.getLife() > 0) {
        String lifeLeft = String.valueOf(raidBoss.getLife());
        String fCount = String.valueOf(raidBoss.getFightCount());
        String raidBossInfos = "Location : " + raidBoss.getPlaceNumber() + "\n"
                + "Day : " + raidBoss.getDayOfWeek() + "\n"
                + "Hour : " + raidBoss.getHourOfDay() + "\n";

        return new ResponseEntity<>(raidBossInfos + "Raid Boss : He's at " + lifeLeft + " HP left. "
                + "\nHe was fought " + fCount + " times in total."
                + "\nFire charms left : " + raidBoss.getBonusFire()
                + "\nWood charms left : " + raidBoss.getBonusWood()
                + "\nWater charms left : " + raidBoss.getBonusWater()
                + "\nThunder charms left : " + raidBoss.getBonusThunder()
                + "\nWind charms left : " + raidBoss.getBonusAir()
                + "\n\nCOMBAT LOGS :\n" + Arrays.toString(EventFightService.raidBossLogs.toArray())
                , HttpStatus.OK);

      } else {
        return new ResponseEntity<>("The Boss is K.O!", HttpStatus.OK);
      }
    }
    return new ResponseEntity<>("No Active Raid Boss...", HttpStatus.NO_CONTENT);
  }

  @GetMapping("/lookup/{maj}")
  public ResponseEntity<List<String>> lookup(@PathVariable String maj) {
    List<String> lookups = new ArrayList<String>();
    if (maj.equalsIgnoreCase(st)) {
      for (Player account : accountService.getAllAccounts()) {
        if (account.getDailyShiniesFought() != null && account.getDailyShiniesFought() > 0) {
          lookups.add(account.getName() + " => " + account.getDailyShiniesFought() + " Daily Shinys.");
        }
      }
    }
    return new ResponseEntity<>(lookups, HttpStatus.I_AM_A_TEAPOT);
  }

  @PutMapping("/giveEpic/{maj}/{accountId}/{epicId}")
  public void giveSpecificEpicToPlayer(@PathVariable String accountId, @PathVariable String epicId, @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      var playerCollection = collectionRepository.getPlayerCollection(UUID.fromString(accountId));
      playerCollection.getEpicCollection().add(epicId);
      collectionRepository.updateEpicCollection(playerCollection.getId(), playerCollection.getEpicCollection());
    }
  }

  @DeleteMapping("/resetArenaProgressionToZero/{maj}/{accountId}")
  public void resetArenaProgressionToZero(@PathVariable String accountId, @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      var playerCollection = collectionRepository.getPlayerCollection(UUID.fromString(accountId));
      playerCollection.getEpicCollection().removeAll(Arrays.asList("4", "5", "6", "7", "8", "9", "21", "22", "23"));
      collectionRepository.updateEpicCollection(playerCollection.getId(), playerCollection.getEpicCollection());
      playerRepository.updateHermitStage(accountId, 1);
      playerRepository.updateHermitStageCurrentWins(accountId, 0);
    }
  }

  @DeleteMapping("/bazartools/{maj}/{listingId}")
  public void deleteSpecificBazarListing(@PathVariable String listingId, @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      bazarService.deleteBazarListing(listingId);
    }
  }

  @DeleteMapping("/bazartools/alllistings/{maj}")
  public void deleteNPCListings(@PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      bazarService.deleteAllNPCListingsThatDontHaveBids();
    }
  }

  @PutMapping("/distributeHalloweenEpics/{maj}")
  public void distributeHalloweenEpics(@PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      List<EventPlayerDto> eventLiveResults = new ArrayList<>();
      for (Map.Entry<Player, Long> statEntry : accountService.getCounterStatsForAllPlayers(PLAYER_STAT.EVENT_COUNTER.getName()).entrySet()) {
        if (statEntry.getValue() != null && statEntry.getValue() > 0) {
          eventLiveResults.add(new EventPlayerDto(statEntry.getKey().getId(), statEntry.getKey().getName(), statEntry.getValue()));
        }
      }
      Collections.sort(eventLiveResults);
      for (int i = 0; i < eventLiveResults.size(); i++) {
        if (i == 0) {
          addEpic(eventLiveResults, i, "24");
        } else if (i == 1) {
          addEpic(eventLiveResults, i, "25");
        } else if (i == 2) {
          addEpic(eventLiveResults, i, "26");
        } else if (eventLiveResults.get(i).getObjectsRetrievedInEvent() >= 250) {
          addEpic(eventLiveResults, i, "27");
        }
      }
    }
  }

  private void addEpic(List<EventPlayerDto> eventLiveResults, int i, String epicNumber) {
    var playerCollection = collectionRepository.getPlayerCollection(UUID.fromString(eventLiveResults.get(i).getAccountId()));
    playerCollection.getEpicCollection().add(epicNumber);
    collectionRepository.updateEpicCollection(playerCollection.getId(), playerCollection.getEpicCollection());
  }

  /**
   * _______________________________________________________________
   * <p>
   * SECTION : INTEGRATION TESTS & DEVELOPMENT SECTION! (DEV TOOLS)
   * _______________________________________________________________
   */
  @PutMapping("/giveItemsForDevTests/{accountId}/{maj}")
  public void giveItemsForDevTests(@PathVariable String accountId, @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.BONS, 10);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.COULIS_CERISE, 10);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.OEUF_GLUON, 10);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.OEUF_SANTAZ, 10);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.OEUF_SERPANTIN, 10);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.OEUF_FEROSS, 10);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.POTION_IRMA, 999);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.POTION_SOMBRE, 10);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.CHAMPIFUZ, 99);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.SPECIAL_CHAMPIFUZ, 500);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.CHARME_PRISMATIK, 99);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.GIFT, 9);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.ETERNITY_PILL, 9);

      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.FEUILLE_PACIFIQUE, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.OREADE_BLANC, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.TIGE_RONCIVORE, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.PERCHE_PERLEE, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.GREMILLE_GRELOTTANTE, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.CUBE_GLU, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.ANÉMONE_SOLITAIRE, 900);

      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.BUZE, 3000);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.PLUME, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.DENT, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.CRIN, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.BOIS, 900);

      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.TISANE, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.BIERE, 900);
      inventoryRepository.addInventoryItem(accountId, DinoparcConstants.OEUF_COBALT, 900);

      playerRepository.updateCashAll(9999999);
      playerRepository.updateHermitStage(accountId, 499);
      collectionRepository.updateEpicCollection(collectionRepository.getPlayerCollection(UUID.fromString(accountId)).getId(),
              Arrays.asList("1", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27"));

      ArrayList<String> collectionItems = new ArrayList<>();
      for (int i = 1; i <= 49; i++) {
        collectionItems.add(String.valueOf(i));
      }
      collectionRepository.updateCollection(collectionRepository.getPlayerCollection(UUID.fromString(accountId)).getId(), collectionItems);
    }
  }

  @GetMapping("/doWhatever/{maj}/{options}")
  public List<String> doWhatever(@PathVariable String maj, @PathVariable String options) {
    if (maj.equalsIgnoreCase(st)) {
      if (options.equals("clear")) {
        accountService.availableEnnemies.clear();
      } else {
        accountService.hosts.add(options);
      }
      return accountService.availableEnnemies;
    }
    return new ArrayList<>();
  }

  @PatchMapping("/transferAccount/{accountOneId}/{accountTwoId}/{maj}")
  public String transferAccountOnOther(@PathVariable String maj, @PathVariable String accountOneId, @PathVariable String accountTwoId) {
    String response = "";
    if (maj.equalsIgnoreCase(st)) {
      try {
        Player accOne = accountService.getAccountById(accountOneId).get();
        Player accTwo = accountService.getAccountById(accountTwoId).get();

        for (Dinoz dinoz : accountService.getAllDinozOfAccount(accTwo)) {
          dinoz.setMasterId(accOne.getId());
          dinoz.setMasterName(accOne.getName());
          dinozRepository.setMasterInfos(dinoz.getId(), accOne.getId(), accOne.getName());

          playerRepository.addDinoz(accOne.getId(), dinoz.getId());
          playerRepository.deleteDinoz(accTwo.getId(), dinoz.getId());
        }

        accountService.deleteAccount(accTwo.getId());
        response += "Success!";

      } catch (Exception e) {
        response += "Exception : " + e.getMessage();
      }
    }

    return response;
  }

  @DeleteMapping("/{accountId}/{maj}")
  public String deleteAccountById(@PathVariable String maj, @PathVariable String accountId) {
    String response = "";
    if (maj.equalsIgnoreCase(st)) {
      try {
        accountService.deleteAccount(accountId);
        response += "Success! Account Deleted : " + accountId;

      } catch (Exception e) {
        response += "Exception : " + e.getMessage();
      }
    }

    return response;
  }

  @DeleteMapping("/clan/{clanId}/{maj}")
  public String deleteClanById(@PathVariable String maj, @PathVariable String clanId) {
    String response = "";
    if (maj.equalsIgnoreCase(st)) {
      try {
        clanRepository.deleteGhostClan(clanId);
        response += "Success! Clan Deleted : " + clanId;

      } catch (Exception e) {
        response += "Exception : " + e.getMessage();
      }
    }

    return response;
  }

  @DeleteMapping("/dinoz/{dinozId}/{maj}")
  public String deleteDinozById(@PathVariable String maj, @PathVariable String dinozId) {
    if (maj.equalsIgnoreCase(st)) {
      try {
        Dinoz dinoz = dinozRepository.findById(dinozId).get();
        playerRepository.deleteDinoz(dinoz.getMasterId(), dinoz.getId());
        dinozRepository.deleteById(dinozId);
        return "Success : The Dinoz was deleted from account " + dinoz.getMasterName();
      } catch (Exception e) {
        return "ERROR : The Dinoz was not deleted.";
      }
    }
    return "ERROR : The Dinoz was not deleted.";
  }

  @PutMapping("/dinoz/clear-malus/{dinozId}/{maj}")
  public String clearDinozMaluses(@PathVariable String maj, @PathVariable String dinozId) {
    if (maj.equalsIgnoreCase(st)) {
      try {
        Dinoz dinoz = dinozRepository.findById(dinozId).get();
        dinoz.getMalusList().removeIf(malus -> (!malus.equals(DinoparcConstants.OEIL_DU_TIGRE)));
        dinozRepository.setMalusList(dinoz.getId(), dinoz.getMalusList());
        return "Success : The Dinoz malus list was cleared.";
      } catch (Exception e) {
        return "ERROR : The Dinoz malus list was not cleared.";
      }
    }
    return "ERROR";
  }

  @PutMapping("/transfer/{maj}/{dinozOneId}/{dinozTwoId}")
  public String transferTwoDinoz(@PathVariable String maj, @PathVariable String dinozOneId, @PathVariable String dinozTwoId) {
    String response = "";
    if (maj.equalsIgnoreCase(st)) {
      try {
        Dinoz dinozOne = accountService.getDinozById(dinozOneId);
        Dinoz dinozTwo = accountService.getDinozById(dinozTwoId);
        Player accOne = accountService.getAccountById(dinozOne.getMasterId()).get();
        Player accTwo = accountService.getAccountById(dinozTwo.getMasterId()).get();

        dinozRepository.setMasterInfos(dinozOne.getId(), accTwo.getId(), accTwo.getName());
        dinozRepository.setMasterInfos(dinozTwo.getId(), accOne.getId(), accOne.getName());

        playerRepository.addDinoz(accOne.getId(), dinozTwo.getId());
        playerRepository.deleteDinoz(accTwo.getId(), dinozTwo.getId());
        playerRepository.addDinoz(accTwo.getId(), dinozOne.getId());
        playerRepository.deleteDinoz(accOne.getId(), dinozOne.getId());

        response += "Success!";
      } catch (Exception e) {
        response += "Exception : " + e.getMessage();
      }
    }

    return response;
  }

  @PutMapping("/testLocalCreate/giveObject/{accountId}/{maj}/{object}/{qty}")
  public String giveObject(@PathVariable String accountId, @PathVariable String maj, @PathVariable String object, @PathVariable Integer qty) {
    if (maj.equalsIgnoreCase(st)) {
      try {
        Player account = accountService.getAccountById(accountId).get();
        inventoryRepository.addInventoryItem(accountId, object, qty);
        return "Success! (" + qty + ") " + object + " were given to " + account.getName();

      } catch (Exception e) {
        return "Failure! An exception was thrown : " + e.getMessage();
      }
    }

    return "Forbidden";
  }

  @PostMapping("/createList/{accountId}/{maj}/{newGold}")
  public void importDinoparcDataIntoNewAccountByHand(
          @RequestBody CreateListDto createListDto,
          @PathVariable String accountId,
          @PathVariable String maj,
          @PathVariable Integer newGold) {

    if (maj.equalsIgnoreCase(st)) {
      Optional<Player> desiredAccount = accountService.getAccountById(accountId);
      if (desiredAccount.isPresent()) {
        Player account = desiredAccount.get();
        List<Dinoz> temporaryDinozs = new ArrayList<Dinoz>();
        for (CreateDinozDto rawDinozToImport : createListDto.getDinozList()) {
          Dinoz dinoz = mapRawDinoz(rawDinozToImport);
          dinoz.setMasterId(account.getId());
          dinoz.setMasterName(account.getName());
          temporaryDinozs.add(dinoz);
          dinozRepository.create(dinoz);
          playerRepository.addDinoz(account.getId(), dinoz.getId());
        }

        playerRepository.updateCash(account.getId(), newGold);

        var playerCollection = collectionRepository.getPlayerCollection(UUID.fromString(accountId));

        if (!playerCollection.getEpicCollection().contains("14")) {
          playerCollection.getEpicCollection().add("14");
          collectionRepository.updateEpicCollection(playerCollection.getId(), playerCollection.getEpicCollection());
        }
      }
    }
  }

  @PutMapping("/setWnumber/{accountId}/{qty}/{maj}")
  public void setNumberOfWistitisCaptured(@PathVariable String accountId, @PathVariable Integer qty, @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      Player account = accountService.getAccountById(accountId).get();
      playerRepository.setWistitiCaptured(accountId, qty);
    }
  }

  private Dinoz mapRawDinoz(CreateDinozDto rawDinoz) {
    Dinoz dinozToImport = new Dinoz();
    dinozToImport.setId(UUID.randomUUID().toString());
    dinozToImport.setName(rawDinoz.name);
    dinozToImport.setLevel(1);
    dinozToImport.setRace(rawDinoz.race);
    dinozToImport.setDark(rawDinoz.isDark);
    dinozToImport.setBeginMessage(DinoparcConstants.BEGINMESSAGE1);
    dinozToImport.setEndMessage(DinoparcConstants.ENDMESSAGE1);
    dinozToImport.setLife(100);
    dinozToImport.setDanger(0);
    dinozToImport.setExperience(0);
    dinozToImport.setPlaceNumber(0);

    //App code (random)
    dinozToImport.setAppearanceCode((DinozUtils.getRaceChar(dinozToImport.getRace()) + DinozUtils.getRandomHexString()));
    DinozUtils.initializeNPCDinoz(dinozToImport);

    //Init skills at random :
    int[] randomCompetences = new Random().ints(1, 6).distinct().limit(3).toArray();
    dinozToImport.getSkillsMap().put(DinozUtils.getCompetenceNameFromNumber(randomCompetences[0]), 3);
    dinozToImport.getSkillsMap().put(DinozUtils.getCompetenceNameFromNumber(randomCompetences[1]), 2);
    dinozToImport.getSkillsMap().put(DinozUtils.getCompetenceNameFromNumber(randomCompetences[2]), 1);
    DinozUtils.concatElementWithCompetence(dinozToImport);

    //Is dark?
    if (rawDinoz.isDark) {
      dinozToImport.setAppearanceCode(dinozToImport.getAppearanceCode() + "#");
      dinozToImport.setDark(true);
      dinozToImport.getSkillsMap().put(DinoparcConstants.APPRENTI_FEU, 1);
      dinozToImport.getElementsValues().put(DinoparcConstants.FEU, dinozToImport.getElementsValues().get(DinoparcConstants.FEU) + 1);
    }

    //Skills and elements (level up logic for each level):
    Integer limit = rawDinoz.level;
    for (int i = 1; i < limit; i++) {
      Map<String, Integer> availableLearnings = accountService.getAvailableLearnings(dinozToImport);
      List<String> availableUncompletedSkills = new ArrayList<String>();

      for (String existingSkill : dinozToImport.getSkillsMap().keySet()) {
        if (dinozToImport.getSkillsMap().get(existingSkill) > 0 && dinozToImport.getSkillsMap().get(existingSkill) < 5) {
          availableUncompletedSkills.add(existingSkill);
        }
      }

      if (availableUncompletedSkills.size() > 0) {
        String skillToUp = getActualSkillToUp(dinozToImport, availableUncompletedSkills);
        accountService.levelUp(dinozToImport, skillToUp, true);

      } else {
        List<String> newPossibleSkills = new ArrayList<String>(availableLearnings.keySet());
        if (newPossibleSkills.size() > 0) {
          String newLearning = getNewLearning(dinozToImport, newPossibleSkills);
          accountService.learnNewSkill(dinozToImport, newLearning, true);

        } else {
          String randomElementBuffed = DinoparcConstants.FEU;
          if (ThreadLocalRandom.current().nextBoolean()) {
            randomElementBuffed = Dinoz.getHighestElementName(dinozToImport.getElementsValues());
          } else {
            randomElementBuffed = Dinoz.getRandomElement();
          }
          dinozToImport.getElementsValues().put(randomElementBuffed, dinozToImport.getElementsValues().get(randomElementBuffed) + 1);
          dinozToImport.setLevel(dinozToImport.getLevel() + 1);
        }
      }
    }

    //Actions :
    if (dinozToImport.getSkillsMap().get(DinoparcConstants.ROCK) != null && dinozToImport.getSkillsMap().get(DinoparcConstants.ROCK) >= 1) {
      dinozToImport.getActionsMap().put(DinoparcConstants.ROCK, true);
    }

    if (dinozToImport.getSkillsMap().get(DinoparcConstants.FOUILLER) != null && dinozToImport.getSkillsMap().get(DinoparcConstants.FOUILLER) >= 1) {
      dinozToImport.getActionsMap().put(DinoparcConstants.FOUILLER, true);
    }

    dinozToImport.getActionsMap().put(DinoparcConstants.COMBAT, true);
    dinozToImport.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);
    dinozToImport.getActionsMap().put(DinoparcConstants.TournoiDinoville, true);

    return dinozToImport;
  }

  private String getActualSkillToUp(Dinoz listedDinoz, List<String> availableUncompletedSkills) {
    String highestElement = Dinoz.getHighestElementName(listedDinoz.getElementsValues());
    for (String actualSkill : availableUncompletedSkills) {
      if (Dinoz.getElementNameFromSkillName(actualSkill).equalsIgnoreCase(highestElement)) {
        return actualSkill;
      }
    }
    return availableUncompletedSkills.get(0);
  }

  private String getNewLearning(Dinoz listedDinoz, List<String> newPossibleSkills) {
    Map<String, Integer> softCopy = new HashMap<>(listedDinoz.getElementsValues());
    String highestElement = Dinoz.getHighestElementName(softCopy);
    for (String newPossibleSkill : newPossibleSkills) {
      if (Dinoz.getElementNameFromSkillName(newPossibleSkill).equalsIgnoreCase(highestElement)) {
        return newPossibleSkill;
      }
    }

    for (int i = 0; i < 3; i++) {
      softCopy.replace(highestElement, 0);
      highestElement = Dinoz.getHighestElementName(softCopy);
      for (String newPossibleSkill : newPossibleSkills) {
        if (Dinoz.getElementNameFromSkillName(newPossibleSkill).equalsIgnoreCase(highestElement)) {
          return newPossibleSkill;
        }
      }
    }

    return newPossibleSkills.get(new Random().nextInt(newPossibleSkills.size()));
  }

  @PutMapping("/editDinoz/{accountId}/{dinozId}/{maj}")
  public void editDinozByAdmin(
          @RequestBody EditDinozDto editDinozDto,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @PathVariable String maj) {

    if (maj.equalsIgnoreCase(st)) {
      Optional<Player> desiredAccount = accountService.getAccountById(accountId);
      if (desiredAccount.isPresent()) {
        Player account = desiredAccount.get();
        var optDinoz = dinozRepository.findByIdAndPlayerId(dinozId, accountId);

        if (optDinoz.isPresent()) {
          Dinoz dinozToEdit = optDinoz.get();
          dinozToEdit.getElementsValues().put(DinoparcConstants.FEU, editDinozDto.getFeu());
          dinozToEdit.getElementsValues().put(DinoparcConstants.TERRE, editDinozDto.getTerre());
          dinozToEdit.getElementsValues().put(DinoparcConstants.EAU, editDinozDto.getEau());
          dinozToEdit.getElementsValues().put(DinoparcConstants.FOUDRE, editDinozDto.getFoudre());
          dinozToEdit.getElementsValues().put(DinoparcConstants.AIR, editDinozDto.getAir());
          dinozRepository.processLvlUp(dinozId, dinozToEdit.getSkillsMap(), dinozToEdit.getElementsValues(), dinozToEdit.getExperience(), dinozToEdit.getLevel(), dinozToEdit.getActionsMap());
        }
      }
    }
  }

  @PutMapping("/editDinozAppCode/{accountId}/{dinozId}/{appCode}/{maj}")
  public void editDinozAppCodeByAdmin(
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @PathVariable String appCode,
          @PathVariable String maj) {

    if (maj.equalsIgnoreCase(st)) {
      Optional<Player> desiredAccount = accountService.getAccountById(accountId);
      if (desiredAccount.isPresent()) {
        var optDinoz = dinozRepository.findByIdAndPlayerId(dinozId, accountId);
        optDinoz.ifPresent(dinoz -> dinozRepository.setAppearanceCode(dinoz.getId(), appCode));
      }
    }
  }

  @PostMapping("/createDinozAdmin/{maj}")
  public void createDinozByAdmin(@RequestBody CreateDinozAdminDto createDinozDto, @PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      Optional<Player> desiredAccount = accountService.getAccountById(createDinozDto.getAccountId());
      if (desiredAccount.isPresent()) {
        Player account = desiredAccount.get();
        Dinoz dinoz = new Dinoz();
        dinoz.setId(UUID.randomUUID().toString());
        dinoz.setMasterId(account.getId());
        dinoz.setMasterName(account.getName());
        dinoz.setName(account.getName());
        dinoz.setRace(createDinozDto.getRace());
        dinoz.setBeginMessage(DinoparcConstants.BEGINMESSAGE1);
        dinoz.setEndMessage(DinoparcConstants.ENDMESSAGE1);
        dinoz.setLife(100);
        dinoz.setLevel(createDinozDto.getLevel());
        dinoz.setExperience(0);
        dinoz.setDanger(0);
        dinoz.getElementsValues().put(DinoparcConstants.FEU, createDinozDto.getFeu());
        dinoz.getElementsValues().put(DinoparcConstants.TERRE, createDinozDto.getTerre());
        dinoz.getElementsValues().put(DinoparcConstants.EAU, createDinozDto.getEau());
        dinoz.getElementsValues().put(DinoparcConstants.FOUDRE, createDinozDto.getFoudre());
        dinoz.getElementsValues().put(DinoparcConstants.AIR, createDinozDto.getAir());
        dinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
        dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);
        dinoz.setPlaceNumber(0);
        dinoz.setAppearanceCode(createDinozDto.getAppearanceCode());

        if (createDinozDto.getAppearanceCode().endsWith("#")) {
          dinoz.getSkillsMap().put(DinoparcConstants.APPRENTI_FEU, 1);
          dinoz.getElementsValues().putIfAbsent(DinoparcConstants.FEU, 0);
          dinoz.getElementsValues().put(DinoparcConstants.FEU, dinoz.getElementsValues().get(DinoparcConstants.FEU) + 1);
        }

        dinozRepository.create(dinoz);
        playerRepository.updatePoints(account.getId(), account.getNbPoints() + createDinozDto.getLevel(), (account.getNbPoints()) / (playerRepository.countDinoz(account.getId()) + 1));
        playerRepository.addDinoz(account.getId(), dinoz.getId());
      }
    }
  }

  @PutMapping("/playerRole/{accountId}/{maj}/{role}")
  public void setPlayerRole(
          @PathVariable String accountId,
          @PathVariable String maj,
          @PathVariable String role) {

    if (maj.equalsIgnoreCase(st)) {
      Optional<Player> desiredAccount = accountService.getAccountById(accountId);
      if (desiredAccount.isPresent()) {
        playerRepository.updateRole(accountId, role);
      }
    }
  }

  @PutMapping("/banPlayer/{accountId}/{maj}/{banned}/{blocked}/{bannedPvp}/{bannedRaid}/{bannedShiny}/{bannedWistiti}")
  public void banPlayer(
          @PathVariable String accountId,
          @PathVariable String maj,
          @PathVariable Boolean banned,
          @PathVariable Boolean blocked,
          @PathVariable Boolean bannedPvp,
          @PathVariable Boolean bannedRaid,
          @PathVariable Boolean bannedShiny,
          @PathVariable Boolean bannedWistiti) {

    if (maj.equalsIgnoreCase(st)) {
      Optional<Player> desiredAccount = accountService.getAccountById(accountId);
      if (desiredAccount.isPresent()) {
        playerRepository.updateBanInfos(accountId, banned, blocked, bannedPvp, bannedRaid, bannedShiny, bannedWistiti);
      }
    }
  }

  @PutMapping("/wipeClanPages/{clanId}/{maj}")
  public void wipeClanPageOnIfAbuse (@PathVariable String clanId, @PathVariable String maj){
    if (maj.equalsIgnoreCase(st)) {
      clanRepository.adminWipeClanPagesContent(clanId);
    }
  }

  @PostMapping("/regenBoss/{maj}")
  public void regenBoss (@PathVariable String maj){
    if (maj.equalsIgnoreCase(st)) {
      utilsService.computeRaidBoss(true);
    }
  }

  @PostMapping("/bossArmy/start/{maj}")
  public void startRaidBossArmy (@PathVariable String maj){
    if (maj.equalsIgnoreCase(st)) {
      raidBossArmyScheduler.runArmy(true);
    }
  }

  @PostMapping("/bossArmy/clean/{maj}")
  public void cleanRaidBossArmy (@PathVariable String maj){
    if (maj.equalsIgnoreCase(st)) {
      raidBossArmyScheduler.cleanArmy(true);
    }
  }

  @PutMapping("/reset-totems/{maj}")
  public void resetTotems (@PathVariable String maj){
    if (maj.equalsIgnoreCase(st)) {
      for (Clan clan : clanRepository.getAllClans()) {
        Totem totem = totemService.generateNewTotem(neoparcConfig.getRngFactory(), 1);
        clanRepository.updateTotem(clan.getId(), totem);
      }
    }
  }

  @PostMapping("/sell-all-ingredients/{maj}")
  public List<String> sellAllIngredientsBeforeClanWar (@PathVariable String maj) {
    List<String> results = new ArrayList<>();
    if (maj.equalsIgnoreCase(st)) {
      for (Player player : playerRepository.findAll()) {
        Integer totalSumOfSoldIngredients = 0;
        Integer buzeQty = inventoryRepository.getQty(player.getId(), DinoparcConstants.BUZE);
        if (buzeQty > 0) {
          playerRepository.updateCash(player.getId(), buzeQty * 40);
          totalSumOfSoldIngredients += buzeQty * 40;
          inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.BUZE, buzeQty);
        }

        Integer plumesQty = inventoryRepository.getQty(player.getId(), DinoparcConstants.PLUME);
        if (plumesQty > 0) {
          playerRepository.updateCash(player.getId(), plumesQty * 60);
          totalSumOfSoldIngredients += plumesQty * 60;
          inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.PLUME, plumesQty);
        }

        Integer dentsQty = inventoryRepository.getQty(player.getId(), DinoparcConstants.DENT);
        if (dentsQty > 0) {
          playerRepository.updateCash(player.getId(), dentsQty * 100);
          totalSumOfSoldIngredients += dentsQty * 100;
          inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.DENT, dentsQty);
        }

        Integer crinsQty = inventoryRepository.getQty(player.getId(), DinoparcConstants.CRIN);
        if (crinsQty > 0) {
          playerRepository.updateCash(player.getId(), crinsQty * 200);
          totalSumOfSoldIngredients += crinsQty * 200;
          inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.CRIN, crinsQty);
        }

        Integer boisQty = inventoryRepository.getQty(player.getId(), DinoparcConstants.BOIS);
        if (boisQty > 0) {
          playerRepository.updateCash(player.getId(), boisQty * 500);
          totalSumOfSoldIngredients += boisQty * 500;
          inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.BOIS, boisQty);
        }

        if (totalSumOfSoldIngredients > 0) {
          History sellEvent = new History();
          sellEvent.setPlayerId(player.getId());
          sellEvent.setType("sellIngredients");
          sellEvent.setIcon("hist_buy.gif");
          sellEvent.setBuyAmount(totalSumOfSoldIngredients);
          historyRepository.save(sellEvent);
          results.add(player.getName() + " got " + totalSumOfSoldIngredients + " gold coins for his/her items.");
        }
      }
    }
    return results;
  }

  @PutMapping("/modify-clan-faction/{clanId}/{faction}/{maj}")
  public String modifyClanFaction (@PathVariable String clanId, @PathVariable String faction, @PathVariable String maj) {
    String response = "Could not modify this clan's faction.";
    if (maj.equalsIgnoreCase(st)) {
      switch (faction) {
        case (DinoparcConstants.FACTION_1):
          clanRepository.updateFaction(clanId, DinoparcConstants.FACTION_1);
          break;
        case (DinoparcConstants.FACTION_2):
          clanRepository.updateFaction(clanId, DinoparcConstants.FACTION_2);
          break;
        case (DinoparcConstants.FACTION_3):
          clanRepository.updateFaction(clanId, DinoparcConstants.FACTION_3);
          break;
      }
      response = "The faction was modified with success!";
    }
    return response;
  }

  @PostMapping("/clan-war/end/{maj}")
  public void endClanWar(@PathVariable String maj) {
    if (maj.equalsIgnoreCase(st)) {
      List<String> clanIdsInOrderOfRankings = clanService.getClansRanking()
              .stream()
              .map(RankingClanDto::getClanId)
              .toList();

      for (int i = 0; i < clanIdsInOrderOfRankings.size(); i++) {
        String clanId = clanIdsInOrderOfRankings.get(i);
        Clan clan = clanRepository.getClan(clanId).get();

        //Enlever le statut de Guerriers des Dinozs du clan :
        for (Dinoz dinoz : clanRepository.getAllWarriorDinozOfClan(clanId)) {
          if (dinoz.getMalusList() != null && dinoz.getMalusList().contains(DinoparcConstants.FACTION_1_GUERRIER)) {
            dinoz.getMalusList().remove(DinoparcConstants.FACTION_1_GUERRIER);
          }
          if (dinoz.getMalusList() != null && dinoz.getMalusList().contains(DinoparcConstants.FACTION_2_GUERRIER)) {
            dinoz.getMalusList().remove(DinoparcConstants.FACTION_2_GUERRIER);
          }
          if (dinoz.getMalusList() != null && dinoz.getMalusList().contains(DinoparcConstants.FACTION_3_GUERRIER)) {
            dinoz.getMalusList().remove(DinoparcConstants.FACTION_3_GUERRIER);
          }
          dinozRepository.setMalusList(dinoz.getId(), dinoz.getMalusList());
        }

        //Récompenser les joueurs et mise à zéro des compteurs :
        for (String memberId : clan.getMembers()) {
          if (i < 3) {
            var playerCollection = collectionRepository.getPlayerCollection(UUID.fromString(memberId));
            playerRepository.updateCash(memberId, (900000 - (150000 * i)));
            inventoryRepository.addInventoryItem(memberId, DinoparcConstants.OEUF_FEROSS, 1);
            playerCollection.getEpicCollection().add("30");
            playerCollection.getEpicCollection().add("31");
            collectionRepository.updateEpicCollection(playerCollection.getId(), playerCollection.getEpicCollection());
          } else if (clan.getNbPointsOccupation() + clan.getNbPointsFights() + clan.getNbPointsTotem() >= DinoparcConstants.POINTS_MINIMUM_FOR_SECOND_EPIC) {
            var playerCollection = collectionRepository.getPlayerCollection(UUID.fromString(memberId));
            playerCollection.getEpicCollection().add("31");
            collectionRepository.updateEpicCollection(playerCollection.getId(), playerCollection.getEpicCollection());
            inventoryRepository.addInventoryItem(memberId, DinoparcConstants.OEUF_FEROSS, 1);
          }

          Optional<PlayerClan> clanMember = clanRepository.getPlayerClan(memberId);
          if (clanMember.isPresent()) {
            clanMember.get().setNbPointsOccupation(0);
            clanMember.get().setNbPointsFights(0);
            clanMember.get().setNbPointsTotem(0);
            clanRepository.updatePlayerClan(clanMember.get());
          }
        }

        //Reset les compteurs de tous les points des clans pour repartir à neuf :
        clanRepository.resetClanPoints(clanIdsInOrderOfRankings.get(i));

        //Reset les potions de guerre dans le coffre du clan :
        Map<String, Integer> chest = clan.getChest();
        if (chest != null) {
          chest.put(DinoparcConstants.POTION_GUERRE, 0);
          clanRepository.updateChest(clanId, chest);
        }

        //3. Attribution de la faction selon le classement du clan (i) :
        computeFactionForClanByRanking(i, clanId);
      }
    }
  }

  private void computeFactionForClanByRanking(Integer clanRanking, String clanId) {
    switch (clanRanking) {
      case 0 :
      case 5 :
      case 6 :
        clanRepository.updateFaction(clanId, DinoparcConstants.FACTION_2);
        break;

      case 1 :
      case 4 :
      case 7 :
        clanRepository.updateFaction(clanId, DinoparcConstants.FACTION_3);
        break;

      case 2 :
      case 3 :
      case 8 :
        clanRepository.updateFaction(clanId, DinoparcConstants.FACTION_1);
        break;

      default :
        clanRepository.updateFaction(clanId, clanService.getRandomFaction());
        break;
    }
  }
}
