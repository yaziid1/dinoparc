package com.dinoparc.api.controllers;

import com.dinoparc.api.controllers.dto.*;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.News;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.bazar.BazarListing;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.EventDinozDto;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.repository.PgInventoryRepository;
import com.dinoparc.api.repository.PgWildWistitiRepository;
import com.dinoparc.api.services.AccountService;
import com.dinoparc.api.services.BazarService;
import com.dinoparc.api.services.UtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static com.dinoparc.tables.PlayerStat.PLAYER_STAT;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/utils")
public class UtilsController {

  private final UtilsService utilsService;
  private final AccountService accountService;
  private final TokenAccountValidation tokenAccountValidation;
  private final BazarService bazarService;
  private final PgInventoryRepository inventoryRepository;
  private final PgWildWistitiRepository wildWistitiRepository;

  @Autowired
  public UtilsController(UtilsService utilsService, AccountService accountService, TokenAccountValidation tokenAccountValidation,
                         BazarService bazarService, PgInventoryRepository inventoryRepository, PgWildWistitiRepository wildWistitiRepository) {
    this.utilsService = utilsService;
    this.accountService = accountService;
    this.tokenAccountValidation = tokenAccountValidation;
    this.bazarService = bazarService;
    this.inventoryRepository = inventoryRepository;
    this.wildWistitiRepository = wildWistitiRepository;
  }

  @GetMapping("/rankings/moyenne/{pageNumber}")
  public List<PublicAccountDto> getAverageRankings(@PathVariable Integer pageNumber) {
    return utilsService.getAverageRankingsAtGivenPage(pageNumber);
  }

  @GetMapping("/rankings/general/{pageNumber}")
  public List<PublicAccountDto> getGeneralRankings(@PathVariable Integer pageNumber) {
    return utilsService.getGeneralRankingsAtGivenPage(pageNumber);
  }

  @GetMapping("/rankings/general/user/{username}")
  public Integer getUsernamePage(@PathVariable String username) {
    return utilsService.getUsernameRanking(username);
  }

  @GetMapping("/rankings/moyenne/user/{username}")
  public Integer getUsernamePageX(@PathVariable String username) {
    return utilsService.getUsernameRankingX(username);
  }

  @GetMapping("/rankings/general/totalPages")
  public Integer getGeneralRankingsTotalPages() {
    return utilsService.getGeneralRankingsTotalPages();
  }

  @GetMapping("/numberplayers")
  public Integer getNumberOfConnectedPlayers() {
    return accountService.getAllAccounts().size();
  }
  
  @GetMapping("/numberdinozs")
  public Long getNumberOfDinozTotalServer() {
    return accountService.getNumberOfDinozOnServer();
  }

  @GetMapping("/news")
  public List<News> getLatestNews() {
    List<News> newsList = utilsService.getLatestNews();
    Collections.sort(newsList);
    return newsList;
  }

  @GetMapping("/player/{userId}/{pageNumber}")
  public PublicAccountDto getPlayerSummary(@PathVariable String userId, @PathVariable Integer pageNumber) {
    return utilsService.getPublicAccountById(userId, pageNumber);
  }

  @GetMapping("/player/{userId}/totalpages")
  public Integer getPlayerSummaryTotalPages(@PathVariable String userId) {
    return utilsService.getTotalPages(userId);
  }


  @GetMapping("/players")
  public HashMap<String, String> getAllPlayers() {
    return utilsService.getAllPlayerIds();
  }
  
  @GetMapping("/jungle-status")
  public Boolean getJungleStatus() {
    EventDinozDto wildWistiti = wildWistitiRepository.getWildWistiti();
    return wildWistiti.getLife() > 0;
  }

  @GetMapping("/creditInfos/{accountId}")
  public List<Integer> getBankValues(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
      List<Integer> creditArboricoleInitValues = utilsService.getBankValues();
      creditArboricoleInitValues.add(desiredAccount.get().getCash());
      creditArboricoleInitValues.add(inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.BONS));
      return creditArboricoleInitValues;
    }

    return null;
  }

  @PostMapping("/{accountId}/sellBons/{bonsValue}")
  public boolean sellBons(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable Integer bonsValue,
          @RequestBody BuyRequestDto buyRequest) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    Integer bonsNumber = Integer.parseInt(buyRequest.getBonsSell());

    if (AccountController.isValidAndNotBlockedAccount(desiredAccount) && bonsNumber > 0 && bonsValue.intValue() == utilsService.getBankValues().get(1).intValue()) {
      return accountService.sellBonsDirectly(bonsNumber, desiredAccount.get(), utilsService.getBankValues().get(1));
    }

    return false;
  }

  @PostMapping("/{accountId}/buyBons/{bonsValue}")
  public boolean buyBons(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable Integer bonsValue,
          @RequestBody BuyRequestDto buyRequest) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    Integer bonsNumber = Integer.parseInt(buyRequest.getBonsBuy());

    if (AccountController.isValidAndNotBlockedAccount(desiredAccount) && bonsNumber > 0 && bonsValue.intValue() == utilsService.getBankValues().get(0).intValue()) {
      return accountService.buyBonsDirectly(bonsNumber, desiredAccount.get(), utilsService.getBankValues().get(0));
    }

    return false;
  }

  // FIXME : remove this method when front will use /dinozs endpoint
  @Deprecated
  @GetMapping("/all-bazar-listings/{accountId}/{sort}")
  public List<BazarListing> getAllBazarListings(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String sort) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
      return desiredAccount.map(account -> bazarService.deprecatedGetAllBazarListings(account, sort)).orElse(null);
    }

    return null;
  }

  // FIXME : remove this method when front will use /dinozs endpoint
  @Deprecated
  @GetMapping("/chapter-rankings/{chapter}")
  public List<HOFDinozDto> getChapterRanking(@PathVariable String chapter) {
    if (chapter != null && !chapter.isEmpty()) {
      return utilsService.getRaceRankings(chapter);
    }
    return new ArrayList<>();
  }

  // FIXME : remove this method when front will use /bazar endpoint
  @Deprecated
  @GetMapping("/bazar/{dinozId}")
  public Dinoz getBazarDinozById(@PathVariable String dinozId) {
    return bazarService.getBazarDinoz(dinozId);
  }

  @GetMapping("/prize/{accountId}")
  public WheelPrizeDto getRandomPrizeAtDinotownWheel(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (AccountController.isValidAndNotBlockedAccount(desiredAccount) && inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.GIFT) > 0) {
      return utilsService.getRandomChristmasPrize(desiredAccount.get());
    }
    return null;
  }

  @GetMapping("/event-stats")
  public List<EventPlayerDto> getPublicEventStats() {
    List<EventPlayerDto> eventLiveResults = new ArrayList<>();
    for (Map.Entry<Player, Long> statEntry : accountService.getCounterStatsForAllPlayers(PLAYER_STAT.EVENT_COUNTER.getName()).entrySet()) {
      if (statEntry.getValue() != null && statEntry.getValue() > 0) {
        eventLiveResults.add(new EventPlayerDto(statEntry.getKey().getId(), statEntry.getKey().getName(), statEntry.getValue()));
      }
    }
    Collections.sort(eventLiveResults);
    return eventLiveResults;
  }
}