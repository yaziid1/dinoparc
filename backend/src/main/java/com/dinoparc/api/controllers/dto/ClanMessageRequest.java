package com.dinoparc.api.controllers.dto;

public class ClanMessageRequest {

    private String authorId;
    private String message;
    public ClanMessageRequest() {

    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
