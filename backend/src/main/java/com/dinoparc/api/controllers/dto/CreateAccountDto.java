package com.dinoparc.api.controllers.dto;

public class CreateAccountDto {

  private String usernameInscription;

  public CreateAccountDto() {

  }

  public CreateAccountDto(String usernameInscription) {
    this.usernameInscription = usernameInscription;
  }

  public String getUsernameInscription() {
    return usernameInscription;
  }

  public void setUsernameInscription(String usernameInscription) {
    this.usernameInscription = usernameInscription;
  }

}
