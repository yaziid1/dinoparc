package com.dinoparc.api.controllers.dto;

public class SacrificeResultDto {

  private Double newPercentage;
  private Integer potionsWon;
  
  public SacrificeResultDto() {
    
  }

  public Double getNewPercentage() {
    return newPercentage;
  }

  public void setNewPercentage(Double newPercentage) {
    this.newPercentage = newPercentage;
  }

  public Integer getPotionsWon() {
    return potionsWon;
  }

  public void setPotionsWon(Integer potionsWon) {
    this.potionsWon = potionsWon;
  }
  
}
