package com.dinoparc.api.controllers;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.GameMasterAction;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.clan.Clan;
import com.dinoparc.api.domain.clan.PlayerClan;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.repository.PgClanRepository;
import com.dinoparc.api.repository.PgGameMasterActionsRepository;
import com.dinoparc.api.repository.PgPlayerRepository;
import com.dinoparc.api.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/game-master")
public class GameMasterController {

    private AccountService accountService;
    private PgClanRepository clanRepository;
    private PgGameMasterActionsRepository gameMasterActionsRepository;
    private PgPlayerRepository playerRepository;
    private TokenAccountValidation tokenAccountValidation;

    @Autowired
    public GameMasterController(
            AccountService accountService,
            PgClanRepository clanRepository,
            PgGameMasterActionsRepository gameMasterActionsRepository,
            PgPlayerRepository playerRepository,
            TokenAccountValidation tokenAccountValidation
    ) {
        this.accountService = accountService;
        this.clanRepository = clanRepository;
        this.gameMasterActionsRepository = gameMasterActionsRepository;
        this.playerRepository = playerRepository;
        this.tokenAccountValidation = tokenAccountValidation;
    }

    @PutMapping("/{gmAccountId}/banPlayer/{accountId}/{banned}/{blocked}/{bannedPvp}/{bannedRaid}/{bannedShiny}/{bannedWistiti}")
    public GameMasterAction banPlayer(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @PathVariable String accountId,
            @PathVariable Boolean banned,
            @PathVariable Boolean blocked,
            @PathVariable Boolean bannedPvp,
            @PathVariable Boolean bannedRaid,
            @PathVariable Boolean bannedShiny,
            @PathVariable Boolean bannedWistiti) {

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setTargetAccountId(accountId);
        gameMasterAction.setActionType("banPlayer");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            // If game master account exists, retrieve player to block/ban
            Optional<Player> desiredAccount = accountService.getAccountById(accountId);

            if (desiredAccount.isPresent()) {
                // If player to block/ban exists, execute requested block/ban
                playerRepository.updateBanInfos(accountId, banned, blocked, bannedPvp, bannedRaid, bannedShiny, bannedWistiti);

                gameMasterAction.setResult("Done for user " + desiredAccount.get().getName());
                gameMasterAction.setSuccess(true);
                gameMasterAction.setForbidden(false);
            } else {
                // Player to block/ban does not exist
                gameMasterAction.setResult("No user found for accountId " + accountId);
                gameMasterAction.setSuccess(false);
                gameMasterAction.setForbidden(false);
            }
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
        }

        return gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
    }

    @GetMapping("/{gmAccountId}/fixClansPoints/{doIt}")
    public List<String> fixClansPoints(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @PathVariable String doIt) {

        List<String> ret = new ArrayList<>();

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setActionType("fixClansPoints");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            // If game master account exists, try to fix clans points
            List<Clan> allClans = clanRepository.getAllClans();

            for (Clan clan : allClans) {
                if ("Eternal DinoRPG".equals(clan.getName())) {
                    ret.add("- Ignoring Eternal DinoRPG clan due to DB consistency issue");
                } else {
                    List<String> clanPlayerIds = clan.getMembers();
                    int sumFights = 0;
                    int sumTotem = 0;

                    for (String clanPlayerId : clanPlayerIds) {
                        PlayerClan playerClan = clanRepository.getPlayerClan(clanPlayerId).get();
                        sumFights += playerClan.getNbPointsFights();
                        sumTotem += playerClan.getNbPointsTotem();
                    }

                    if (sumFights > clan.getNbPointsFights()) {
                        int missingFightPoints = sumFights - clan.getNbPointsFights();

                        if (Boolean.valueOf(doIt)) {
                            ret.add("- Going to fix fights points of '" + clan.getName() + "', adding : " + missingFightPoints);
                            clanRepository.addClanPointsFights(clan.getId(), missingFightPoints);
                        } else {
                            ret.add("- Need to fix fights points of '" + clan.getName() + "', need to add : " + missingFightPoints);
                        }
                    }

                    if (sumTotem > clan.getNbPointsTotem()) {
                        int missingTotemPoints = sumTotem - clan.getNbPointsTotem();

                        if (Boolean.valueOf(doIt)) {
                            ret.add("- Going to fix totem points of '" + clan.getName() + "', adding : " + missingTotemPoints);
                            clanRepository.addClanPointsFouilles(clan.getId(), missingTotemPoints);
                        } else {
                            ret.add("- Need to fix totem points of '" + clan.getName() + "', need to add : " + missingTotemPoints);
                        }
                    }

                    gameMasterAction.setResult(ret.stream().reduce("", String::concat));
                }
            }
            gameMasterAction.setSuccess(true);
            gameMasterAction.setForbidden(false);
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
            ret.add("No right to do this action");
        }

        gameMasterActionsRepository.createGameMasterAction(gameMasterAction);

        return ret;
    }
}
