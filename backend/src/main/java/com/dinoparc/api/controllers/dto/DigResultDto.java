package com.dinoparc.api.controllers.dto;

public class DigResultDto {

  /**
   * Code 1 means Collection object was found. 
   * Code 2 means Totem element was found. 
   * Code 3 means nothing was found. 
   * Code 4 means ultra-rare was found.
   */
  private Integer digCode;

  /**
   * digObject x means Collection objet x was found. 
   * digObject 1-5 means Totem 1-5 element was found. 
   * digObject 0 means nothing was found.
   */
  
  /** Ultra rares :
   * digObject 1 means bons du trésor was found.
   * digObject 2 means nuage-burger was found.
   * digObject 3 means griffes empoisonnées were found.
   */
  private Integer digObject;

  public DigResultDto() {

  }

  public Integer getDigCode() {
    return digCode;
  }

  public void setDigCode(Integer digCode) {
    this.digCode = digCode;
  }

  public Integer getDigObject() {
    return digObject;
  }

  public void setDigObject(Integer digObject) {
    this.digObject = digObject;
  }

}
