package com.dinoparc.api.controllers.dto;

public class CheckDto {

  private String enc;
  private String userId;
  private String userDisplayName;

  public CheckDto() {

  }

  public String getEnc() {
    return enc;
  }

  public void setEnc(String enc) {
    this.enc = enc;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserDisplayName() {
    return userDisplayName;
  }

  public void setUserDisplayName(String userDisplayName) {
    this.userDisplayName = userDisplayName;
  }

}
