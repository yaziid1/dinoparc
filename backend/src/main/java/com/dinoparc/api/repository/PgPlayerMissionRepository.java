package com.dinoparc.api.repository;

import com.dinoparc.api.domain.mission.PlayerMission;
import java.util.List;
import java.util.UUID;

public interface PgPlayerMissionRepository {
    PlayerMission create(PlayerMission playerMission, boolean daily, boolean weekly, boolean monthly);

    List<PlayerMission> getAllMissions(UUID playerId);

    List<PlayerMission> getPlayerMissions(UUID playerId);

    List<PlayerMission> getPlayerDinozMissions(UUID playerId, String dinozId);

    PlayerMission get(UUID playerMissionId);

    void delete(UUID playerMissionId);

    void rewardClaimed(UUID playerMissionId);

    void deletePlayerMission(UUID fromString);

    void deletePlayerDinozMission(String dinozId);

    void setCounterCurrentValue(UUID id, Long currentCounter);
}
