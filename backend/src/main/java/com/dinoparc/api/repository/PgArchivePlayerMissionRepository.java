package com.dinoparc.api.repository;

import com.dinoparc.api.domain.mission.PlayerMission;

public interface PgArchivePlayerMissionRepository {
    void create(PlayerMission playerMission);
}
