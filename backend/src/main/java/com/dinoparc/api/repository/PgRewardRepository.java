package com.dinoparc.api.repository;

import com.dinoparc.api.domain.mission.Reward;
import java.util.UUID;

public interface PgRewardRepository {
    void create(Reward reward);
    Reward get(UUID rewardId);

}
