package com.dinoparc.api.repository;

import com.dinoparc.api.domain.mission.PlayerMission;
import com.dinoparc.tables.records.PlayerMissionRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
        import java.util.UUID;

        import static com.dinoparc.tables.PlayerMission.PLAYER_MISSION;

@Repository
public class JooqPlayerMissionRepository implements PgPlayerMissionRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqPlayerMissionRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public PlayerMission create(PlayerMission playerMission, boolean daily, boolean weekly, boolean monthly) {
        return jooqContext.insertInto(PLAYER_MISSION).set(toRecord(playerMission, daily, weekly, monthly)).returning().fetchOneInto(PlayerMission.class);
    }

    private PlayerMissionRecord toRecord(PlayerMission playerMission, boolean daily, boolean weekly, boolean monthly) {
        var ret = jooqContext.newRecord(PLAYER_MISSION, playerMission);
        ZonedDateTime neoparcTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        ZonedDateTime expirationTime = daily ? neoparcTime.plusDays(1) : weekly ? neoparcTime.plusDays(7) : neoparcTime.plusDays(30);
        ret.set(PLAYER_MISSION.ID, UUID.randomUUID());
        ret.set(PLAYER_MISSION.BEGIN_DATE, neoparcTime.toOffsetDateTime());
        ret.set(PLAYER_MISSION.EXPIRATION_DATE, expirationTime.toOffsetDateTime());
        return ret;
    }

    @Override
    public List<PlayerMission> getAllMissions(UUID playerId) {
        return jooqContext.selectFrom(PLAYER_MISSION)
                .where(PLAYER_MISSION.PLAYER_ID.eq(playerId))
                .fetchInto(PlayerMission.class);
    }

    @Override
    public List<PlayerMission> getPlayerMissions(UUID playerId) {
        return jooqContext.selectFrom(PLAYER_MISSION)
                .where(PLAYER_MISSION.PLAYER_ID.eq(playerId))
                .and(PLAYER_MISSION.DINOZ_ID.isNull())
                .fetchInto(PlayerMission.class);
    }

    @Override
    public List<PlayerMission> getPlayerDinozMissions(UUID playerId, String dinozId) {
        return jooqContext.selectFrom(PLAYER_MISSION)
                .where(PLAYER_MISSION.PLAYER_ID.eq(playerId))
                .and(PLAYER_MISSION.DINOZ_ID.eq(dinozId))
                .fetchInto(PlayerMission.class);
    }

    @Override
    public void setCounterCurrentValue(UUID id, Long currentCounter) {
        jooqContext.update(PLAYER_MISSION).set(PLAYER_MISSION.COUNTER_CURRENT_VALUE, currentCounter).where(PLAYER_MISSION.ID.eq(id)).execute();
    }

    @Override
    public void rewardClaimed(UUID id) {
        jooqContext.update(PLAYER_MISSION).set(PLAYER_MISSION.REWARD_CLAIMED, true).where(PLAYER_MISSION.ID.eq(id)).execute();
    }

    @Override
    public void deletePlayerMission(UUID playerId) {
        jooqContext.deleteFrom(PLAYER_MISSION).where(PLAYER_MISSION.PLAYER_ID.eq(playerId)).execute();
    }

    @Override
    public void deletePlayerDinozMission(String dinozId) {
        jooqContext.deleteFrom(PLAYER_MISSION).where(PLAYER_MISSION.DINOZ_ID.eq(dinozId)).execute();
    }

    @Override
    public PlayerMission get(UUID id) {
        return jooqContext.selectFrom(PLAYER_MISSION).where(PLAYER_MISSION.ID.eq(id)).fetchOneInto(PlayerMission.class);
    }

    @Override
    public void delete(UUID id) {
        jooqContext.deleteFrom(PLAYER_MISSION).where(PLAYER_MISSION.ID.eq(id)).execute();
    }
}
