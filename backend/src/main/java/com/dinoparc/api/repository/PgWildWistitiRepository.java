package com.dinoparc.api.repository;

import com.dinoparc.api.domain.dinoz.EventDinozDto;
import com.dinoparc.api.domain.dinoz.EventDinozUpdate;

public interface PgWildWistitiRepository {
    public EventDinozDto getWildWistiti();
    EventDinozDto updateWildWistiti(EventDinozUpdate eventDinozUpdate);
    void resetWildWistiti(String appearanceCode, int life, int fightCount, int bonusFire, int bonusWood, int bonusWater, int bonusThunder, int bonusAir);
}
