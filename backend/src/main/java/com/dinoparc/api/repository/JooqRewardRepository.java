package com.dinoparc.api.repository;

import com.dinoparc.api.domain.mission.Reward;
import com.dinoparc.tables.records.RewardRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.DSLContext;
import org.jooq.JSONB;
import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.dinoparc.tables.Player.PLAYER;
import static com.dinoparc.tables.Reward.REWARD;

@Repository
public class JooqRewardRepository implements PgRewardRepository {

    private DSLContext jooqContext;
    private ObjectMapper objectMapper;

    @Autowired
    public JooqRewardRepository(DSLContext jooqContext, ObjectMapper objectMapper) {
        this.jooqContext = jooqContext;
        this.objectMapper = objectMapper;
    }

    @Override
    public Reward get(UUID rewardId) {
        return jooqContext.selectFrom(REWARD).where(REWARD.ID.eq(rewardId)).fetchOne().map(r -> fromRecord(r));
    }

    @Override
    public void create(Reward reward) {
        jooqContext.insertInto(REWARD).set(toRecord(reward)).execute();
    }

    private RewardRecord toRecord(Reward reward) {
        RewardRecord ret = jooqContext.newRecord(REWARD);

        ret.setId(reward.getId());
        ret.setDescription(reward.getDescription());
        ret.setGoldAmount(reward.getGoldAmount());

        if (reward.getCollectionContent() != null) {
            try {
                var collectionContent = JSONB.valueOf(objectMapper.writeValueAsString(reward.getCollectionContent()));
                ret.setCollectionContent(collectionContent);
            } catch(Exception e) {
                // Error in collectionContent create
            }
        }

        if (reward.getInventoryContent() != null) {
            try {
                var inventoryContent = JSONB.valueOf(objectMapper.writeValueAsString(reward.getInventoryContent()));
                ret.setInventoryContent(inventoryContent);
            } catch(Exception e) {
                // Error in collectionContent create
            }
        }

        return ret;
    }

    private Reward fromRecord(Record record) {
        var rewardRecord = (RewardRecord) record;
        Reward ret = new Reward();

        ret.setId(rewardRecord.getId());
        ret.setDescription(rewardRecord.getDescription());
        ret.setGoldAmount(rewardRecord.getGoldAmount());
        try {
            ret.setCollectionContent(rewardRecord.getCollectionContent() == null ? null : objectMapper.readValue(rewardRecord.getCollectionContent().data(), List.class));
        } catch(Exception e) {
            ret.setCollectionContent(null);
        }
        try {
            ret.setInventoryContent(rewardRecord.getInventoryContent() == null ? null : objectMapper.readValue(rewardRecord.getInventoryContent().data(), Map.class));
        } catch(Exception e) {
            ret.setInventoryContent(null);
        }

        return ret;
    }
}
