package com.dinoparc.api.repository;

import static com.dinoparc.tables.Boss.BOSS;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.dinoz.EventDinozDto;
import com.dinoparc.api.domain.dinoz.EventDinozUpdate;
import com.dinoparc.tables.records.BossRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.dinoparc.api.domain.account.DinoparcConstants.RAID_BOSS_ID;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class JooqBossRepository implements PgBossRepository {
    private DSLContext jooqContext;

    @Autowired
    public JooqBossRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public EventDinozDto getRaidBoss() {
        return jooqContext.selectFrom(BOSS).where(BOSS.BOSS_ID.eq(RAID_BOSS_ID)).fetch().map(this::fromRecord).get(0);
    }

    @Override
    public EventDinozDto updateRaidBoss(EventDinozUpdate eventDinozUpdate) {
        EventDinozDto raidBoss = jooqContext.update(BOSS)
                .set(BOSS.FIGHT_COUNT, BOSS.FIGHT_COUNT.add(1))
                .set(BOSS.LIFE, BOSS.LIFE.sub(eventDinozUpdate.getLife()))
                .set(BOSS.BONUS_FIRE, BOSS.BONUS_FIRE.sub(eventDinozUpdate.getBonusFire()))
                .set(BOSS.BONUS_WOOD, BOSS.BONUS_WOOD.sub(eventDinozUpdate.getBonusWood()))
                .set(BOSS.BONUS_WATER, BOSS.BONUS_WATER.sub(eventDinozUpdate.getBonusWater()))
                .set(BOSS.BONUS_THUNDER, BOSS.BONUS_THUNDER.sub(eventDinozUpdate.getBonusThunder()))
                .set(BOSS.BONUS_AIR, BOSS.BONUS_AIR.sub(eventDinozUpdate.getBonusAir()))
                .set(BOSS.BONUS_CLAW, BOSS.BONUS_CLAW.sub(eventDinozUpdate.getBonusClaw()))
                .where(BOSS.BOSS_ID.eq(RAID_BOSS_ID))
                .returning()
                .fetch()
                .map(this::fromRecord)
                .get(0);

        if (raidBoss.getLife() < 0) {
            raidBoss = jooqContext.update(BOSS).set(BOSS.LIFE, 0).where(BOSS.BOSS_ID.eq(RAID_BOSS_ID)).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusFire() < 0) {
            raidBoss = jooqContext.update(BOSS).set(BOSS.BONUS_FIRE, 0).where(BOSS.BOSS_ID.eq(RAID_BOSS_ID)).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusWood() < 0) {
            raidBoss = jooqContext.update(BOSS).set(BOSS.BONUS_WOOD, 0).where(BOSS.BOSS_ID.eq(RAID_BOSS_ID)).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusWater() < 0) {
            raidBoss = jooqContext.update(BOSS).set(BOSS.BONUS_WATER, 0).where(BOSS.BOSS_ID.eq(RAID_BOSS_ID)).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusThunder() < 0) {
            raidBoss = jooqContext.update(BOSS).set(BOSS.BONUS_THUNDER, 0).where(BOSS.BOSS_ID.eq(RAID_BOSS_ID)).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusAir() < 0) {
            raidBoss = jooqContext.update(BOSS).set(BOSS.BONUS_AIR, 0).where(BOSS.BOSS_ID.eq(RAID_BOSS_ID)).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusClaw() < 0) {
            raidBoss = jooqContext.update(BOSS).set(BOSS.BONUS_CLAW, 0).where(BOSS.BOSS_ID.eq(RAID_BOSS_ID)).returning().fetch().map(this::fromRecord).get(0);
        }

        return raidBoss;
    }

    @Override
    public void resetRaidBoss(Integer placeNumber, Integer dayOfWeek, Integer hourOfDay, String appearanceCode, int fightCount, int fire, int wood, int water,
                              int thunder, int air, int life, int bonusFire, int bonusWood, int bonusWater, int bonusThunder, int bonusAir, int bonusClaws,
                              String firstElement, String secondElement, String thirdElement) {
        jooqContext.update(BOSS)
                .set(BOSS.PLACE_NUMBER, placeNumber)
                .set(BOSS.DAY_OF_WEEK, dayOfWeek)
                .set(BOSS.HOUR_OF_DAY, hourOfDay)
                .set(BOSS.APPEARANCE_CODE, appearanceCode)
                .set(BOSS.FIGHT_COUNT, fightCount)
                .set(BOSS.FIRE, fire)
                .set(BOSS.WOOD, wood)
                .set(BOSS.WATER, water)
                .set(BOSS.THUNDER, thunder)
                .set(BOSS.AIR, air)
                .set(BOSS.LIFE, life)
                .set(BOSS.BONUS_FIRE, bonusFire)
                .set(BOSS.BONUS_WOOD, bonusWood)
                .set(BOSS.BONUS_WATER, bonusWater)
                .set(BOSS.BONUS_THUNDER, bonusThunder)
                .set(BOSS.BONUS_AIR, bonusAir)
                .set(BOSS.BONUS_CLAW, bonusClaws)
                .set(BOSS.FIRST_ELEMENT, firstElement)
                .set(BOSS.SECOND_ELEMENT, secondElement)
                .set(BOSS.THIRD_ELEMENT, thirdElement)
                .set(BOSS.ARMY_ATTACKING, false)
                .set(BOSS.MINUTES_DURATION, 30)
                .set(BOSS.REMAINING_ARMY_ATTACKS, 7500)
                .where(BOSS.BOSS_ID.eq(RAID_BOSS_ID))
                .execute();
    }

    @Override
    public void reviveRaidBoss(Integer hp, Integer charms) {
        jooqContext.update(BOSS)
                .set(BOSS.LIFE, hp)
                .set(BOSS.BONUS_FIRE, charms)
                .set(BOSS.BONUS_WOOD, charms)
                .set(BOSS.BONUS_WATER, charms)
                .set(BOSS.BONUS_THUNDER, charms)
                .set(BOSS.BONUS_AIR, charms)
                .set(BOSS.REMAINING_ARMY_ATTACKS, DinoparcConstants.RAID_BOSS_MAX_NB_ATTACK)
                .where(BOSS.BOSS_ID.eq(RAID_BOSS_ID))
                .execute();
    }

    @Override
    public void setArmyAttacking(UUID bossId, boolean armyAttacking) {
        jooqContext.update(BOSS).set(BOSS.ARMY_ATTACKING, true).where(BOSS.BOSS_ID.eq(bossId)).execute();
    }

    @Override
    public EventDinozDto decrementBossRemainingArmyAttacks(UUID bossId) {
        return jooqContext.update(BOSS)
                .set(BOSS.REMAINING_ARMY_ATTACKS, BOSS.REMAINING_ARMY_ATTACKS.sub(1))
                .where(BOSS.BOSS_ID.eq(bossId))
                .returning()
                .fetch()
                .map(this::fromRecord).get(0);
    }

    @Override
    public Optional<EventDinozDto> getBoss(UUID bossId) {
        return jooqContext.selectFrom(BOSS)
                .where(BOSS.BOSS_ID.eq(bossId))
                .fetchOptional()
                .map(this::fromRecord);
    }

    @Override
    public List<EventDinozDto> findByPlaceNumberAndDayOfWeek(int placeNumber, int dayOfWeek) {
        return jooqContext.selectFrom(BOSS)
                .where(BOSS.PLACE_NUMBER.eq(placeNumber))
                .and(BOSS.DAY_OF_WEEK.eq(dayOfWeek))
                .fetch()
                .map(this::fromRecord);
    }

    @Override
    public void setRemainingArmyAttacks(UUID bossId, int remainingNbAttacks) {
        jooqContext.update(BOSS)
                .set(BOSS.REMAINING_ARMY_ATTACKS, remainingNbAttacks)
                .where(BOSS.BOSS_ID.eq(bossId))
                .returning()
                .fetch()
                .map(this::fromRecord).get(0);
    }

    private EventDinozDto fromRecord(BossRecord record) {
        EventDinozDto ret = new EventDinozDto();

        ret.setBossId(record.getBossId().toString());

        ret.setAppearanceCode(record.getAppearanceCode());
        ret.setName(record.getName());
        ret.setPlaceNumber(record.getPlaceNumber());
        ret.setHourOfDay(record.getHourOfDay());
        ret.setDayOfWeek(record.getDayOfWeek());
        ret.setMinutesDuration(record.getMinutesDuration());
        ret.setArmyAttacking(record.getArmyAttacking());
        ret.setRemainingArmyAttacks(record.getRemainingArmyAttacks());

        ret.setLife(record.getLife());
        ret.setFire(record.getFire());
        ret.setWood(record.getWood());
        ret.setWater(record.getWater());
        ret.setThunder(record.getThunder());
        ret.setAir(record.getAir());

        ret.setBonusFire(record.getBonusFire());
        ret.setBonusWood(record.getBonusWood());
        ret.setBonusWater(record.getBonusWater());
        ret.setBonusThunder(record.getBonusThunder());
        ret.setBonusAir(record.getBonusAir());
        ret.setBonusClaw(record.getBonusClaw());

        ret.setFightCount(record.getFightCount());
        ret.setFirstElement(record.getFirstElement());
        ret.setSecondElement(record.getSecondElement());
        ret.setThirdElement(record.getThirdElement());

        return ret;
    }
}
