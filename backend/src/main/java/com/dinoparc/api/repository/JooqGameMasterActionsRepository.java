package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.GameMasterAction;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.dinoparc.tables.GameMasterActions.GAME_MASTER_ACTIONS;

@Repository
public class JooqGameMasterActionsRepository implements PgGameMasterActionsRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqGameMasterActionsRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public GameMasterAction createGameMasterAction(GameMasterAction gameMasterAction) {
        return jooqContext.insertInto(GAME_MASTER_ACTIONS).set(jooqContext.newRecord(GAME_MASTER_ACTIONS, gameMasterAction)).returning().fetchOneInto(GameMasterAction.class);
    }
}
