package com.dinoparc.api.domain.dinoz;

import java.util.Collections;
import java.util.List;

public class ArmyDinozOverview {
    private List<ArmyDinozDto> armyDinozs;
    private Dinoz boss;

    public List<ArmyDinozDto> getArmyDinozs() {
        return armyDinozs;
    }

    public void setArmyDinozs(List<ArmyDinozDto> armyDinozs) {
        this.armyDinozs = armyDinozs;
    }

    public Dinoz getBoss() {
        return boss;
    }

    public void setBoss(Dinoz boss) {
        this.boss = boss;
        boss.setElementsValues(Collections.emptyMap());
    }
}
