package com.dinoparc.api.domain.clan;

public class OccupationData {

    public String location;
    public String faction;
    public String percentage;

    public OccupationData() {

    }

    public OccupationData(String location, String faction, String percentage) {
        this.location = location;
        this.faction = faction;
        this.percentage = percentage;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }
}
