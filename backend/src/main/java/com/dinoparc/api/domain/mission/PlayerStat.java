package com.dinoparc.api.domain.mission;

import java.util.UUID;

public class PlayerStat {
    UUID id;
    UUID playerId;
    String dinozId;
    long nbSkip;
    long nbFight;
    long nbWonFight;
    long nbLostFight;
    long nbShiny;
    long nbMove;
    long nbDig;
    long nbRaidFight;
    long nbWistitiFight;
    long nbDailyRecMission;
    long nbWeeklyRecMission;
    long nbMonthlyRecMission;
    long nbFusion;
    long sumFusion;
    long nbRaidFightWon;
    long sumGoldWon;
    long nbDinozBuy;
    long nbBazarSell;
    long nbBazarBuy;
    long nbBonsBuy;
    long nbBonsSell;
    long nbEggHatch;

    public PlayerStat() {
        this.id = UUID.randomUUID();
        this.nbSkip = 0;
        this.nbFight = 0;
        this.nbWonFight = 0;
        this.nbLostFight = 0;
        this.nbShiny = 0;
        this.nbMove = 0;
        this.nbDig = 0;
        this.nbRaidFight = 0;
        this.nbWistitiFight = 0;
        this.nbDailyRecMission = 0;
        this.nbWeeklyRecMission = 0;
        this.nbMonthlyRecMission = 0;
        this.nbFusion = 0;
        this.sumFusion = 0;
        this.nbRaidFightWon = 0;
        this.sumGoldWon = 0;
        this.nbDinozBuy = 0;
        this.nbBazarSell = 0;
        this.nbBazarBuy = 0;
        this.nbBonsBuy = 0;
        this.nbBonsSell = 0;
        this.nbEggHatch = 0;
    }

    public PlayerStat(UUID playerId, String dinozId) {
        this();
        this.playerId = playerId;
        this.dinozId = dinozId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public void setPlayerId(UUID playerId) {
        this.playerId = playerId;
    }

    public String getDinozId() {
        return dinozId;
    }

    public void setDinozId(String dinozId) {
        this.dinozId = dinozId;
    }

    public long getNbSkip() {
        return nbSkip;
    }

    public void setNbSkip(long nbSkip) {
        this.nbSkip = nbSkip;
    }

    public long getNbFight() {
        return nbFight;
    }

    public void setNbFight(long nbFight) {
        this.nbFight = nbFight;
    }

    public long getNbWonFight() {
        return nbWonFight;
    }

    public void setNbWonFight(long nbWonFight) {
        this.nbWonFight = nbWonFight;
    }

    public long getNbLostFight() {
        return nbLostFight;
    }

    public void setNbLostFight(long nbLostFight) {
        this.nbLostFight = nbLostFight;
    }

    public long getNbShiny() {
        return nbShiny;
    }

    public void setNbShiny(long nbShiny) {
        this.nbShiny = nbShiny;
    }

    public long getNbMove() {
        return nbMove;
    }

    public void setNbMove(long nbMove) {
        this.nbMove = nbMove;
    }

    public long getNbDig() {
        return nbDig;
    }

    public void setNbDig(long nbDig) {
        this.nbDig = nbDig;
    }

    public long getNbRaidFight() {
        return nbRaidFight;
    }

    public void setNbRaidFight(long nbRaidFight) {
        this.nbRaidFight = nbRaidFight;
    }

    public long getNbWistitiFight() {
        return nbWistitiFight;
    }

    public void setNbWistitiFight(long nbWistitiFight) {
        this.nbWistitiFight = nbWistitiFight;
    }

    public long getNbDailyRecMission() {
        return nbDailyRecMission;
    }

    public void setNbDailyRecMission(long nbDailyRecMission) {
        this.nbDailyRecMission = nbDailyRecMission;
    }

    public long getNbWeeklyRecMission() {
        return nbWeeklyRecMission;
    }

    public void setNbWeeklyRecMission(long nbWeeklyRecMission) {
        this.nbWeeklyRecMission = nbWeeklyRecMission;
    }

    public long getNbMonthlyRecMission() {
        return nbMonthlyRecMission;
    }

    public void setNbMonthlyRecMission(long nbMonthlyRecMission) {
        this.nbMonthlyRecMission = nbMonthlyRecMission;
    }

    public long getNbFusion() {
        return nbFusion;
    }

    public void setNbFusion(long nbFusion) {
        this.nbFusion = nbFusion;
    }

    public long getSumFusion() {
        return sumFusion;
    }

    public void setSumFusion(long sumFusion) {
        this.sumFusion = sumFusion;
    }

    public long getNbRaidFightWon() {
        return nbRaidFightWon;
    }

    public void setNbRaidFightWon(long nbRaidFightWon) {
        this.nbRaidFightWon = nbRaidFightWon;
    }

    public long getSumGoldWon() {
        return sumGoldWon;
    }

    public void setSumGoldWon(long sumGoldWon) {
        this.sumGoldWon = sumGoldWon;
    }

    public long getNbDinozBuy() {
        return nbDinozBuy;
    }

    public void setNbDinozBuy(long nbDinozBuy) {
        this.nbDinozBuy = nbDinozBuy;
    }

    public long getNbBazarSell() {
        return nbBazarSell;
    }

    public void setNbBazarSell(long nbBazarSell) {
        this.nbBazarSell = nbBazarSell;
    }

    public long getNbBazarBuy() {
        return nbBazarBuy;
    }

    public void setNbBazarBuy(long nbBazarBuy) {
        this.nbBazarBuy = nbBazarBuy;
    }

    public long getNbBonsBuy() {
        return nbBonsBuy;
    }

    public void setNbBonsBuy(long nbBonsBuy) {
        this.nbBonsBuy = nbBonsBuy;
    }

    public long getNbBonsSell() {
        return nbBonsSell;
    }

    public void setNbBonsSell(long nbBonsSell) {
        this.nbBonsSell = nbBonsSell;
    }

    public long getNbEggHatch() {
        return nbEggHatch;
    }

    public void setNbEggHatch(long nbEggHatch) {
        this.nbEggHatch = nbEggHatch;
    }
}
