package com.dinoparc.api.domain.dinoz;

import java.util.UUID;

public class ArmyDinoz {
    private String armyDinozId;
    private String dinozId;
    private UUID masterId;
    private UUID bossId;
    private int rank;
    private int nbAttacks;
    private int maxNbAttacks;
    private int bossLifeLost;

    public String getArmyDinozId() {
        return armyDinozId;
    }

    public void setArmyDinozId(String armyDinozId) {
        this.armyDinozId = armyDinozId;
    }

    public String getDinozId() {
        return dinozId;
    }

    public void setDinozId(String dinozId) {
        this.dinozId = dinozId;
    }

    public UUID getMasterId() {
        return masterId;
    }

    public void setMasterId(UUID masterId) {
        this.masterId = masterId;
    }

    public UUID getBossId() {
        return bossId;
    }

    public void setBossId(UUID bossId) {
        this.bossId = bossId;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getNbAttacks() {
        return nbAttacks;
    }

    public void setNbAttacks(int nbAttacks) {
        this.nbAttacks = nbAttacks;
    }

    public int getMaxNbAttacks() {
        return maxNbAttacks;
    }

    public void setMaxNbAttacks(int maxNbAttacks) {
        this.maxNbAttacks = maxNbAttacks;
    }

    public int getBossLifeLost() {
        return bossLifeLost;
    }

    public void setBossLifeLost(int bossLifeLost) {
        this.bossLifeLost = bossLifeLost;
    }
}
