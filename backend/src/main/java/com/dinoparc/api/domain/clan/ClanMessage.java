package com.dinoparc.api.domain.clan;

import java.util.ArrayDeque;

public class ClanMessage {

    private String clanId;
    private ArrayDeque<ForumMessage> messages;

    public ClanMessage() {
        this.messages = new ArrayDeque<ForumMessage>();
    }

    public String getClanId() {
        return clanId;
    }

    public void setClanId(String clanId) {
        this.clanId = clanId;
    }

    public ArrayDeque<ForumMessage> getMessages() {
        return messages;
    }

    public void setMessages(ArrayDeque<ForumMessage> messages) {
        this.messages = messages;
    }
}
