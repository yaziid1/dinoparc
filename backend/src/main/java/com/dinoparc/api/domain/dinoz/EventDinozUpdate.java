package com.dinoparc.api.domain.dinoz;

import com.dinoparc.api.domain.account.DinoparcConstants;

public class EventDinozUpdate {
    Integer life = 0;
    Integer bonusFire = 0;
    Integer bonusWood = 0;
    Integer bonusWater = 0;
    Integer bonusThunder = 0;
    Integer bonusAir = 0;
    Integer bonusClaw = 0;

    public Integer getLife() {
        return life;
    }

    public Integer getBonusFire() {
        return bonusFire;
    }

    public Integer getBonusWood() {
        return bonusWood;
    }

    public Integer getBonusWater() {
        return bonusWater;
    }

    public Integer getBonusThunder() {
        return bonusThunder;
    }

    public Integer getBonusAir() {
        return bonusAir;
    }

    public Integer getBonusClaw() {
        return bonusClaw;
    }

    public void setLife(Integer life) {
        this.life = life;
    }

    public void setBonusFire(Integer bonusFire) {
        this.bonusFire = bonusFire;
    }

    public void setBonusWood(Integer bonusWood) {
        this.bonusWood = bonusWood;
    }

    public void setBonusWater(Integer bonusWater) {
        this.bonusWater = bonusWater;
    }

    public void setBonusThunder(Integer bonusThunder) {
        this.bonusThunder = bonusThunder;
    }

    public void setBonusAir(Integer bonusAir) {
        this.bonusAir = bonusAir;
    }

    public void setBonusClaw(Integer bonusClaw) {
        this.bonusClaw = bonusClaw;
    }

    public void addLostCharm(String element) {
        if (DinoparcConstants.FEU.equals(element)) {
            this.bonusFire++;
            return;
        }
        if (DinoparcConstants.TERRE.equals(element)) {
            this.bonusWood++;
            return;
        }
        if (DinoparcConstants.EAU.equals(element)) {
            this.bonusWater++;
            return;
        }
        if (DinoparcConstants.FOUDRE.equals(element)) {
            this.bonusThunder++;
            return;
        }
        bonusAir++;
    }
}
