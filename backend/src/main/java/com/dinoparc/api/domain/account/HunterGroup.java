package com.dinoparc.api.domain.account;

import java.util.ArrayList;
import java.util.List;

public class HunterGroup {

    private List<String> accountIds;
    private Integer hour;

    public HunterGroup() {
        this.accountIds = new ArrayList<String>();
    }

    public List<String> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<String> accountIds) {
        this.accountIds = accountIds;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }
}
