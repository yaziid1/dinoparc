package com.dinoparc.api.domain.bazar;

import com.dinoparc.api.domain.dinoz.Dinoz;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BazarListing implements Comparable<BazarListing> {
    private String id;
    private boolean isActive;

    private String dinozId;
    private Map<String, Integer> objects;
    private String sellerId;
    private String sellerName;
    private Integer initialDurationInDays;
    private Long endDateInEpochSeconds;

    private String lastBiderId;
    private String lastBiderName;
    private Integer lastPrice;

    public BazarListing() {
        this.id = UUID.randomUUID().toString();
        this.objects = new HashMap<String, Integer>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getDinozId() {
        return dinozId;
    }

    public void setDinozId(String dinozId) {
        this.dinozId = dinozId;
    }

    public Map<String, Integer> getObjects() {
        return objects;
    }

    public void setObjects(Map<String, Integer> objects) {
        this.objects = objects;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Integer getInitialDurationInDays() {
        return initialDurationInDays;
    }

    public void setInitialDurationInDays(Integer initialDurationInDays) {
        this.initialDurationInDays = initialDurationInDays;
    }

    public String getLastBiderId() {
        return lastBiderId;
    }

    public void setLastBiderId(String lastBiderId) {
        this.lastBiderId = lastBiderId;
    }

    public String getLastBiderName() {
        return lastBiderName;
    }

    public void setLastBiderName(String lastBiderName) {
        this.lastBiderName = lastBiderName;
    }

    public Integer getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(Integer lastPrice) {
        this.lastPrice = lastPrice;
    }

    public Long getEndDateInEpochSeconds() {
        return endDateInEpochSeconds;
    }

    public void setEndDateInEpochSeconds(Long endDateInEpochSeconds) {
        this.endDateInEpochSeconds = endDateInEpochSeconds;
    }

    @Override
    public int compareTo(@NotNull BazarListing other) {
        if (this.getEndDateInEpochSeconds() < other.getEndDateInEpochSeconds()) {
            return -1;
        }

        if (this.getEndDateInEpochSeconds() > other.getEndDateInEpochSeconds()) {
            return 1;
        }

        else {
            return 0;
        }
    }
}
