package com.dinoparc.api.domain.dinoz;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.RandomCollection;
import com.dinoparc.api.services.AccountService;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class RestrictedDinozDto {
    private String id;
    private int life;
    private int level;
    private int experience;
    private String name;
    private Map<String, Boolean> actionsMap;
    private int placeNumber;
    private String appearanceCode;
    private String race;

    public RestrictedDinozDto() {
        actionsMap = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(int placeNumber) {
        this.placeNumber = placeNumber;
    }

    public String getAppearanceCode() {
        return appearanceCode;
    }

    public void setAppearanceCode(String appearanceCode) {
        this.appearanceCode = appearanceCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Map<String, Boolean> getActionsMap() {
        return actionsMap;
    }

    public void setActionsMap(Map<String, Boolean> actionsMap) {
        this.actionsMap = actionsMap;
    }
}