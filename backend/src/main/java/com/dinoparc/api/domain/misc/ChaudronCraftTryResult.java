package com.dinoparc.api.domain.misc;

import com.dinoparc.api.domain.account.DinoparcConstants;

public class ChaudronCraftTryResult {

    private boolean success;
    private String generatedObject;
    private Integer generatedObjectQty;

    public ChaudronCraftTryResult() {
        //Default fallback :
        this.success = false;
        this.generatedObject = DinoparcConstants.POTION_IRMA;
        this.generatedObjectQty = 0;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getGeneratedObject() {
        return generatedObject;
    }

    public void setGeneratedObject(String generatedObject) {
        this.generatedObject = generatedObject;
    }

    public Integer getGeneratedObjectQty() {
        return generatedObjectQty;
    }

    public void setGeneratedObjectQty(Integer generatedObjectQty) {
        this.generatedObjectQty = generatedObjectQty;
    }
}
