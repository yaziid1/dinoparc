package com.dinoparc.api.domain.account;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class History {

    private String id;
    private String playerId;
    private boolean seen;
    private String icon;
    private String type;
    private String numericalDate;

    private boolean hasWon;
    private String fromUserName;
    private String fromDinozName;
    private String toUserName;
    private String toDinozName;
    private Integer expGained;
    private Integer lifeLost;

    private Integer buyAmount;
    private String object;

    private int rank;

    public History() {
        this.seen = false;
        this.rank = 0;
    }

    public History(String fromUserName, String fromDinozName, String toUserName,
                   String toDinozName, Integer expGained, Integer lifeLost, String numericalDate,
                   boolean hasWon) {
        this.seen = false;
        this.fromUserName = fromUserName;
        this.fromDinozName = fromDinozName;
        this.toUserName = toUserName;
        this.toDinozName = toDinozName;
        this.expGained = expGained;
        this.lifeLost = lifeLost;
        this.numericalDate = numericalDate;
        this.hasWon = hasWon;
        this.rank = 0;
    }

    public void setId(UUID id) {
        this.id = id.toString();
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getFromDinozName() {
        return fromDinozName;
    }

    public void setFromDinozName(String fromDinozName) {
        this.fromDinozName = fromDinozName;
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getToDinozName() {
        return toDinozName;
    }

    public void setToDinozName(String toDinozName) {
        this.toDinozName = toDinozName;
    }

    public Integer getExpGained() {
        return expGained;
    }

    public void setExpGained(Integer expGained) {
        this.expGained = expGained;
    }

    public Integer getLifeLost() {
        return lifeLost;
    }

    public void setLifeLost(Integer lifeLost) {
        this.lifeLost = lifeLost;
    }

    public String getNumericalDate() {
        return numericalDate;
    }

    public boolean isHasWon() {
        return hasWon;
    }

    public void setHasWon(boolean hasWon) {
        this.hasWon = hasWon;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getBuyAmount() {
        return buyAmount;
    }

    public void setBuyAmount(Integer buyAmount) {
        this.buyAmount = buyAmount;
    }

    public void setNumericalDate(OffsetDateTime numericalDate) {
        StringBuilder sb = new StringBuilder(numericalDate.atZoneSameInstant(ZoneId.of("Europe/Paris")).format(DateTimeFormatter.ISO_LOCAL_DATE));
        sb.append(" ");
        sb.append(numericalDate.format(DateTimeFormatter.ISO_LOCAL_TIME).substring(0, 8));
        this.numericalDate = sb.toString();
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
