package com.dinoparc.api.domain.dinoz;

public class ThreeMainElements {

  private String one;
  private String two;
  private String three;
  
  public ThreeMainElements() {
    
  }

  public String getOne() {
    return one;
  }

  public void setOne(String one) {
    this.one = one;
  }

  public String getTwo() {
    return two;
  }

  public void setTwo(String two) {
    this.two = two;
  }

  public String getThree() {
    return three;
  }

  public void setThree(String three) {
    this.three = three;
  }
  
}
