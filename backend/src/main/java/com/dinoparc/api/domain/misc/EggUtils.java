package com.dinoparc.api.domain.misc;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.RandomCollection;

public class EggUtils {
    public static String getDinozRaceFromEggType(String eggType) {
        if (eggType.equals(DinoparcConstants.OEUF_COBALT)) {
            RandomCollection<String> randomElement = new RandomCollection<String>()
                    .add(25, DinoparcConstants.GOUPIGNON)
                    .add(30, DinoparcConstants.SANTAZ)
                    .add(35, DinoparcConstants.SERPANTIN)
                    .add(10, DinoparcConstants.FEROSS);
            return randomElement.next();
        } else {
            return switch (eggType) {
                case "EGG_11" -> DinoparcConstants.GOUPIGNON;
                case "EGG_14" -> DinoparcConstants.SANTAZ;
                case "EGG_18" -> DinoparcConstants.SERPANTIN;
                case "EGG_20" -> DinoparcConstants.FEROSS;
                default -> null;
            };
        }
    }

    public static String getDinozRaceCharCodeFromEggType(String race) {
        return switch (race) {
            case DinoparcConstants.GOUPIGNON -> "B";
            case DinoparcConstants.SANTAZ -> "E";
            case DinoparcConstants.SERPANTIN -> "I";
            case DinoparcConstants.FEROSS -> "K";
            default -> null;
        };
    }
}