package com.dinoparc.api.domain.account;

import java.time.OffsetDateTime;

public class ClientInformation {
    private String username;
    private byte[] first;
    private byte[] second;
    private byte[] third;
    private byte[] fourth;
    private OffsetDateTime retrievedDate;
    private String retrievedDateTxt;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getFirst() {
        return first;
    }

    public void setFirst(byte[] first) {
        this.first = first;
    }

    public byte[] getSecond() {
        return second;
    }

    public void setSecond(byte[] second) {
        this.second = second;
    }

    public byte[] getThird() {
        return third;
    }

    public void setThird(byte[] third) {
        this.third = third;
    }

    public byte[] getFourth() {
        return fourth;
    }

    public void setFourth(byte[] fourth) {
        this.fourth = fourth;
    }

    public OffsetDateTime getRetrievedDate() {
        return retrievedDate;
    }

    public void setRetrievedDate(OffsetDateTime retrievedDate) {
        this.retrievedDate = retrievedDate;
    }

    public String getRetrievedDateTxt() {
        return retrievedDateTxt;
    }

    public void setRetrievedDateTxt(String retrievedDateTxt) {
        this.retrievedDateTxt = retrievedDateTxt;
    }
}
