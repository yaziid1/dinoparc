package com.dinoparc.api.domain.mission;

import java.util.UUID;

public class PlayerMission {
    UUID id;
    UUID playerId;
    String dinozId;
    String dinozName;
    UUID recMissionId;
    boolean daily;
    boolean weekly;
    boolean monthly;

    UUID rewardId;
    int difficulty;
    int rarity;
    String counterName;
    long counterInitialValue;
    long counterCurrentValue;
    long counterTargetValue;
    Long beginDate;
    Long expirationDate;
    boolean rewardClaimed;
    Reward reward;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public void setPlayerId(UUID playerId) {
        this.playerId = playerId;
    }

    public String getDinozId() {
        return dinozId;
    }

    public void setDinozId(String dinozId) {
        this.dinozId = dinozId;
    }

    public UUID getRecMissionId() {
        return recMissionId;
    }

    public void setRecMissionId(UUID recMissionId) {
        this.recMissionId = recMissionId;
    }

    public boolean isDaily() {
        return daily;
    }

    public void setDaily(boolean daily) {
        this.daily = daily;
    }

    public boolean isWeekly() {
        return weekly;
    }

    public void setWeekly(boolean weekly) {
        this.weekly = weekly;
    }

    public boolean isMonthly() {
        return monthly;
    }

    public void setMonthly(boolean monthly) {
        this.monthly = monthly;
    }

    public UUID getRewardId() {
        return rewardId;
    }

    public void setRewardId(UUID rewardId) {
        this.rewardId = rewardId;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getRarity() {
        return rarity;
    }

    public void setRarity(int rarity) {
        this.rarity = rarity;
    }

    public String getCounterName() {
        return counterName;
    }

    public void setCounterName(String counterName) {
        this.counterName = counterName;
    }

    public long getCounterInitialValue() {
        return counterInitialValue;
    }

    public void setCounterInitialValue(long counterInitialValue) {
        this.counterInitialValue = counterInitialValue;
    }

    public long getCounterCurrentValue() {
        return counterCurrentValue;
    }

    public void setCounterCurrentValue(long counterCurrentValue) {
        this.counterCurrentValue = counterCurrentValue;
    }

    public long getCounterTargetValue() {
        return counterTargetValue;
    }

    public void setCounterTargetValue(long counterTargetValue) {
        this.counterTargetValue = counterTargetValue;
    }

    public Long getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Long beginDate) {
        this.beginDate = beginDate;
    }

    public Long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isRewardClaimed() {
        return rewardClaimed;
    }

    public void setRewardClaimed(boolean rewardClaimed) {
        this.rewardClaimed = rewardClaimed;
    }

    public String getDinozName() {
        return dinozName;
    }

    public void setDinozName(String dinozName) {
        this.dinozName = dinozName;
    }

    public Reward getReward() {
        return reward;
    }

    public void setReward(Reward reward) {
        this.reward = reward;
    }
}
