package com.dinoparc.api.domain.account;

import java.time.ZonedDateTime;
import java.util.UUID;

public class News implements Comparable<News> {
  private String id;
  private String image;
  private Long timestamp;

  private String titleFr;
  private String dateFr;
  private String textFr;

  private String titleEn;
  private String dateEn;
  private String textEn;

  private String titleEs;
  private String dateEs;
  private String textEs;

  private String titleCh;
  private String dateCh;
  private String textCh;

  public News() {
    this.id = UUID.randomUUID().toString();
    this.timestamp = ZonedDateTime.now().toEpochSecond();
  }

  public News(String image, String titleFr, String dateFr, String textFr, String titleEn,
      String dateEn, String textEn, String titleEs, String dateEs, String textEs, String titleCh,
      String dateCh, String textCh) {
    this.id = UUID.randomUUID().toString();
    this.image = image;
    this.titleFr = titleFr;
    this.dateFr = dateFr;
    this.textFr = textFr;
    this.titleEn = titleEn;
    this.dateEn = dateEn;
    this.textEn = textEn;
    this.titleEs = titleEs;
    this.dateEs = dateEs;
    this.textEs = textEs;
    this.titleCh = titleCh;
    this.dateCh = dateCh;
    this.textCh = textCh;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getTitleFr() {
    return titleFr;
  }

  public void setTitleFr(String titleFr) {
    this.titleFr = titleFr;
  }

  public String getDateFr() {
    return dateFr;
  }

  public void setDateFr(String dateFr) {
    this.dateFr = dateFr;
  }

  public String getTextFr() {
    return textFr;
  }

  public void setTextFr(String textFr) {
    this.textFr = textFr;
  }

  public String getTitleEn() {
    return titleEn;
  }

  public void setTitleEn(String titleEn) {
    this.titleEn = titleEn;
  }

  public String getDateEn() {
    return dateEn;
  }

  public void setDateEn(String dateEn) {
    this.dateEn = dateEn;
  }

  public String getTextEn() {
    return textEn;
  }

  public void setTextEn(String textEn) {
    this.textEn = textEn;
  }

  public String getTitleEs() {
    return titleEs;
  }

  public void setTitleEs(String titleEs) {
    this.titleEs = titleEs;
  }

  public String getDateEs() {
    return dateEs;
  }

  public void setDateEs(String dateEs) {
    this.dateEs = dateEs;
  }

  public String getTextEs() {
    return textEs;
  }

  public void setTextEs(String textEs) {
    this.textEs = textEs;
  }

  public String getTitleCh() {
    return titleCh;
  }

  public void setTitleCh(String titleCh) {
    this.titleCh = titleCh;
  }

  public String getDateCh() {
    return dateCh;
  }

  public void setDateCh(String dateCh) {
    this.dateCh = dateCh;
  }

  public String getTextCh() {
    return textCh;
  }

  public void setTextCh(String textCh) {
    this.textCh = textCh;
  }

  public Long getTimestamp() {
    return timestamp;
  }

  @Override
  public int compareTo(News news) {
    return (int) (news.getTimestamp() - this.getTimestamp());
  }

}
