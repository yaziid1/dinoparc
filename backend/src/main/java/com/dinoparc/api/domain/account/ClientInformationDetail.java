package com.dinoparc.api.domain.account;

import java.time.OffsetDateTime;

public class ClientInformationDetail {
    private String accountId;
    private String dinozId;
    private String first;
    private String second;
    private String detailType;
    private OffsetDateTime retrievedDate;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDinozId() {
        return dinozId;
    }

    public void setDinozId(String dinozId) {
        this.dinozId = dinozId;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getDetailType() {
        return detailType;
    }

    public void setDetailType(String detailType) {
        this.detailType = detailType;
    }

    public OffsetDateTime getRetrievedDate() {
        return retrievedDate;
    }

    public void setRetrievedDate(OffsetDateTime retrievedDate) {
        this.retrievedDate = retrievedDate;
    }
}
