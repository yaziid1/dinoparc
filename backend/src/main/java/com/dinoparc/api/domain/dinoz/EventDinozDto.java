package com.dinoparc.api.domain.dinoz;

import com.dinoparc.api.domain.account.DinoparcConstants;

import java.util.HashMap;
import java.util.Map;

public class EventDinozDto {
    String bossId;
    String appearanceCode;
    String name;
    Integer placeNumber;
    Integer hourOfDay;
    Integer dayOfWeek;
    Integer minutesDuration;
    Integer remainingArmyAttacks;
    Boolean armyAttacking;

    Integer life;
    Integer fire;
    Integer wood;
    Integer water;
    Integer thunder;
    Integer air;
    Integer bonusFire;
    Integer bonusWood;
    Integer bonusWater;
    Integer bonusThunder;
    Integer bonusAir;
    Integer bonusClaw;
    Integer fightCount;

    String firstElement;
    String secondElement;
    String thirdElement;

    public String getBossId() {
        return bossId;
    }

    public void setBossId(String bossId) {
        this.bossId = bossId;
    }

    public String getAppearanceCode() {
        return appearanceCode;
    }

    public void setAppearanceCode(String appearanceCode) {
        this.appearanceCode = appearanceCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(Integer placeNumber) {
        this.placeNumber = placeNumber;
    }

    public Integer getHourOfDay() {
        return hourOfDay;
    }

    public void setHourOfDay(Integer hourOfDay) {
        this.hourOfDay = hourOfDay;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Integer getMinutesDuration() {
        return minutesDuration;
    }

    public void setMinutesDuration(Integer minutesDuration) {
        this.minutesDuration = minutesDuration;
    }

    public Integer getRemainingArmyAttacks() {
        return remainingArmyAttacks;
    }

    public void setRemainingArmyAttacks(Integer remainingArmyAttacks) {
        this.remainingArmyAttacks = remainingArmyAttacks;
    }

    public Boolean getArmyAttacking() {
        return armyAttacking;
    }

    public void setArmyAttacking(Boolean armyAttacking) {
        this.armyAttacking = armyAttacking;
    }

    public Integer getLife() {
        return life;
    }

    public void setLife(Integer life) {
        this.life = life;
    }

    public Integer getFire() {
        return fire;
    }

    public void setFire(Integer fire) {
        this.fire = fire;
    }

    public Integer getWood() {
        return wood;
    }

    public void setWood(Integer wood) {
        this.wood = wood;
    }

    public Integer getWater() {
        return water;
    }

    public void setWater(Integer water) {
        this.water = water;
    }

    public Integer getThunder() {
        return thunder;
    }

    public void setThunder(Integer thunder) {
        this.thunder = thunder;
    }

    public Integer getAir() {
        return air;
    }

    public void setAir(Integer air) {
        this.air = air;
    }

    public Integer getBonusFire() {
        return bonusFire;
    }

    public void setBonusFire(Integer bonusFire) {
        this.bonusFire = bonusFire;
    }

    public Integer getBonusWood() {
        return bonusWood;
    }

    public void setBonusWood(Integer bonusWood) {
        this.bonusWood = bonusWood;
    }

    public Integer getBonusWater() {
        return bonusWater;
    }

    public void setBonusWater(Integer bonusWater) {
        this.bonusWater = bonusWater;
    }

    public Integer getBonusThunder() {
        return bonusThunder;
    }

    public void setBonusThunder(Integer bonusThunder) {
        this.bonusThunder = bonusThunder;
    }

    public Integer getBonusAir() {
        return bonusAir;
    }

    public void setBonusAir(Integer bonusAir) {
        this.bonusAir = bonusAir;
    }

    public Integer getBonusClaw() {
        return bonusClaw;
    }

    public void setBonusClaw(Integer bonusClaw) {
        this.bonusClaw = bonusClaw;
    }

    public Integer getFightCount() {
        return fightCount;
    }

    public void setFightCount(Integer fightCount) {
        this.fightCount = fightCount;
    }

    public String getFirstElement() {
        return firstElement;
    }

    public void setFirstElement(String firstElement) {
        this.firstElement = firstElement;
    }

    public String getSecondElement() {
        return secondElement;
    }

    public void setSecondElement(String secondElement) {
        this.secondElement = secondElement;
    }

    public String getThirdElement() {
        return thirdElement;
    }

    public void setThirdElement(String thirdElement) {
        this.thirdElement = thirdElement;
    }

    public Map<String, Integer> getElementsValues() {
        var ret = new HashMap<String, Integer>();
        ret.put(DinoparcConstants.FEU, this.fire);
        ret.put(DinoparcConstants.TERRE, this.wood);
        ret.put(DinoparcConstants.EAU, this.water);
        ret.put(DinoparcConstants.FOUDRE, this.thunder);
        ret.put(DinoparcConstants.AIR, this.air);
        return ret;
    }

    public boolean hasCharm(String element) {
        if (DinoparcConstants.FEU.equals(element)) {
            return bonusFire > 0;
        }
        if (DinoparcConstants.TERRE.equals(element)) {
            return bonusWood > 0;
        }
        if (DinoparcConstants.EAU.equals(element)) {
            return bonusWater > 0;
        }
        if (DinoparcConstants.FOUDRE.equals(element)) {
            return bonusThunder > 0;
        }
        return bonusAir > 0;
    }
}
