package com.dinoparc.api.configuration;

import java.net.URI;
import java.net.URISyntaxException;

import com.dinoparc.api.domain.account.DinoparcConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import net.eternaltwin.client.HttpEtwinClient;
import net.eternaltwin.oauth.client.RfcOauthClient;

@EnableWebSecurity
public class WebSecurityConfig {

  @Configuration
  public static class ApiConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${etwin.uri}")
    public String etwinUri;

    @Value("${server.backend}")
    public String backendUri;

    @Value("${etwin.client-id}")
    public String clientId;

    @Value("${etwin.client-secret}")
    public String clientSecret;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http.cors().and().csrf().disable().sessionManagement()
              .and().authorizeRequests()
              .antMatchers(DinoparcConstants.UnrestrictedPathsArray).permitAll()
              .anyRequest().authenticated()
              .and()
              .addFilter(new JWTAuthorizationFilter(authenticationManager()))
              .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    public RfcOauthClient getOauthClient() throws URISyntaxException {
      URI authUri = new URI(joinUri(etwinUri, "oauth/authorize"));
      URI tokenUri = new URI(joinUri(etwinUri, "oauth/token"));
      URI callBackUri = new URI(joinUri(backendUri, "api/account/callback"));

      return new RfcOauthClient(authUri, tokenUri, callBackUri, clientId, clientSecret);
    }

    @Bean
    public HttpEtwinClient getEtwinClient() throws URISyntaxException {

      return new HttpEtwinClient(new URI(etwinUri));
    }

    public static String joinUri(String left, String right) {
      if (left.endsWith("/")) {
        return left + right;
      } else {
        return left + "/" + right;
      }
    }
  }
}
