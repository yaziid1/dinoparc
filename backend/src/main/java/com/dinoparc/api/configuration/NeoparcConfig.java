package com.dinoparc.api.configuration;

import com.dinoparc.api.rand.RngFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.charset.StandardCharsets;

@Configuration
public class NeoparcConfig {
  @Value("${server.seed}")
  private String seed;

  @Bean
  public RngFactory getRngFactory() {
    return RngFactory.fromSeed(this.seed.getBytes(StandardCharsets.UTF_8));
  }
}
