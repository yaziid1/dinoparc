package com.dinoparc.api.services.migration;

import net.eternaltwin.dinoparc.DinoparcServer;
import net.eternaltwin.user.UserId;

/**
 * The user already imported from the requested server
 */
public final class AlreadyImportedServerException extends ImportCheckException {
  public final UserId userId;
  public final DinoparcServer server;

  public AlreadyImportedServerException(final UserId userId, final DinoparcServer server) {
    this.userId = userId;
    this.server = server;
  }
}
