package com.dinoparc.api.services;

import com.dinoparc.api.configuration.NeoparcConfig;
import com.dinoparc.api.controllers.dto.HermitStageDto;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.misc.HermitConstants;
import com.dinoparc.api.rand.Rng;
import com.dinoparc.api.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ArenaService {
    private final List<Integer> specialStages = List.of(10, 25, 50, 100, 250, 500);
    private final PgPlayerRepository playerRepository;
    private final PgDinozRepository dinozRepository;
    private final PgCollectionRepository collectionRepository;
    private final PgInventoryRepository inventoryRepository;
    private final NeoparcConfig neoparcConfig;

    @Autowired
    public ArenaService(PgPlayerRepository playerRepository, PgDinozRepository dinozRepository,
                        PgCollectionRepository collectionRepository, PgInventoryRepository inventoryRepository, NeoparcConfig neoparcConfig) {
        this.playerRepository = playerRepository;
        this.dinozRepository = dinozRepository;
        this.collectionRepository = collectionRepository;
        this.inventoryRepository = inventoryRepository;
        this.neoparcConfig = neoparcConfig;
    }

    public HermitStageDto getActualStage(Player account, Dinoz actualDinoz) {
        initDataIfNew(account);
        HermitStageDto stageDto = new HermitStageDto();

        Rng rng = neoparcConfig.getRngFactory()
                .string("getActualStage")
                .string(String.valueOf(account.getHermitStage()))
                .string(account.getId())
                .toRng();

        stageDto.setActualStage(account.getHermitStage());
        stageDto.setActualWins(account.getHermitStageCurrentWins());
        stageDto.setActualFighterId(account.getHermitStageFightingDinoz());
        stageDto.setEnnemyLevel(getEnnemyLevelByStage(stageDto.getActualStage()));
        stageDto.setNeededWins(getNeededWinsByStage(stageDto.getEnnemyLevel()));
        stageDto.setAppearanceCode(getRandomRace(rng) + getRandomHexString(rng));

        if (account.getHermitStageFightingDinoz() != null) {
            Optional<Dinoz> fightingMaybe = dinozRepository.findById(account.getHermitStageFightingDinoz());
            if (fightingMaybe.isPresent()) {
                stageDto.setActualFighterName(fightingMaybe.get().getName());

            } else {
                account.setHermitStageFightingDinoz(null);
                playerRepository.updateHermitStageFightingDinoz(account.getId(), null);
            }
        }

        Integer partOne = rng.randIntBetween(0, 21);
        Integer partTwo = rng.randIntBetween(0, 21);
        Integer partThree = rng.randIntBetween(0, 21);

        stageDto.setEnnemyNameFr(
                HermitConstants.names.get(partOne) + ", "
                        + HermitConstants.ADJ1_FR.get(partTwo) + " "
                        + HermitConstants.ADJ2_FR.get(partThree) + "."
        );

        stageDto.setEnnemyNameEs(
                HermitConstants.names.get(partOne) + ", "
                        + HermitConstants.ADJ1_ES.get(partTwo) + " "
                        + HermitConstants.ADJ2_ES.get(partThree) + "."
        );

        stageDto.setEnnemyNameEn(
                HermitConstants.names.get(partOne) + ", "
                        + HermitConstants.ADJ1_EN.get(partTwo) + " "
                        + HermitConstants.ADJ2_EN.get(partThree) + "."
        );

        ArrayList<String> elements = new ArrayList<String>();
        elements.add(DinoparcConstants.FEU);
        elements.add(DinoparcConstants.TERRE);
        elements.add(DinoparcConstants.EAU);
        elements.add(DinoparcConstants.FOUDRE);
        elements.add(DinoparcConstants.AIR);
        rng.shuffleInPlace(elements);

        Integer firstElementValue = Math.floorDiv(stageDto.getEnnemyLevel(), 3) + 5;
        Integer secondElementValue = Math.floorDiv(stageDto.getEnnemyLevel(), 3) + 2;
        Integer thirdElementValue = Math.floorDiv(stageDto.getEnnemyLevel(), 4);
        Integer fourthElementValue = 0;
        Integer fifthElementValue = 0;

        stageDto.setElementMajeur(elements.get(0));
        stageDto.getElementsValues().put(elements.get(0), firstElementValue);
        stageDto.getElementsValues().put(elements.get(1), secondElementValue);
        stageDto.getElementsValues().put(elements.get(2), thirdElementValue);
        stageDto.getElementsValues().put(elements.get(3), fourthElementValue);
        stageDto.getElementsValues().put(elements.get(4), fifthElementValue);

        if (account.getHermitStageFightingDinoz() != null && !account.getHermitStageFightingDinoz().equalsIgnoreCase(actualDinoz.getId())){
            stageDto.setFighterIsValid(false);
        }

        if (Math.abs(stageDto.getEnnemyLevel() - actualDinoz.getLevel()) > getMaximumLevelGapForStage(stageDto.getActualStage())) {
            stageDto.setLevelGapIsValid(false);
        }

        checkForRewards(account, stageDto);
        return stageDto;
    }

    private int getMaximumLevelGapForStage(Integer actualStage) {
        if (actualStage <= 250) {
            return 10;
        }
        return 30;
    }

    public void validateRewards(Player account, Dinoz dinoz) {
        HermitStageDto data = getActualStage(account, dinoz);
        checkForRewards(account, data);
        for (String rewardKey : data.getRewards().keySet()) {
            //Reward is gold :
            if (rewardKey.equalsIgnoreCase(DinoparcConstants.COINS)) {
                playerRepository.updateCash(account.getId(), data.getRewards().get(rewardKey));
            }
            //Reward is epic object :
            else if (rewardKey.startsWith("globe_")) {
                var collection = collectionRepository.getPlayerCollection(UUID.fromString(account.getId()));
                var epicCollection = collection.getEpicCollection();
                epicCollection.add(getGlobeEpicNumber(rewardKey));
                collectionRepository.updateEpicCollection(collection.getId(), epicCollection);
            }

            //Reward is consumable :
            else {
                inventoryRepository.addInventoryItem(account.getId(), rewardKey, data.getRewards().get(rewardKey));
            }
        }

        if (account.getHermitStageCurrentWins() >= getNeededWinsByStage(data.getEnnemyLevel())) {
            playerRepository.updateHermitStage(account.getId(), data.getActualStage() + 1);
            playerRepository.updateHermitStageCurrentWins(account.getId(), 0);
        }
    }

    public Dinoz rebuildArenaBot(Player account, Dinoz dinoz) {
        HermitStageDto data = getActualStage(account, dinoz);
        Dinoz arenaFighter = new Dinoz();
        arenaFighter.setAppearanceCode(data.getAppearanceCode());
        arenaFighter.getElementsValues().put(DinoparcConstants.FEU, data.getElementsValues().get(DinoparcConstants.FEU));
        arenaFighter.getElementsValues().put(DinoparcConstants.TERRE, data.getElementsValues().get(DinoparcConstants.TERRE));
        arenaFighter.getElementsValues().put(DinoparcConstants.EAU, data.getElementsValues().get(DinoparcConstants.EAU));
        arenaFighter.getElementsValues().put(DinoparcConstants.FOUDRE, data.getElementsValues().get(DinoparcConstants.FOUDRE));
        arenaFighter.getElementsValues().put(DinoparcConstants.AIR, data.getElementsValues().get(DinoparcConstants.AIR));
        arenaFighter.setBeginMessage(DinoparcConstants.BEGINMESSAGEBOT);
        arenaFighter.setEndMessage(DinoparcConstants.ENDMESSAGEBOT);
        arenaFighter.setLife(100);
        arenaFighter.setPlaceNumber(5);
        arenaFighter.setMasterName(DinoparcConstants.HERMIT);
        arenaFighter.setLevel(data.getEnnemyLevel());
        arenaFighter.setName(data.getEnnemyNameFr() + "-" + data.getEnnemyNameEs() + "-" + data.getEnnemyNameEn());

        return arenaFighter;
    }

    public void setFighterForTheDay(Player account, String dinozId) {
        if (account.getHermitStageFightingDinoz() == null || account.getHermitStageFightingDinoz().isEmpty()) {
            playerRepository.updateHermitStageFightingDinoz(account.getId(), dinozId);
        }
    }

    public void setIncrementWin(Player account) {
        playerRepository.updateHermitStageCurrentWins(account.getId(), account.getHermitStageCurrentWins() + 1);
    }

    public void resetWins(Player account) {
        playerRepository.updateHermitStageCurrentWins(account.getId(), 0);
    }

    private String getRandomRace(Rng rng) {
        return String.valueOf("0123456789ACDGH".charAt(rng.randIntBetween(0, 15)));
    }

    private String getRandomHexString(Rng rng) {
        String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuffer sb = new StringBuffer();

        while (sb.length() < 21) {
            sb.append(alphabet.charAt(rng.randIntBetween(0, 62)));
        }

        return sb.toString().substring(0, 21);
    }

    private Integer getEnnemyLevelByStage(Integer actualStage) {
        Integer level = 0;
        if (actualStage <= 250) {
            level = (int) (Math.floor(Math.sqrt(actualStage) * 6) + (Math.floor(Math.sqrt(actualStage))));
        } else {
            level = (int) (Math.floor(Math.sqrt(actualStage) * 15) + (actualStage - 188));
        }

        return level;
    }

    private Integer getNeededWinsByStage(Integer ennemyLevel) {
        Rng rng = neoparcConfig.getRngFactory()
                .string(String.valueOf(ennemyLevel))
                .toRng();

        return rng.randIntBetween(3, Math.floorDiv(ennemyLevel + 1, 2));
    }

    private Integer getRewardCoinsByStage(Integer stage) {
        Rng rng = neoparcConfig.getRngFactory()
                .string(String.valueOf(stage))
                .toRng();

        Integer cashReward = rng.randIntBetween(2500 + (stage * 10), 2500 + (stage * 100));
        if (stage > 250) {
            cashReward = rng.randIntBetween((stage * 100), (stage * 200));
        }

        return cashReward;
    }

    private void initDataIfNew(Player account) {
        if (account.getHermitStage() == null || account.getHermitStage() == 0) {
            playerRepository.updateHermitStage(account.getId(), 1);
        }

        if (account.getHermitStageCurrentWins() == null) {
            playerRepository.updateHermitStageCurrentWins(account.getId(), 0);
        }
    }

    private void checkForRewards(Player account, HermitStageDto stageDto) {
        if (account.getHermitStageCurrentWins() >= getNeededWinsByStage(stageDto.getEnnemyLevel())) {
            stageDto.setDisplayingReward(true);
            stageDto.getRewards().put(DinoparcConstants.COINS, getRewardCoinsByStage(stageDto.getActualStage()));
            if (specialStages.contains(stageDto.getActualStage())) {
                switch (stageDto.getActualStage()) {
                    case 10 :
                        stageDto.getRewards().put(DinoparcConstants.GLOBE_CHARBON, 1);
                        stageDto.getRewards().put(DinoparcConstants.POTION_IRMA, 25);
                        stageDto.getRewards().put(DinoparcConstants.NUAGE_BURGER, 10);
                        stageDto.getRewards().put(DinoparcConstants.PAIN_CHAUD, 1);
                        break;

                    case 25 :
                        stageDto.getRewards().put(DinoparcConstants.GLOBE_BRONZE, 1);
                        stageDto.getRewards().put(DinoparcConstants.POTION_IRMA, 50);
                        stageDto.getRewards().put(DinoparcConstants.TARTE_VIANDE, 10);
                        stageDto.getRewards().put(DinoparcConstants.PAIN_CHAUD, 3);
                        break;

                    case 50 :
                        stageDto.getRewards().put(DinoparcConstants.GLOBE_ARGENT, 1);
                        stageDto.getRewards().put(DinoparcConstants.POTION_IRMA, 100);
                        stageDto.getRewards().put(DinoparcConstants.TARTE_VIANDE, 10);
                        stageDto.getRewards().put(DinoparcConstants.PAIN_CHAUD, 5);
                        stageDto.getRewards().put(DinoparcConstants.CHARME_PRISMATIK, 50);
                        break;

                    case 100 :
                        stageDto.getRewards().put(DinoparcConstants.GLOBE_OR, 1);
                        stageDto.getRewards().put(DinoparcConstants.POTION_IRMA, 100);
                        stageDto.getRewards().put(DinoparcConstants.TARTE_VIANDE, 10);
                        stageDto.getRewards().put(DinoparcConstants.PAIN_CHAUD, 10);
                        stageDto.getRewards().put(DinoparcConstants.CHARME_PRISMATIK, 100);
                        break;

                    case 250 :
                        stageDto.getRewards().put(DinoparcConstants.GLOBE_PLATINE, 1);
                        stageDto.getRewards().put(DinoparcConstants.POTION_IRMA, 250);
                        stageDto.getRewards().put(DinoparcConstants.TARTE_VIANDE, 25);
                        stageDto.getRewards().put(DinoparcConstants.PAIN_CHAUD, 25);
                        stageDto.getRewards().put(DinoparcConstants.CHARME_PRISMATIK, 250);
                        break;

                    case 500 :
                        stageDto.getRewards().put(DinoparcConstants.GLOBE_DIAMANT, 1);
                        stageDto.getRewards().put(DinoparcConstants.POTION_IRMA, 1000);
                        stageDto.getRewards().put(DinoparcConstants.TARTE_VIANDE, 100);
                        stageDto.getRewards().put(DinoparcConstants.PAIN_CHAUD, 50);
                        stageDto.getRewards().put(DinoparcConstants.CHARME_PRISMATIK, 500);
                        break;
                }
            }
        }
    }

    private String getGlobeEpicNumber(String rewardKey) {
        switch(rewardKey) {
            case DinoparcConstants.GLOBE_CHARBON :
                return "4";
            case DinoparcConstants.GLOBE_BRONZE:
                return "5";
            case DinoparcConstants.GLOBE_ARGENT:
                return "6";
            case DinoparcConstants.GLOBE_OR:
                return "7";
            case DinoparcConstants.GLOBE_PLATINE:
                return "8";
            case DinoparcConstants.GLOBE_DIAMANT:
                return "9";
            default:
                return "";
        }
    }
}
