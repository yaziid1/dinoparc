package com.dinoparc.api.services;

import com.dinoparc.api.configuration.NeoparcConfig;
import com.dinoparc.api.controllers.AccountController;
import com.dinoparc.api.controllers.dto.HOFDinozDto;
import com.dinoparc.api.controllers.dto.PublicAccountDto;
import com.dinoparc.api.controllers.dto.WheelPrizeDto;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.News;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.RandomCollection;
import com.dinoparc.api.domain.clan.Clan;
import com.dinoparc.api.domain.clan.OccupationData;
import com.dinoparc.api.domain.clan.OccupationSummary;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.DinozUtils;
import com.dinoparc.api.domain.dinoz.EventDinozDto;
import com.dinoparc.api.domain.misc.RaidBossInfo;
import com.dinoparc.api.domain.mission.PlayerMission;
import com.dinoparc.api.rand.Rng;
import com.dinoparc.api.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
@EnableScheduling
public class UtilsService {
  private static final int dinozPageSize = 30;

  public final NeoparcConfig neoparcConfig;
  private final PgPlayerRepository playerRepository;
  private final PgPlayerStatRepository playerStatRepository;
  private final PgDinozRepository dinozRepository;
  private final PgNewsRepository newsRepository;
  private final PgInventoryRepository inventoryRepository;
  private final AccountService accountService;
  private final PgBossRepository raidBossRepository;
  private final PgWildWistitiRepository wildWistitiRepository;
  private final PgClanRepository clanRepository;
  private final PgArmyDinozRepository armyDinozRepository;
  private final PgPlayerMissionRepository playerMissionRepository;
  private final ClanService clanService;


  private Iterator<Player> iterator;
  private Iterator<Player> iteratorX;
  private List<PublicAccountDto> generalRankingsList = new ArrayList<PublicAccountDto>();
  private List<PublicAccountDto> averageRankingsList = new ArrayList<PublicAccountDto>();

  @Autowired
  public UtilsService(
          PgPlayerRepository playerRepository,
          PgPlayerStatRepository playerStatRepository, PgNewsRepository newsRepository,
          PgDinozRepository dinozRepository,
          PgInventoryRepository inventoryRepository,
          AccountService accountService,
          PgBossRepository raidBossRepository,
          PgWildWistitiRepository wildWistitiRepository,
          PgClanRepository clanRepository,
          PgArmyDinozRepository armyDinozRepository,
          PgPlayerMissionRepository playerMissionRepository,
          ClanService clanService,
          NeoparcConfig neoparcConfig) {

    this.playerRepository = playerRepository;
    this.playerStatRepository = playerStatRepository;
    this.newsRepository = newsRepository;
    this.dinozRepository = dinozRepository;
    this.inventoryRepository = inventoryRepository;
    this.raidBossRepository = raidBossRepository;
    this.wildWistitiRepository = wildWistitiRepository;
    this.accountService = accountService;
    this.clanRepository = clanRepository;
    this.armyDinozRepository = armyDinozRepository;
    this.playerMissionRepository = playerMissionRepository;
    this.clanService = clanService;
    this.neoparcConfig = neoparcConfig;
  }

  @Scheduled(cron = "0 0 0 * * ?", zone = "Europe/Paris")
  public void maintenanceGenerale() {
    List<Player> accountsToUpdate = new ArrayList<Player>();
    Iterable<Player> allAccounts = playerRepository.findAll();

    // Players daily reset
    for (Player account : allAccounts) {
      var playerDinozs = dinozRepository.findByPlayerId(account.getId());

      if (playerDinozs.size() > 0) {
        this.refreshPoints(account);
        playerRepository.dailyReset(account);
        playerStatRepository.resetAllPlayerStat(UUID.fromString(account.getId()), PgPlayerStatRepository.NB_WISTITI_SKIP);
        playerStatRepository.resetAllPlayerStat(UUID.fromString(account.getId()), PgPlayerStatRepository.NB_DAILY_RAID_FIGHT);

        for (var playerDinoz : playerDinozs) {
          resetDailyActions(playerDinoz);
          toggleClimbNavigate(playerDinoz);
          AccountService.applyMaraisStateByDay(playerDinoz);
          dinozRepository.processDailyReset(playerDinoz.getId(), playerDinoz.getLife(), playerDinoz.getActionsMap(), playerDinoz.getLastFledEnnemy());
        }
      }

      //Put non stackable potions on account :
      Integer playerIrmasSurplusStock = inventoryRepository.getQty(account.getId(), DinoparcConstants.POTION_IRMA_SURPLUS);
      Integer surplusPotionsForPlayer = Math.floorDiv((60 * (750 - playerRepository.countDinoz(account.getId()))), 100);
      if (surplusPotionsForPlayer > 0 && playerIrmasSurplusStock < surplusPotionsForPlayer) {
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.POTION_IRMA_SURPLUS, (surplusPotionsForPlayer - playerIrmasSurplusStock));
      }

      //Remove completed missions and expired missions from player's list :
      for (PlayerMission currentMission : playerMissionRepository.getAllMissions(UUID.fromString(account.getId()))) {
        if (currentMission.isRewardClaimed() || ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond() >= currentMission.getExpirationDate() / 1000) {
          playerMissionRepository.delete(currentMission.getId());
        }
      }

      //Don't include ghost accounts (accounts with 0 Dinoz) to the wistiti group shuffle :
      if (Objects.nonNull(account.getNbPoints()) && account.getNbPoints() > 0) {
        accountsToUpdate.add(account);
      }
    }

    // Wistiti reset
    Collections.shuffle(accountsToUpdate);
    List<List<Player>> groups = IntStream.range(0, accountsToUpdate.size())
            .boxed()
            .collect(Collectors.groupingBy(i -> i % 20))
            .values()
            .stream()
            .map(il -> il.stream().map(accountsToUpdate::get).collect(Collectors.toList()))
            .collect(Collectors.toList());

    List<String> groups1Ids = new ArrayList<String>();
    List<String> groups2Ids = new ArrayList<String>();
    List<String> groups3Ids = new ArrayList<String>();
    List<String> groups4Ids = new ArrayList<String>();
    List<String> groups5Ids = new ArrayList<String>();
    splitPlayersIntoFiveGroups(groups, groups1Ids, groups2Ids, groups3Ids, groups4Ids, groups5Ids);
    accountService.getHunterGroups().get(1).setAccountIds(groups1Ids);
    accountService.getHunterGroups().get(1).setHour(ThreadLocalRandom.current().nextInt(0, 6 + 1)); //7 creneaux au total en G1
    accountService.getHunterGroups().get(2).setAccountIds(groups2Ids);
    accountService.getHunterGroups().get(2).setHour(ThreadLocalRandom.current().nextInt(7, 13 + 1)); //7 creneaux au total en G2
    accountService.getHunterGroups().get(3).setAccountIds(groups3Ids);
    accountService.getHunterGroups().get(3).setHour(ThreadLocalRandom.current().nextInt(14, 18 + 1)); //5 creneaux au total en G3
    accountService.getHunterGroups().get(4).setAccountIds(groups4Ids);
    accountService.getHunterGroups().get(4).setHour(ThreadLocalRandom.current().nextInt(19, 21 + 1)); //3 creneaux au total en G4
    accountService.getHunterGroups().get(5).setAccountIds(groups5Ids);
    accountService.getHunterGroups().get(5).setHour(ThreadLocalRandom.current().nextInt(22, 23 + 1)); //2 creneaux au total en G5

    updateClansData();
    regenerateWistiti();
    EventFightService.wistitiWasCaughtToday = false;
    EventFightService.wistitiLogs.clear();

    // Clean army dinozs of every boss
    armyDinozRepository.cleanAll();

    // Try to compute raid boss
    computeRaidBoss(false);
    EventFightService.raidBossLogs.clear();

    // Clean stats
    AccountController.connectedToday.clear();
    AccountController.enrolledToday.clear();
  }

  private void updateClansData() {
    for (Clan clan : clanRepository.getAllClans()) {
      if (clan.getMembers().size() > 0) {
        Integer nbDinozOfClan = 0;
        Integer nbWarriorDinozOfClan = 0;
        for (String playerId : clan.getMembers()) {
          nbDinozOfClan = nbDinozOfClan + playerRepository.countDinoz(playerId);
          nbWarriorDinozOfClan = nbWarriorDinozOfClan
                  + dinozRepository.findByPlayerId(playerId)
                  .stream()
                  .filter(dinoz -> (
                                  dinoz.getMalusList().contains(DinoparcConstants.FACTION_1_GUERRIER)
                                  || dinoz.getMalusList().contains(DinoparcConstants.FACTION_2_GUERRIER)
                                  || dinoz.getMalusList().contains(DinoparcConstants.FACTION_3_GUERRIER)
                          )
                  )
                  .toList()
                  .size();
        }
        clanRepository.updateClanNbOfDinoz(clan, nbDinozOfClan);
        clanRepository.updateClanNbOfWarriorDinoz(clan, nbWarriorDinozOfClan);

        if (!clan.getMembers().contains(clan.getCreatorId())) {
          clanRepository.updateChef(clan.getId(), clan.getMembers().get(0));
        }
      } else {
        clanRepository.deleteGhostClan(clan.getId());
      }
    }
  }

  @Scheduled(cron = "0 0 * * * ?", zone = "Europe/Paris")
  public void resetWistitiStatsNextHour() {
    EventDinozDto eventDinozDto = wildWistitiRepository.getWildWistiti();
    if (eventDinozDto.getLife() > 0) {
      Integer currentHourInDay = ZonedDateTime.now(ZoneId.of("Europe/Paris")).getHour();
      Integer randomLife = 500 + (currentHourInDay * (50));

      wildWistitiRepository.resetWildWistiti(eventDinozDto.getAppearanceCode(), randomLife, eventDinozDto.getFightCount(),
              ThreadLocalRandom.current().nextInt(4, 9 + 1),
              ThreadLocalRandom.current().nextInt(4, 9 + 1),
              ThreadLocalRandom.current().nextInt(4, 9 + 1),
              ThreadLocalRandom.current().nextInt(4, 9 + 1),
              ThreadLocalRandom.current().nextInt(4, 9 + 1));
    }
  }

  @Scheduled(cron = "0 0 12 * * MON", zone = "Europe/Paris")
  public void resetRaidBossEverySundayNight() {
    computeRaidBoss(true);
  }

  @Scheduled(cron = "0 0 * * * ?", zone = "Europe/Paris")
  public void moveMerchantAroundMap() {
    accountService.changeMerchantLocation(getRandomLocation());
  }

  @Scheduled(cron = "0 0/5 * 1/1 * ?", zone = "Europe/Paris")
  public void refreshAllClansNumberOfDinozAndNumberOfWarriorDinoz() {
    //This auto-updater runs every 5 minutes :
    updateClansData();
  }

  @Scheduled(cron = "0 0/1 * 1/1 * ?", zone = "Europe/Paris")
  public void spawnPVEWaveAndRefreshClansOccupationSummary() {
    //This auto-updater runs every minute :
    if (DinoparcConstants.WAR_MODE) {
      List<Dinoz> newPVEWarriors = new ArrayList<>();
      if (clanService.occupationSummary == null || clanService.occupationSummary.getAlivePVEWarEnnemies().isEmpty()) {
        for (int i = 1; i < ThreadLocalRandom.current().nextInt(75, 125 + 1); i++) {
          Dinoz newPVEWarriorEnnemy = clanService.createRandomPVEWarriorDinoz();
          newPVEWarriors.add(newPVEWarriorEnnemy);
        }
      }
      clanService.computeOccupationSummary(newPVEWarriors);
    }
  }

  @Scheduled(cron = "0 0/1 * 1/1 * ?", zone = "Europe/Paris")
  public void giveTerritoryPointsToOccupyingClans() {
    if (DinoparcConstants.WAR_MODE) {
      OccupationSummary occupationSummary = clanService.occupationSummary;
      if (Objects.nonNull(occupationSummary)) {
        for (OccupationData location : occupationSummary.getOccupationData()) {
          if (Objects.nonNull(location.getPercentage()) && Float.parseFloat(location.getPercentage()) > 50) {
            for (Clan clan : clanRepository.getAllClans().stream().filter(clan -> alliedClanDeserveTerritoryPoints(location, clan)).toList()) {
              clanRepository.addClanPointsOccupation(clan.getId(), 1);
            }
          }
        }
      }
    }
  }

//  @Scheduled(cron = "0 0 * * * ?", zone = "Europe/Paris")
//  public void moveHalloweenHordesAroundMap() {
//    accountService.changeHalloweenHordesLocation(getRandomLocation());
//  }

  public Integer getRandomLocation() {
    RandomCollection<Integer> randomElement = new RandomCollection<Integer>()
            .add(1, 0)
            .add(1, 1)
            .add(1, 2)
            .add(1, 3)
            .add(1, 4)
            .add(1, 5)
            .add(1, 6)
            .add(1, 7)
            .add(1, 8)
            .add(1, 9)
            .add(1, 10)
            .add(1, 11)
            .add(1, 12)
            .add(1, 13)
            .add(1, 14)
            .add(1, 15)
            .add(1, 16)
            .add(1, 17)
            .add(1, 18)
            .add(1, 19)
            .add(1, 20)
            .add(1, 21)
            .add(1, 22)
            .add(1, 37);
    return randomElement.next();
  }

  private void regenerateWistiti() {
    wildWistitiRepository.resetWildWistiti(
            "F" + DinozUtils.getRandomHexString(),
            500,
            0,
            ThreadLocalRandom.current().nextInt(4, 9),
            ThreadLocalRandom.current().nextInt(4, 9),
            ThreadLocalRandom.current().nextInt(4, 9),
            ThreadLocalRandom.current().nextInt(4, 9),
            ThreadLocalRandom.current().nextInt(4, 9));
  }

  public void computeRaidBoss(boolean adminAction) {
    Calendar cal = Calendar.getInstance(Locale.FRANCE);
    var life = DinoparcConstants.RAID_BOSS_MAX_LIFE;
    var bonuses = 2500;

    if (adminAction || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
      RaidBossInfo raidInfos = getNextRaidBossInfos();
      raidBossRepository.resetRaidBoss(raidInfos.getPlaceNumber(), raidInfos.getDayOfWeek(), raidInfos.getHourOfDay(),
              "B" + DinozUtils.getRandomHexString() + "#", 0,
              110, 110, 110, 110, 110,
              life,
              bonuses, bonuses, bonuses, bonuses, bonuses, bonuses,
              raidInfos.getFirstElement(), raidInfos.getSecondElement(), raidInfos.getThirdElement());
    } else {
      raidBossRepository.reviveRaidBoss(life, bonuses);
    }
  }

  @Scheduled(fixedRate = 900000)
  public void fetchGeneralRankingsEvery15Minutes() {
    this.generalRankingsList.clear();
    Iterable<Player> result = playerRepository.findAll();
    this.iterator = result.iterator();

    while (iterator.hasNext()) {
      Player next = iterator.next();
      var playerNbDinoz = playerRepository.countDinoz(next.getId());

      if (playerNbDinoz > 0) {
        this.refreshPoints(next);
      }

      PublicAccountDto dto = new PublicAccountDto();
      dto.setNbPoints(next.getNbPoints());
      dto.setId(next.getId());
      dto.setName(next.getName());
      dto.setNbDinoz(playerNbDinoz);

      this.generalRankingsList.add(dto);
    }

    this.generalRankingsList.sort(Comparator.comparingDouble(PublicAccountDto::getNbPoints).reversed());
  }

  public WheelPrizeDto getRandomChristmasPrize(Player account) {
    RandomCollection<Object> randomElement = new RandomCollection<Object>()
            .add(20, DinoparcConstants.POTION_ANGE)
            .add(20, DinoparcConstants.PAIN_CHAUD)
            .add(10, DinoparcConstants.COINS)
            .add(8, DinoparcConstants.SPECIAL_CHAMPIFUZ)
            .add(15, DinoparcConstants.CHAMPIFUZ)
            .add(10, DinoparcConstants.COINS)
            .add(5, DinoparcConstants.ETERNITY_PILL)
            .add(1, DinoparcConstants.OEUF_GLUON)
            .add(1, DinoparcConstants.OEUF_SERPANTIN)
            .add(10, DinoparcConstants.COINS);

    String consumableDrawn = (String) randomElement.next();
    WheelPrizeDto prizeDrawn = mapPrizeDrawnToWheelNumber(consumableDrawn);

    if (consumableDrawn.equals(DinoparcConstants.COINS)) {
      prizeDrawn.setQtee(accountService.getRandomAmountOfGoldWonForEgg());
      playerRepository.updateCash(account.getId(), prizeDrawn.getQtee());
    } else {
      inventoryRepository.addInventoryItem(account.getId(), consumableDrawn, 1);
    }
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.GIFT, 1);
    return prizeDrawn;
  }

  private WheelPrizeDto mapPrizeDrawnToWheelNumber(String consumableDrawn) {
    switch (consumableDrawn) {
      case DinoparcConstants.POTION_ANGE:
        return new WheelPrizeDto(0, 1);
      case DinoparcConstants.PAIN_CHAUD:
        return new WheelPrizeDto(1, 1);
      case DinoparcConstants.COINS:
        return new WheelPrizeDto(getRandomIntegerForGoldPrizeId(), 1);
      case DinoparcConstants.SPECIAL_CHAMPIFUZ:
        return new WheelPrizeDto(3, 1);
      case DinoparcConstants.CHAMPIFUZ:
        return new WheelPrizeDto(4, 1);
      case DinoparcConstants.ETERNITY_PILL:
        return new WheelPrizeDto(6, 1);
      case DinoparcConstants.OEUF_GLUON:
        return new WheelPrizeDto(7, 1);
      case DinoparcConstants.OEUF_SERPANTIN:
        return new WheelPrizeDto(8, 1);

     default:
        return null;
    }
  }

  public int getRandomIntegerForGoldPrizeId() {
    List<Integer> list = new ArrayList<>(Arrays.asList(2, 5, 9));
    return list.get(new Random().nextInt(list.size()));
  }

  private void refreshPoints(Player account) {
    Integer sumOfPoints = 0;
    List<Dinoz> playerDinoz = this.getAllDinozOfAccount(account);
    int nbDinozs = playerDinoz.isEmpty() ? 1 : playerDinoz.size();

    for (Dinoz dinoz : playerDinoz) {
      sumOfPoints += dinoz.getLevel();
    }

    account.setNbPoints(sumOfPoints);
    account.setAverageLevelPoints(sumOfPoints / nbDinozs);
  }

  private void toggleClimbNavigate(Dinoz dinoz) {
    switch (dinoz.getPlaceNumber()) {
      case 4:
        // Barrage
        dinoz.getActionsMap().put(DinoparcConstants.BARRAGE, true);
        break;

      case 7:
        // Porte de Granit
        dinoz.getActionsMap().put(DinoparcConstants.PASSAGEGRANIT, true);
        break;

      case 11:
        // Port de la Prune
        dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
        dinoz.getActionsMap().put(DinoparcConstants.ACT_NAVIGUER, true);
        break;

      case 16:
        // Marais Collant
        dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
        break;

      case 21:
        // Hutte du Vieux sage
        dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
        break;

      default:
        break;
    }
  }

  private void resetDailyActions(Dinoz dinoz) {
    if (dinoz.getActionsMap().get(DinoparcConstants.POTION_IRMA) != null) {
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, false);
    }

    dinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
    dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.FOUILLER)
        && dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) != null
        && dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) > 0) {

      dinoz.getActionsMap().put(DinoparcConstants.FOUILLER, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.ROCK)
        && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null
        && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {

      dinoz.getActionsMap().put(DinoparcConstants.ROCK, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.BAIN_FLAMMES)
            && dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) > 0
            && dinoz.getLife() < 10) {

      dinoz.getActionsMap().put(DinoparcConstants.BAIN_FLAMMES, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.PÊCHE)
            && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) > 0
            && DinoparcConstants.FISHING_LOCS.contains(dinoz.getPlaceNumber())) {

      dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.CUEILLETTE)
            && dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) > 0
            && DinoparcConstants.PICKING_LOCS.contains(dinoz.getPlaceNumber())) {

      dinoz.getActionsMap().put(DinoparcConstants.CUEILLETTE, true);
    }

    if (dinoz.getMalusList().contains(DinoparcConstants.CHIKABUM) && dinoz.getLife() > 0) {
    	dinoz.setLife(dinoz.getLife() - 1);
    }

    if (dinoz.getMalusList().contains(DinoparcConstants.EMPOISONNE) && dinoz.getLife() > 4) {
      dinoz.setLife(dinoz.getLife() - 5);

    } else if (dinoz.getMalusList().contains(DinoparcConstants.EMPOISONNE) && dinoz.getLife() > 0 && dinoz.getLife() < 5) {
      dinoz.setLife(0);
    }

    if (dinoz.getLife() < 0) {
      dinoz.setLife(0);
    }

    if (dinoz.getRace().equalsIgnoreCase(DinoparcConstants.KORGON) && dinoz.getLife() > 0) {
        if (dinoz.getPassiveList().get(DinoparcConstants.CHARME_TERRE) == null || dinoz.getPassiveList().get(DinoparcConstants.CHARME_TERRE) == 0) {
            dinoz.getPassiveList().put(DinoparcConstants.CHARME_TERRE, 1);
            dinozRepository.setPassiveList(dinoz.getId(), dinoz.getPassiveList());
        }
    }

    dinoz.setLastFledEnnemy(null);
  }


  @Scheduled(fixedRate = 900000)
  public void fetchAverageRankingsEvery15Minutes() {
    this.averageRankingsList.clear();
    Iterable<Player> result = playerRepository.findAll();
    this.iteratorX = result.iterator();

    while (iteratorX.hasNext()) {
      Player next = iteratorX.next();
      var playerNbDinoz = playerRepository.countDinoz(next.getId());
      PublicAccountDto dto = new PublicAccountDto();

      if (playerNbDinoz > 0) {
        float totalPoints = Float.parseFloat(String.valueOf(next.getNbPoints()));
        float totalDinoz = Float.parseFloat(String.valueOf(playerNbDinoz));
        float m = totalPoints / totalDinoz;
        String moyenne = String.format("%.1f", m).replace(",", ".");
        dto.setAverageLevelPoints(Double.valueOf(moyenne));

      } else {
        dto.setAverageLevelPoints((double) 0);
      }

      dto.setId(next.getId());
      dto.setName(next.getName());
      this.averageRankingsList.add(dto);
    }

    this.averageRankingsList.sort(Comparator.comparingDouble(PublicAccountDto::getAverageLevelPoints).reversed());
  }

  public Integer getGeneralRankingsTotalPages() {
    if (this.generalRankingsList.size() % 30 == 0) {
      return this.generalRankingsList.size() / 30;

    } else {
      return (this.generalRankingsList.size() / 30) + 1;
    }
  }

  public List<PublicAccountDto> getGeneralRankingsAtGivenPage(Integer pageNumber) {
    Integer offset = pageNumber * 30;

    try {
      return this.generalRankingsList.subList(offset, offset + 30);
    } catch (IndexOutOfBoundsException e) {
      try {
        return this.generalRankingsList.subList(offset, this.generalRankingsList.size());

      } catch (Exception finalException) {
        return new ArrayList<PublicAccountDto>();
      }
    }
  }

  public Integer getUsernameRanking(String username) {
    Integer counter = 0;
    for (PublicAccountDto dto : this.generalRankingsList) {
      if (!dto.getName().equalsIgnoreCase(username)) {
        counter++;

      } else {
        break;
      }
    }

    if (counter == 0) {
      return 0;
    }

    if (counter == this.generalRankingsList.size()) {
      return 0;
    }

    return (counter / 30);
  }

  public Integer getUsernameRankingForProfile(String username) {
    Integer counter = 1;
    for (PublicAccountDto dto : this.generalRankingsList) {
      if (!dto.getName().equalsIgnoreCase(username)) {
        counter++;

      } else {
        break;
      }
    }

    return counter;
  }

  public void createANews(News news) {
    newsRepository.create(news);
  }

  public List<News> getLatestNews() {
    return newsRepository.findAll();
  }

  public List<PublicAccountDto> getAverageRankingsAtGivenPage(Integer pageNumber) {
    Integer offset = pageNumber * 30;

    try {
      return this.averageRankingsList.subList(offset, offset + 30);
    } catch (IndexOutOfBoundsException e) {
      try {
        return this.averageRankingsList.subList(offset, this.averageRankingsList.size());

      } catch (Exception finalException) {
        return new ArrayList<PublicAccountDto>();
      }
    }
  }

  public Integer getUsernameRankingX(String username) {
    Integer counter = 0;
    for (PublicAccountDto dto : this.averageRankingsList) {
      if (!dto.getName().equalsIgnoreCase(username)) {
        counter++;

      } else {
        break;
      }
    }

    if (counter == 0) {
      return 0;
    }

    if (counter == this.averageRankingsList.size()) {
      return 0;
    }

    return (counter / 30);
  }

  public PublicAccountDto getPublicAccountById(String userId, Integer pageNumber) {
    PublicAccountDto response = new PublicAccountDto();
    Optional<Player> account = playerRepository.findById(userId);

    if (account.isPresent()) {
      response.setId(userId);
      response.setName(account.get().getName());
      response.setNbDinoz(playerRepository.countDinoz(account.get().getId()));
      response.setNbPoints(account.get().getNbPoints());
      response.setPosition(getUsernameRankingForProfile(response.getName()));
      response.setActualHermitStage(account.get().getHermitStage());
      response.setCap(account.get().getAccountDinozLimit());
      if (clanRepository.getPlayerClan(userId).isPresent()) {
        response.setClanName(clanRepository.getPlayerClan(userId).get().getClanName());
      }
      if (account.get().getWistitiCaptured() != null) {
        response.setNbCaptures(account.get().getWistitiCaptured());
      } else {
        response.setNbCaptures(0);
      }
    } else {
      response = null;
    }

    return response;
  }

  public List<Dinoz> getAllDinozOfAccount(Player account) {
    return dinozRepository.findByPlayerId(account.getId());
  }

  public Integer getTotalPages(String userId) {
    var playerNbDinoz = playerRepository.countDinoz(userId);
    return playerNbDinoz % dinozPageSize == 0 ?
            (playerNbDinoz / dinozPageSize) :
            (playerNbDinoz / dinozPageSize) + 1;
  }

  public HashMap<String, String> getAllPlayerIds() {
      HashMap<String, String> playerIdentification = new HashMap<String, String>();
      playerRepository.findAll().forEach( account -> {
        playerIdentification.put(account.getId(), account.getName());
      });

      return playerIdentification;
  }

  public List<Integer> getBankValues() {
    List<Integer> bankValues = new ArrayList<Integer>();
    Rng rng = neoparcConfig.getRngFactory()
            .string("getBankValues")
            .string(String.valueOf(ZonedDateTime.now(ZoneId.of("Europe/Paris")).get(ChronoField.ALIGNED_WEEK_OF_YEAR)))
            .toRng();

    int randomBaseValue = rng.randIntBetween(5000, 6000);
    bankValues.add(rng.randIntBetween(randomBaseValue, randomBaseValue + 1000));
    bankValues.add(rng.randIntBetween(randomBaseValue - 1000, randomBaseValue));

    return bankValues;
  }

  public List<HOFDinozDto> getRaceRankings(String chapter) {
    return dinozRepository.getRanking(chapter);
  }

  private void splitPlayersIntoFiveGroups(List<List<Player>> groups, List<String> groups1Ids, List<String> groups2Ids, List<String> groups3Ids, List<String> groups4Ids, List<String> groups5Ids) {
    for (Player account : groups.get(0)) {
      playerRepository.updateHunterGroup(account.getId(), 1);
      groups1Ids.add(account.getId());
    }

    for (Player account : groups.get(1)) {
      playerRepository.updateHunterGroup(account.getId(), 1); //10% of players in G1
      groups1Ids.add(account.getId());
    }

    for (Player account : groups.get(2)) {
      playerRepository.updateHunterGroup(account.getId(), 2);
      groups2Ids.add(account.getId());
    }

    for (Player account : groups.get(3)) {
      playerRepository.updateHunterGroup(account.getId(), 2); //10% of players in G2
      groups2Ids.add(account.getId());
    }

    for (Player account : groups.get(4)) {
      playerRepository.updateHunterGroup(account.getId(), 3);
      groups3Ids.add(account.getId());
    }

    for (Player account : groups.get(5)) {
      playerRepository.updateHunterGroup(account.getId(), 3);
      groups3Ids.add(account.getId());
    }

    for (Player account : groups.get(6)) {
      playerRepository.updateHunterGroup(account.getId(), 3); //15% of players in G3
      groups4Ids.add(account.getId());
    }

    for (Player account : groups.get(7)) {
      playerRepository.updateHunterGroup(account.getId(), 4);
      groups4Ids.add(account.getId());
    }

    for (Player account : groups.get(8)) {
      playerRepository.updateHunterGroup(account.getId(), 4);
      groups5Ids.add(account.getId());
    }

    for (Player account : groups.get(9)) {
      playerRepository.updateHunterGroup(account.getId(), 4); //15% of players in G4
      groups5Ids.add(account.getId());
    }

    for (Player account : groups.get(10)) {
      playerRepository.updateHunterGroup(account.getId(), 5);
    }

    for (Player account : groups.get(11)) {
      playerRepository.updateHunterGroup(account.getId(), 5);
    }

    for (Player account : groups.get(12)) {
      playerRepository.updateHunterGroup(account.getId(), 5); //15% of players in G5
    }

    for (Player account : groups.get(13)) {
      playerRepository.updateHunterGroup(account.getId(), 6);
    }

    for (Player account : groups.get(14)) {
      playerRepository.updateHunterGroup(account.getId(), 6);
    }

    for (Player account : groups.get(15)) {
      playerRepository.updateHunterGroup(account.getId(), 6);
    }

    for (Player account : groups.get(16)) {
      playerRepository.updateHunterGroup(account.getId(), 6);
    }

    for (Player account : groups.get(17)) {
      playerRepository.updateHunterGroup(account.getId(), 6);
    }

    for (Player account : groups.get(18)) {
      playerRepository.updateHunterGroup(account.getId(), 6);
    }

    for (Player account : groups.get(19)) {
      playerRepository.updateHunterGroup(account.getId(), 6); //35% can't see
    }
  }

  private RaidBossInfo getNextRaidBossInfos() {
    RaidBossInfo raidBossInfo = new RaidBossInfo();
    ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
    Rng rng = neoparcConfig.getRngFactory()
            .string("getNextRaidBossInfos_V8")
            .string(getWeekOfYear())
            .string(String.valueOf(now.getYear()))
            .toRng();

    raidBossInfo.setDayOfWeek(rng.randIntBetween(5, 8));
    raidBossInfo.setHourOfDay(rng.randIntBetween(19, 23));
    raidBossInfo.setPlaceNumber(rng.randIntBetween(0, 23));
    List<String> possibleElements = new ArrayList(Arrays.asList(DinoparcConstants.FEU, DinoparcConstants.TERRE, DinoparcConstants.EAU, DinoparcConstants.FOUDRE, DinoparcConstants.AIR));

    int firstE = rng.randIntBetween(0, 5);
    raidBossInfo.setFirstElement(possibleElements.get(firstE));
    possibleElements.remove(firstE);

    int secondE = rng.randIntBetween(0, 4);
    raidBossInfo.setSecondElement(possibleElements.get(secondE));
    possibleElements.remove(secondE);

    int thirdE = rng.randIntBetween(0, 3);
    raidBossInfo.setThirdElement(possibleElements.get(thirdE));

    return raidBossInfo;
  }

  private String getWeekOfYear() {
    Calendar cal = Calendar.getInstance(Locale.FRANCE);
    return String.valueOf(cal.get(Calendar.WEEK_OF_YEAR));
  }

  private boolean alliedClanDeserveTerritoryPoints(OccupationData location, Clan clan) {
    Long warriorsAliveOnLocation = clanRepository.getAllWarriorDinozOfClan(clan.getId())
            .stream()
            .filter(warrior -> warrior.getLife() > 0)
            .filter(warrior -> new Integer(warrior.getPlaceNumber()).equals(Integer.parseInt(location.getLocation())))
            .count();
    return clan.getFaction().equalsIgnoreCase(location.getFaction()) && warriorsAliveOnLocation >= 1;
  }
}