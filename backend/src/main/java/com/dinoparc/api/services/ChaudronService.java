package com.dinoparc.api.services;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.RandomCollection;
import com.dinoparc.api.domain.misc.ChaudronCraftTryResult;
import com.dinoparc.api.repository.PgInventoryRepository;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class ChaudronService {

    public ChaudronService() {}

    public ChaudronCraftTryResult findMatchingRecipe(String playerId, PgInventoryRepository inventoryRepository, Map<String, Integer> objects) {
        Integer nbFish1 = objects.get(DinoparcConstants.PERCHE_PERLEE);
        Integer nbFish2 = objects.get(DinoparcConstants.GREMILLE_GRELOTTANTE);
        Integer nbFish3 = objects.get(DinoparcConstants.CUBE_GLU);
        Integer nbPlant1 = objects.get(DinoparcConstants.FEUILLE_PACIFIQUE);
        Integer nbPlant2 = objects.get(DinoparcConstants.OREADE_BLANC);
        Integer nbPlant3 = objects.get(DinoparcConstants.TIGE_RONCIVORE);
        Integer nbPlant4 = objects.get(DinoparcConstants.ANÉMONE_SOLITAIRE);

        return processRecipeIsCompleted(playerId, inventoryRepository, nbFish1, nbFish2, nbFish3, nbPlant1, nbPlant2, nbPlant3, nbPlant4);
    }

    private ChaudronCraftTryResult processRecipeIsCompleted(String playerId, PgInventoryRepository inventoryRepository,
                                             Integer nbFish1, Integer nbFish2, Integer nbFish3, Integer nbPlant1, Integer nbPlant2, Integer nbPlant3, Integer nbPlant4) {

        ChaudronCraftTryResult result = new ChaudronCraftTryResult();

        if (nbFish1 == 50 && nbFish2 == 10 && nbFish3 == 0 && nbPlant1 == 50 && nbPlant2 == 10 && nbPlant3 == 0 && nbPlant4 == 0) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.PERCHE_PERLEE, nbFish1);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.GREMILLE_GRELOTTANTE, nbFish2);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.FEUILLE_PACIFIQUE, nbPlant1);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.OREADE_BLANC, nbPlant2);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.NUAGE_BURGER, 10);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.NUAGE_BURGER);
            result.setGeneratedObjectQty(10);

        } else if (nbFish1 == 75 && nbFish2 == 15 && nbFish3 == 0 && nbPlant1 == 60 && nbPlant2 == 20 && nbPlant3 == 1 && nbPlant4 == 0) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.PERCHE_PERLEE, nbFish1);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.GREMILLE_GRELOTTANTE, nbFish2);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.FEUILLE_PACIFIQUE, nbPlant1);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.OREADE_BLANC, nbPlant2);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.TIGE_RONCIVORE, nbPlant3);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.TARTE_VIANDE, 10);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.TARTE_VIANDE);
            result.setGeneratedObjectQty(10);

        } else if (nbFish1 == 0 && nbFish2 == 0 && nbFish3 == 0 && nbPlant1 == 90 && nbPlant2 == 45 && nbPlant3 == 10 && nbPlant4 == 0) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.FEUILLE_PACIFIQUE, nbPlant1);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.OREADE_BLANC, nbPlant2);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.TIGE_RONCIVORE, nbPlant3);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.MEDAILLE_CHOCOLAT, 10);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.MEDAILLE_CHOCOLAT);
            result.setGeneratedObjectQty(10);

        } else if (nbFish1 == 0 && nbFish2 == 0 && nbFish3 == 0 && nbPlant1 == 20 && nbPlant2 == 5 && nbPlant3 == 3 && nbPlant4 == 1) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.FEUILLE_PACIFIQUE, nbPlant1);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.OREADE_BLANC, nbPlant2);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.TIGE_RONCIVORE, nbPlant3);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.ANÉMONE_SOLITAIRE, nbPlant4);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.TISANE, 1);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.TISANE);
            result.setGeneratedObjectQty(1);

        } else if (nbFish1 == 0 && nbFish2 == 20 && nbFish3 == 1 && nbPlant1 == 0 && nbPlant2 == 0 && nbPlant3 == 20 && nbPlant4 == 2) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.GREMILLE_GRELOTTANTE, nbFish2);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.CUBE_GLU, nbFish3);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.TIGE_RONCIVORE, nbPlant3);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.ANÉMONE_SOLITAIRE, nbPlant4);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.BIERE, 1);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.BIERE);
            result.setGeneratedObjectQty(1);

        } else if (nbFish1 == 0 && nbFish2 == 0 && nbFish3 == 25 && nbPlant1 == 0 && nbPlant2 == 0 && nbPlant3 == 0 && nbPlant4 == 25) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.CUBE_GLU, nbFish3);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.ANÉMONE_SOLITAIRE, nbPlant4);
            getRandomRareItem(result, inventoryRepository, playerId);
        }

        return result;
    }

    private void getRandomRareItem(ChaudronCraftTryResult result, PgInventoryRepository inventoryRepository, String playerId) {
        RandomCollection<Object> randomElement = new RandomCollection<>()
                .add(25, DinoparcConstants.POTION_IRMA)
                .add(25, DinoparcConstants.CHARME_PRISMATIK)
                .add(20, DinoparcConstants.PAIN_CHAUD)
                .add(15, DinoparcConstants.GOUTTE_GLU)
                .add(9, DinoparcConstants.ETERNITY_PILL)
                .add(3, DinoparcConstants.OEUF_COBALT)
                .add(3, DinoparcConstants.COULIS_CERISE);

        String drawnElement = (String) randomElement.next();
        if (drawnElement.equalsIgnoreCase(DinoparcConstants.POTION_IRMA)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.POTION_IRMA);
            Integer qty = ThreadLocalRandom.current().nextInt(300, 500);
            result.setGeneratedObjectQty(qty);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.POTION_IRMA, qty);

        } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.PAIN_CHAUD)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.PAIN_CHAUD);
            Integer qty = ThreadLocalRandom.current().nextInt(50, 75);
            result.setGeneratedObjectQty(qty);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.PAIN_CHAUD, qty);

        } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.CHARME_PRISMATIK)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.CHARME_PRISMATIK);
            Integer qty = ThreadLocalRandom.current().nextInt(100, 125);
            result.setGeneratedObjectQty(qty);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.CHARME_PRISMATIK, qty);

        } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.GOUTTE_GLU)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.GOUTTE_GLU);
            Integer qty = ThreadLocalRandom.current().nextInt(500, 1000);
            result.setGeneratedObjectQty(qty);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.GOUTTE_GLU, qty);

        } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.OEUF_COBALT)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.OEUF_COBALT);
            result.setGeneratedObjectQty(1);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.OEUF_COBALT, 1);

        } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.ETERNITY_PILL)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.ETERNITY_PILL);
            Integer qty = ThreadLocalRandom.current().nextInt(2, 5);
            result.setGeneratedObjectQty(qty);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.ETERNITY_PILL, qty);

        } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.COULIS_CERISE)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.COULIS_CERISE);
            result.setGeneratedObjectQty(1);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.COULIS_CERISE, 1);
        }
    }
}