package com.dinoparc.api.services.migration;

/**
 * Represents a failure to check the preconditions to go ahead with the import.
 */
public class ImportCheckException extends Exception {
}
