package com.dinoparc.api.services;

import com.dinoparc.api.controllers.dto.HOFDinozDto;
import com.dinoparc.api.domain.misc.Pagination;
import com.dinoparc.api.repository.PgDinozRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DinozService {

    private final PgDinozRepository dinozRepository;

    public DinozService(PgDinozRepository dinozRepository) {
        this.dinozRepository = dinozRepository;
    }

    public Pagination<HOFDinozDto> getRaceRankings(String chapter, Integer offset, Integer limit) {
        if (limit == null || limit < 1 || limit > 100) {
            return dinozRepository.getRanking(chapter, offset, 100);
        }
        return dinozRepository.getRanking(chapter, offset, limit);
    }
}
