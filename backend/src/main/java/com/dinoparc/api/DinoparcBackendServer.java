package com.dinoparc.api;

import java.net.URISyntaxException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.autoconfigure.r2dbc.R2dbcAutoConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(exclude = { R2dbcAutoConfiguration.class })
@EnableSwagger2
public class DinoparcBackendServer {

  public static void main(String[] args) throws URISyntaxException {
    SpringApplication.run(DinoparcBackendServer.class, args);
  }
}
