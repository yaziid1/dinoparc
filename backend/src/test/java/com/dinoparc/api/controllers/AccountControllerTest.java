package com.dinoparc.api.controllers;

import com.dinoparc.api.DinoparcBackendServer;
import com.dinoparc.api.controllers.dto.CreateAccountDto;
import com.dinoparc.api.domain.account.BuyNewDinozRequest;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.PlayerDinozStore;
import com.dinoparc.api.repository.PgDinozRepository;
import com.dinoparc.api.repository.PgInventoryRepository;
import com.dinoparc.api.repository.PgPlayerRepository;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.jooq.DSLContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static com.dinoparc.tables.Player.PLAYER;
import static com.dinoparc.tables.PlayerDinoz.PLAYER_DINOZ;
import static com.dinoparc.tables.PlayerDinozStore.PLAYER_DINOZ_STORE;
import static com.dinoparc.tables.Dinoz.DINOZ;
import static com.dinoparc.tables.PlayerStat.PLAYER_STAT;
import static com.dinoparc.tables.Inventory.INVENTORY;
import static com.dinoparc.tables.History.HISTORY;

@SpringBootTest(classes = DinoparcBackendServer.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureEmbeddedDatabase
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class AccountControllerTest {
    @Autowired
    private AccountController accountController;
    @Autowired
    private PlayerDinozStoreController playerDinozStoreController;

    @Autowired
    private AdminController adminController;

    @Autowired
    private PgDinozRepository dinozRepository;

    @Autowired
    private PgInventoryRepository inventoryRepository;

    @Autowired
    private PgPlayerRepository playerRepository;

    @Autowired
    private DSLContext jooqContext;

    @Value("${server.test}")
    public String st;

    private Player createdPlayer;

    @BeforeEach
    public void beforeEachMethod() throws Exception {
        accountController.setTokenAccountValidation(new FakeTokenAccountValidation(null));

        CreateAccountDto toCreate = new CreateAccountDto();
        toCreate.setUsernameInscription("TestAccount");
        createdPlayer = accountController.createAccount(toCreate);
    }

    @AfterEach
    public void afterEachMethod() {
        jooqContext.deleteFrom(PLAYER).execute();
        jooqContext.deleteFrom(PLAYER_DINOZ).execute();
        jooqContext.deleteFrom(PLAYER_STAT).execute();
        jooqContext.deleteFrom(DINOZ).execute();
        jooqContext.deleteFrom(INVENTORY).execute();
        jooqContext.deleteFrom(HISTORY).execute();
    }

    @Test
    void testCreateAccount() {
        assertEquals(createdPlayer.getName(), "TestAccount");
        assertEquals(createdPlayer.getCash(), 99000);
        assertEquals(createdPlayer.getNbPoints(), 0);
        assertEquals(createdPlayer.getWistitiCaptured(), 0);;
        assertEquals(createdPlayer.getHermitStage(), 1);;
        assertEquals(createdPlayer.getDailyShiniesFought(), 0);
        assertEquals(createdPlayer.getAverageLevelPoints(), 0);
        assertFalse(createdPlayer.isBanned());
        assertFalse(createdPlayer.isBlocked());

        var accountId = createdPlayer.getId();
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.POTION_IRMA), 3);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.POTION_ANGE), 1);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.NUAGE_BURGER), 3);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.PAIN_CHAUD), 1);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.TARTE_VIANDE), 1);

        var nbPlayerDinozStore = jooqContext.selectCount().from(PLAYER_DINOZ_STORE).fetchOneInto(Integer.class);
        assertEquals(nbPlayerDinozStore, 15);
    }

    @Test
    void testDailyBonus() {
        var accountId = createdPlayer.getId();

        // Any day
        var zonedDateTime = ZonedDateTime.of(2022, 1, 1, 0, 0, 0, 0, ZoneId.of("Europe/Paris"));
        accountController.addDailyGoldBonusEvent(createdPlayer, zonedDateTime);
        assertEquals(createdPlayer.getCash(), 101000);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.COULIS_CERISE), 0);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.GIFT), 0);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_SANTAZ), 0);

        // Easter
        zonedDateTime = ZonedDateTime.of(2022, 4, 17, 0, 0, 0, 0, ZoneId.of("Europe/Paris"));
        accountController.addDailyGoldBonusEvent(createdPlayer, zonedDateTime);
        assertEquals(createdPlayer.getCash(), 103000);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.COULIS_CERISE), 1);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.GIFT), 0);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_SANTAZ), 0);

        // December first day
        zonedDateTime = ZonedDateTime.of(2022, 12, 1, 0, 0, 0, 0, ZoneId.of("Europe/Paris"));
        accountController.addDailyGoldBonusEvent(createdPlayer, zonedDateTime);
        assertEquals(createdPlayer.getCash(), 105000);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.COULIS_CERISE), 1);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.GIFT), 10);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_SANTAZ), 0);

        // December second day
        zonedDateTime = ZonedDateTime.of(2022, 12, 2, 0, 0, 0, 0, ZoneId.of("Europe/Paris"));
        accountController.addDailyGoldBonusEvent(createdPlayer, zonedDateTime);
        assertEquals(createdPlayer.getCash(), 109000);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.COULIS_CERISE), 1);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.GIFT), 20);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_SANTAZ), 0);

        // Christmas day
        zonedDateTime = ZonedDateTime.of(2022, 12, 25, 0, 0, 0, 0, ZoneId.of("Europe/Paris"));
        accountController.addDailyGoldBonusEvent(createdPlayer, zonedDateTime);
        assertEquals(createdPlayer.getCash(), 159000);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.COULIS_CERISE), 1);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.GIFT), 20);
        assertEquals(inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_SANTAZ), 1);
    }

    @Test
    void testBuyCommonDinoz() {
        var dinozToBuy = getDinozToBuy(createdPlayer, false, false);
        testBuyDinoz(createdPlayer, dinozToBuy);
    }

    @Test
    void testBuyPterozDinoz() {
        var dinozToBuy = getDinozToBuy(createdPlayer, true, false);
        testBuyDinoz(createdPlayer, dinozToBuy);
    }

    @Test
    void testBuyRokkyDinoz() {
        var dinozToBuy = getDinozToBuy(createdPlayer, false, true);
        testBuyDinoz(createdPlayer, dinozToBuy);
    }

    private void testBuyDinoz(Player createdPlayer, PlayerDinozStore dinozToBuy) {
        var accountId = createdPlayer.getId();

        BuyNewDinozRequest request = new BuyNewDinozRequest();
        request.setAccountId(accountId);
        request.setDinozName("TestDinoz");
        request.setNewBornDinozId(dinozToBuy.getId().toString());

        playerDinozStoreController.buyNewDinozFromShop("test", request);

        var createdDinoz = dinozRepository.findById(dinozToBuy.getId().toString()).get();
        assertEquals(playerRepository.countDinoz(accountId), 1);
        assertEquals(createdDinoz.getId(), dinozToBuy.getId());
        assertEquals(createdDinoz.getDanger(), 0);
        assertEquals(createdDinoz.getName(), "TestDinoz");
        assertEquals(createdDinoz.getMasterId(), accountId);
        assertEquals(createdDinoz.getAppearanceCode(), dinozToBuy.getAppearanceCode());
        assertEquals(createdDinoz.getExperience(), 0);
        assertEquals(createdDinoz.getKabukiProgression(), 0);
        assertEquals(createdDinoz.getLevel(), 1);
        assertEquals(createdDinoz.getLife(), 100);
        assertEquals(createdDinoz.getMasterName(), createdPlayer.getName());
        assertEquals(createdDinoz.getPlaceNumber(), 0);
        assertEquals(createdDinoz.getSkips(), 0);
        assertEquals(createdDinoz.getMalusList().size(), 0);
        assertEquals(createdDinoz.getPassiveList().size(), 0);
        assertEquals(createdDinoz.getLastValidBotHash(), 0);
        assertEquals(createdDinoz.getRace(), dinozToBuy.getRace());
        assertEquals(createdDinoz.isDark(), false);
        assertEquals(createdDinoz.isInBazar(), false);
        assertEquals(createdDinoz.isInTourney(), false);
        assertEquals(createdDinoz.getElementsValues(), dinozToBuy.getInitialElementsValues());

        int nbSkills = 0;
        for (var entry : createdDinoz.getSkillsMap().entrySet()) {
            if (entry.getValue() != null) {
                assertEquals(entry.getValue(), dinozToBuy.getInitialCompList().get(entry.getKey()));
                nbSkills++;
            }
        }
        assertEquals(nbSkills, dinozToBuy.getInitialCompList().size());

        if (dinozToBuy.getRace().equals(DinoparcConstants.ROKKY)) {
            assertEquals(createdDinoz.getActionsMap().size(), 4);
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.COMBAT));
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.ROCK));
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.TournoiDinoville));
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.DÉPLACERVERT));
        } else if (dinozToBuy.getRace().equals(DinoparcConstants.PTEROZ)) {
            assertEquals(createdDinoz.getActionsMap().size(), 4);
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.COMBAT));
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.FOUILLER));
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.TournoiDinoville));
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.DÉPLACERVERT));
        } else {
            assertEquals(createdDinoz.getActionsMap().size(), 3);
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.COMBAT));
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.TournoiDinoville));
            assertTrue(createdDinoz.getActionsMap().containsKey(DinoparcConstants.DÉPLACERVERT));
        }

        createdPlayer = playerRepository.findById(accountId).get();
        assertEquals(createdPlayer.getCash(), 99000 - dinozToBuy.getPrice());
        assertEquals(createdPlayer.getDinozIsList().size(), 1);
    }

    private PlayerDinozStore getDinozToBuy(Player createdPlayer, boolean isPteroz, boolean isRokky) {
        PlayerDinozStore ret = null;
        var playerDinozStoreContent = jooqContext.selectFrom(PLAYER_DINOZ_STORE)
                .where(PLAYER_DINOZ_STORE.PLAYER_ID.eq(UUID.fromString(createdPlayer.getId())))
                .orderBy(PLAYER_DINOZ_STORE.RANK)
                .fetchInto(PlayerDinozStore.class);

        while (ret == null) {
            for (var playerDinozStore : playerDinozStoreContent) {
                if (isPteroz && playerDinozStore.getRace().equals(DinoparcConstants.PTEROZ)) {
                    ret = playerDinozStore;
                    break;
                }
                if (isRokky && playerDinozStore.getRace().equals(DinoparcConstants.ROKKY)) {
                    ret = playerDinozStore;
                    break;
                }
                ret = playerDinozStore;
                break;
            }

            if (ret == null) {
                inventoryRepository.addInventoryItem(createdPlayer.getId(), DinoparcConstants.CHAMPIFUZ, 1);
                accountController.spendOneChampifuzTicket("test", createdPlayer.getId());
            }
        }

        return ret;
    }
}
