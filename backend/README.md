# Neoparc Backend

This is the backend server for the Neoparc project.

## Requirements

- Java 16
- Elasticsearch 7

## Gradle tasks

This project uses Gradle tasks. Use `./gradlew <task>` to execute a task.

### `run`

Compiles and runs the project.

### `flywayMigrate`

Update the database schema to the latest version

### `flywayClean`

Drop everything in the database!
