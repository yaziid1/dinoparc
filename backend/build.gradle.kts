import org.jooq.meta.jaxb.Logging
import org.yaml.snakeyaml.Yaml
import java.io.InputStream
import java.io.IOException

val springSwaggerVersion = "2.9.2"
val kotlinVersion = "1.8.20"

buildscript {
  repositories {
    mavenCentral()
  }
  dependencies {
    "classpath"(group = "org.yaml", name = "snakeyaml", version = "1.30")
  }
}

plugins {
  kotlin("jvm") version "1.8.20"
  id("nu.studer.jooq") version "7.1"
  id("org.flywaydb.flyway") version "8.4.4"
  java
  application
}

group = "com.jonathan.dinoparc"
version = "0.1.0"

java {
  sourceCompatibility = JavaVersion.VERSION_16
  targetCompatibility = JavaVersion.VERSION_16
}

kotlin {
  target {
    compilations.all {
      kotlinOptions {
        jvmTarget = "16"
      }
    }
  }
}

tasks.withType<JavaCompile> {
  options.encoding = "UTF-8"
  options.compilerArgs.addAll(listOf("-Xlint:deprecation"))
}

repositories {
  mavenCentral()
  maven(url = "https://gitlab.com/api/v4/projects/17810311/packages/maven")
  maven(url = "https://gitlab.com/api/v4/projects/23125194/packages/maven")
}

dependencies {
  implementation("org.apache.commons:commons-lang3:3.12.0")
  implementation("org.apache.commons:commons-collections4:4.4")
  implementation("org.apache.commons:commons-csv:1.8")
  implementation("org.bouncycastle:bcprov-jdk15on:1.69")
  implementation("org.postgresql:postgresql:42.3.1")
  implementation("org.springframework.boot:spring-boot-starter-jdbc:2.4.2")
  implementation("org.springframework.boot:spring-boot-starter-web:2.4.2")
  implementation("org.springframework.boot:spring-boot-starter-security:2.4.2")
  implementation("org.springframework.security:spring-security-core:5.1.5")
  implementation("org.springframework.security:spring-security-web:5.1.5")
  implementation("org.springframework.security:spring-security-config:5.1.5")
  implementation("org.flywaydb:flyway-core:8.4.3")

  implementation(group = "io.springfox", name = "springfox-swagger2", version = springSwaggerVersion)
  implementation(group = "io.springfox", name = "springfox-swagger-ui", version = springSwaggerVersion)
  implementation("org.jsoup:jsoup:1.14.3")
  implementation("net.eternaltwin:oauth-client:0.1.7")
  implementation("net.eternaltwin:etwin:0.7.0")
  implementation("com.squareup.okhttp3:okhttp:4.9.1")
  implementation("com.auth0:java-jwt:3.18.3")
  testImplementation("org.springframework.boot:spring-boot-starter-test:2.6.3")
  testImplementation("io.zonky.test:embedded-database-spring-test:2.1.1")


  jooqGenerator("org.postgresql:postgresql:42.3.1")
}

tasks.test {
  // Use the built-in JUnit support of Gradle.
  useJUnitPlatform()
}

fun readConfig(): Map<String, Any?> {
  val script: File = project.buildscript.sourceFile ?: throw Error("FailedToResolveBuildScriptFile");
  val backendDir: File = script.parentFile;
  val resourcesDir = File(File(File(backendDir, "src"), "main"), "resources");
  val defaultConfigFile = File(resourcesDir, "application.yml");
  val localConfigFile = File(backendDir, "application.yml");

  val defaultConfig = parseConfig(defaultConfigFile.inputStream())
  val localConfig = try {
    parseConfig(localConfigFile.inputStream())
  } catch (e: IOException) {
    mapOf()
  }

  return defaultConfig + localConfig;
}

fun parseConfig(input: InputStream): Map<String, Any?> {
  val yaml = Yaml();
  val doc: Any = yaml.load(input)

  val flat = mutableMapOf<String, Any?>();
  flattenConfig(flat, "", doc);
  return flat
}

fun flattenConfig(flat: MutableMap<String, Any?>, path: String, value: Any?) {
  // https://bitbucket.org/snakeyaml/snakeyaml/wiki/Documentation#markdown-header-yaml-tags-and-java-types
  when (value) {
    is Map<*, *> -> {
      for (entry in value) {
        val k = entry.key as String
        val v = entry.value
        val newPath: String = if (path.isEmpty()) { k } else { "$path.$k" };
        flattenConfig(flat, newPath, v)
      }
    }
    else -> {
      assert(path.isNotEmpty())
      flat[path] = value
    }
  }
}

val config = readConfig();
val isDev = (config["server.backend"] as String).contains("localhost")

jooq {
  version.set("3.15.0")  // default (can be omitted)
  edition.set(nu.studer.gradle.jooq.JooqEdition.OSS)  // default (can be omitted)

  configurations {
    create("main") {  // name of the jOOQ configuration
      generateSchemaSourceOnCompilation.set(true)  // default (can be omitted)

      jooqConfiguration.apply {
        logging = Logging.WARN
        jdbc.apply {
          driver = "org.postgresql.Driver"
          url = config["spring.datasource.url"] as String
          user = config["spring.datasource.username"] as String
          password = config["spring.datasource.password"] as String
        }
        generator.apply {
          name = "org.jooq.codegen.DefaultGenerator"
          database.apply {
            name = "org.jooq.meta.postgres.PostgresDatabase"
            inputSchema = "public"
          }
          generate.apply {
            isDeprecated = false
            isRecords = true
            isImmutablePojos = true
            isFluentSetters = true
          }
          target.apply {
            packageName = "com.dinoparc"
            directory = "build/generated-src/jooq/main"  // default (can be omitted)
          }
          strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
        }
      }
    }
  }
}

flyway {
  url = config["spring.datasource.url"] as String
  user = config["spring.datasource.username"] as String
  password = config["spring.datasource.password"] as String
  locations = arrayOf("filesystem:src/main/resources/db/migration")
  // Disable `clean` outside of the dev environment (prevent errors when deployed)
  cleanDisabled = !isDev
}

tasks.named<nu.studer.gradle.jooq.JooqGenerate>("generateJooq") {
  allInputsDeclared.set(true)
}

application {
  mainClass.set("com.dinoparc.api.DinoparcBackendServer")
}
